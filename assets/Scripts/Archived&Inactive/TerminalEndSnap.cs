﻿using UnityEngine;
using System.Collections;

public class TerminalEndSnap : MonoBehaviour {

	public GameObject[] otherTerminals;
	public static int[] correspondOtherTerminals;
	public GameObject tableTop;
	public static int currentRes = 0;
	public static int nearestRes = 0;
	public static bool currentIs90 = false;
	public static bool nearestIs90 = false;

	private int x;

	// Use this for initialization
	void Start ()
	{
		otherTerminals = GameObject.FindGameObjectsWithTag("Terminal");

		Debug.Log (otherTerminals.Length);
		for(int i = 0; i < otherTerminals.Length; i++)
		{
			Debug.Log (otherTerminals [i]);
		}
	}

	void CheckOrientation(GameObject terminal1, GameObject terminal2)
	{
		if (terminal1.transform.parent.transform.rotation.z == 0) {
			currentIs90 = false;
		} else {
			currentIs90 = true;
		}

		if (terminal2.transform.parent.transform.rotation.z == 0) {
			nearestIs90 = false;
		} else {
			nearestIs90 = true;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		float distance;
		//int x;
		for (int i = 0; i < otherTerminals.Length; i++) 
		{
			for (int j = i+1; j < otherTerminals.Length; j++) 
			{
			/*	if (j == i) 
				{
					continue;
				}*/

				distance = Vector2.Distance (otherTerminals [i].transform.position, otherTerminals [j].transform.position);
				if(distance <= 0.5f)
				{
					x = 1;	//something wrong in this section with checking otherTerminals name
					if (otherTerminals [i].name == this.name && otherTerminals [i].transform.parent == this.transform.parent)
					{
						//correspondOtherTerminals [i] = 1;
						//correspondOtherTerminals [j] = 2;	
						currentRes = i;
						nearestRes = j;
						CheckOrientation (otherTerminals[i], otherTerminals[j]);
						//Debug.Log (this.ToString () + " is close to " + otherTerminals [j].transform.ToString ());
						if(Input.GetMouseButtonUp(0))
						{
							tableTop.SendMessage ("Snap", x);
						}
					}
					else if (otherTerminals [j].name == this.name && otherTerminals [j].transform.parent == this.transform.parent)
					{
						//correspondOtherTerminals [i] = 2;
						//correspondOtherTerminals [j] = 1;	
						currentRes = j;
						nearestRes = i;
						CheckOrientation (otherTerminals[j], otherTerminals[i]);
						//Debug.Log (this.ToString () + " is close to " + otherTerminals [i].transform.ToString ());
						if(Input.GetMouseButtonUp(0))
						{
							tableTop.SendMessage ("Snap", x);
						}
					}


				}
			}
		}
//		for (int i = 0; i < otherTerminals.Length; i++) 
//		{
//			if (otherTerminals [i].name == this.name && otherTerminals [i].transform.parent == this.transform.parent) {//changed to &&, not ||
//				x = i;
//				//continue;
//			}
//		}
//
//		for (int i = 0; i < otherTerminals.Length; i++) 
//		{
//			if (i == x) {//changed to &&, not ||
//				//x = i;
//				continue;
//			}
//			else
//			{ 	
//				distance = Vector2.Distance (otherTerminals [i].transform.position, this.transform.position);
//				if(distance <= 1)
//				{
//					//Debug.Log (this.ToString () + " is close to " + otherTerminals [i].transform.ToString ());
//					if(Input.GetMouseButtonUp(0))
//					{
//						//Debug.Log (this.transform.parent.ToString());
//						//tableTop.SendMessage ("Snap", this.transform.parent);
//						tableTop.SendMessage ("Snap", x);
//					}
//				}
//			}
//		}

	}
}
