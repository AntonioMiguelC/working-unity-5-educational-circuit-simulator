﻿using UnityEngine;
using System.Collections;

public class ParallelTransform : MonoBehaviour {

	public GameObject[] otherResistors;
	public GameObject[] allWires;
	public GameObject[] allNodes;

	public void TransformParallel()
	{
		int resistorsSelected = 0;
		GameObject keepResistor;
		int keepNum;
		otherResistors = GameObject.FindGameObjectsWithTag("Resistor");
		for (int i = 0; i < otherResistors.Length; i++)
		{
			if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected)
			{
				resistorsSelected++;
				if (resistorsSelected == 1)
				{
					//keepResistor = otherResistors [i];
					keepNum = i;
					Debug.Log ("keepNum value = " + keepNum);
					GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected = false;
				}
				//Debug.Log (resistorsSelected + " is selected");
			}
		}

		if (resistorsSelected <= 1)
		{
			Debug.Log ("Not enough resistors selected for this transform");
		} 
		else
		{
			for (int i = otherResistors.Length - 1; i >= 0; i--)
			{
				if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected)
				{
					Destroy (otherResistors [i]);
				}
			}
		}

		allWires = GameObject.FindGameObjectsWithTag ("Wire");
		for (int i = allWires.Length - 1; i >= 0; i--)
		{
			Destroy (allWires[i]);
		}

		allNodes = GameObject.FindGameObjectsWithTag ("EndNode");
		for (int i = allNodes.Length - 1; i >= 0; i--)
		{
			Destroy (allNodes[i]);
		}

		GameObject go = GameObject.FindGameObjectWithTag ("TableTopMain");
		SpawnResistorHandler other = (SpawnResistorHandler) go.GetComponent (typeof(SpawnResistorHandler));
		other.ConnectWires ();

	}
}
