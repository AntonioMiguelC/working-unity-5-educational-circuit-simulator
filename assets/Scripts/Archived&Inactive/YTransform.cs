﻿using UnityEngine;
using System.Collections;

public class YTransform : MonoBehaviour {

	public GameObject[] otherResistors;
	public GameObject[] allWires;
	public GameObject[] allNodes;

	private GameObject[] allTerminals;
	private int[] allSelectedTerminals;

	private bool valid = false;
	private int terminalCount;
	private float timeLeft;


	//Private Flag variables for OnGUI//
	private bool tooManyResistorsSelected = false;
	private bool notValidDeltaYTransform = false;
	private bool tooFewResistorsSelected = false;
	/*******************************/

	private void YTransformCheck()
	{    
		terminalCount = 0;
		allSelectedTerminals = new int[10];
		allTerminals = GameObject.FindGameObjectsWithTag ("Terminal");
		int checkTerminal = 0;
		int commonNode = 0;
		bool commonNodeFound = false;
		int commonNodeIntersects = 0;
		int commonNodeSelectedIntersects = 0;

		for (int i = 0; i < allTerminals.Length; i++)
		{
			if (GameObject.Find (allTerminals [i].transform.parent.name).GetComponent<SelectHandler> ().isSelected)
			{
				allSelectedTerminals [terminalCount] = i;
				terminalCount++;
			}
		}

		if (terminalCount != 6)
		{
			valid = false;
		} 
		else
		{
			for (int i = 0; i < allSelectedTerminals.Length - 1; i++)
			{
				for (int j = i + 1; j < allSelectedTerminals.Length; j++)
				{
					if ((allTerminals [allSelectedTerminals [i]].GetComponent<BoxCollider2D> ().bounds.Intersects (allTerminals [allSelectedTerminals [j]].GetComponent<BoxCollider2D> ().bounds))&&(!commonNodeFound))
					{
						commonNode = allSelectedTerminals [i];
						commonNodeFound = true;
					}
				}
			}

			for (int i = 0; i < allTerminals.Length; i++)
			{
				if (i != commonNode)
				{
					if (allTerminals [i].GetComponent<BoxCollider2D> ().bounds.Intersects (allTerminals[commonNode].GetComponent<BoxCollider2D>().bounds))
					{
						commonNodeIntersects++;
						if (GameObject.Find (allTerminals [i].transform.parent.name).GetComponent<SelectHandler> ().isSelected)
						{
							commonNodeSelectedIntersects++;
						}
					}
				}
			}

			if ((commonNodeIntersects == 2) && (commonNodeSelectedIntersects == 2))
			{
				valid = true;
			} 
			else
			{
				valid = false;	
			}
		}
	}

	public void TransformY()
	{
		int resistorsSelected = 0;
		int resistorIs90 = 0;
		int shiftOver = 0;
		int reference = 0;
		GameObject keepResistor;
//		int keepNum;
		otherResistors = GameObject.FindGameObjectsWithTag("Resistor");
		for (int i = 0; i < otherResistors.Length; i++)
		{
			if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected)
			{
				resistorsSelected++;
				if (otherResistors [i].transform.rotation.z != 0)
				{
					resistorIs90++;	
				}
			}
		}

		YTransformCheck ();

		if (resistorsSelected != 3)
		{
			if (resistorsSelected < 3) //More than 3 resistor values selected
			{
				int required = 3 - resistorsSelected;
				Debug.Log ("Please select " + required + " more resistor(s) for this transform");
				tooManyResistorsSelected = false;
				notValidDeltaYTransform = false;
				tooFewResistorsSelected = true;
				timeLeft = 5f;
			} else if (resistorsSelected > 3)
			{
				Debug.Log ("You have selected too many resistors.");
				tooManyResistorsSelected = true;
				notValidDeltaYTransform = false;
				tooFewResistorsSelected = false;
				timeLeft = 5f;
			}
		} 
		else if (valid == false)
		{
			Debug.Log ("The selected resistors are not valid for a Y to Delta transform. Please select again.");
			tooManyResistorsSelected = false;
			notValidDeltaYTransform = true;
			tooFewResistorsSelected = false;
			timeLeft = 5f;
		}
		else //Y transform can happen
		{
			//Debug.Log (resistorIs90 + " perpendicular resistors");
			tooManyResistorsSelected = false;
			notValidDeltaYTransform = false;
			tooFewResistorsSelected = false;
			if (resistorIs90 == 1)
			{
				for (int i = 0; i < otherResistors.Length; i++)
				{
					if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected && otherResistors[i].transform.rotation.z != 0)
					{
						shiftOver = i;
					}
					if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected && otherResistors[i].transform.rotation.z == 0)
					{
						reference = i;
					}
				}
				otherResistors [shiftOver].transform.Rotate (0f, 0f, -270f);

				if (otherResistors [shiftOver].transform.position.y < otherResistors [reference].transform.position.y)
				{
					float gap = otherResistors [reference].transform.position.y - otherResistors [shiftOver].transform.position.y; 
					otherResistors [shiftOver].transform.Translate (0f, 2*gap, 0f);
				}
				else
				{
					float gap = otherResistors [shiftOver].transform.position.y - otherResistors [reference].transform.position.y; 
					otherResistors [shiftOver].transform.Translate (0f, -2*gap, 0f);
				}

				GameObject go = GameObject.FindGameObjectWithTag ("TableTopMain");
				SpawnResistorHandler other = (SpawnResistorHandler) go.GetComponent (typeof(SpawnResistorHandler));
				other.ConnectYParallelWires ();
			} 
			else
			{   
				for (int i = 0; i < otherResistors.Length; i++)
				{
					if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected && otherResistors[i].transform.rotation.z == 0)
					{
						shiftOver = i;
					}
					if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected && otherResistors[i].transform.rotation.z != 0)
					{
						reference = i;
					}
				}
				otherResistors [shiftOver].transform.Rotate (0f, 0f, 270f);

				if (otherResistors [shiftOver].transform.position.x < otherResistors [reference].transform.position.x)
				{
					float gap = otherResistors [reference].transform.position.x - otherResistors [shiftOver].transform.position.x; 
					otherResistors [shiftOver].transform.Translate (0f, 2*gap, 0f);
				}
				else
				{
					float gap = otherResistors [shiftOver].transform.position.x - otherResistors [reference].transform.position.x; 
					otherResistors [shiftOver].transform.Translate (0f, -2*gap, 0f);
				}

				GameObject go = GameObject.FindGameObjectWithTag ("TableTopMain");
				SpawnResistorHandler other = (SpawnResistorHandler) go.GetComponent (typeof(SpawnResistorHandler));
				other.ConnectYPerpendicularWires ();
			}

			for (int i = 0; i < otherResistors.Length; i++)
			{
				if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected)
				{
					GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected = false;
				}
			}
		}
		//Debug.Log ("Shiftover resistor is R" + shiftOver);					
	}

	public void OnGUI() //Change so it looks better
	{	
		if (tooManyResistorsSelected && timeLeft > 0f) 
		{
			GUI.Label (new Rect (10, 10, 150, 150), "You have selected too many resistors.");
			timeLeft -= Time.deltaTime;
		} 
		else if (notValidDeltaYTransform && timeLeft > 0f) 
		{
			GUI.Label (new Rect (10, 10, 150, 150), "Not valid for Delta-Y Transformation.");
			timeLeft -= Time.deltaTime;
		}
		else if(tooFewResistorsSelected && timeLeft > 0f)
		{
			GUI.Label (new Rect (10, 10, 150, 150), "You need to select 3 resistors for this Transform");
			timeLeft -= Time.deltaTime;
		}
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
}