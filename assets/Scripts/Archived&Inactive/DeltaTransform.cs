﻿using UnityEngine;
using System.Collections;

public class DeltaTransform : MonoBehaviour {

	public GameObject[] otherResistors;
	public GameObject[] allWires;
	public GameObject[] allNodes;

	//Private Flag variables for OnGUI//
	private bool tooManyResistorsSelected = false;
	private bool notValidDeltaYTransform = false;
	private bool tooFewResistorsSelected = false;

	private float timeLeft;
	/*******************************/

	public void TransformDelta()
	{
		int resistorsSelected = 0;
		GameObject keepResistor;
//		int keepNum;
		otherResistors = GameObject.FindGameObjectsWithTag("Resistor");
		for (int i = 0; i < otherResistors.Length; i++)
		{
			if (GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected)
			{
				resistorsSelected++;
//				if (resistorsSelected == 1)
//				{
//					keepNum = i;
//					Debug.Log ("keepNum value = " + keepNum);
//					GameObject.Find (otherResistors [i].name).GetComponent<SelectHandler> ().isSelected = false;
//				}
			}
		}

		if (resistorsSelected != 3)
		{
			if (resistorsSelected < 3)
			{
				int required = 3 - resistorsSelected;
				Debug.Log ("Please select " + required + " more resistor(s) for this transform");
				tooManyResistorsSelected = false;
				notValidDeltaYTransform = false;
				tooFewResistorsSelected = true;
				timeLeft = 5f;
			}
			else if (resistorsSelected > 3) 
			{
				tooManyResistorsSelected = true;
				notValidDeltaYTransform = false;
				tooFewResistorsSelected = false;
				timeLeft = 5f;
				Debug.Log ("You have selected too many resistors.");
			}
		} 
		else
		{

		}
	}

	public void OnGUI() //Change so it looks better
	{	
		if (tooManyResistorsSelected && timeLeft > 0f) 
		{
			GUI.Label (new Rect (10, 10, 150, 150), "You have selected too many resistors.");
			timeLeft -= Time.deltaTime;
		} 
		else if (notValidDeltaYTransform && timeLeft > 0f) 
		{
			GUI.Label (new Rect (10, 10, 150, 150), "Not valid for Delta-Y Transformation.");
			timeLeft -= Time.deltaTime;
		}
		else if(tooFewResistorsSelected && timeLeft > 0f)
		{
			GUI.Label (new Rect (10, 10, 150, 150), "You need to select 3 resistors for this Transform");
			timeLeft -= Time.deltaTime;
		}
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
}
