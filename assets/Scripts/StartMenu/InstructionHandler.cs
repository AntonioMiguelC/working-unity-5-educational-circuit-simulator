﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class InstructionHandler : MonoBehaviour
{
	//This script is used for linking buttons with different scenes in the Main instruction level

	public Canvas quitWindow;
	public Button nextButton;
	public Button escapeButton;
    private bool escapeToggle;

	// Use this for initialization
	void Start ()
	{
		quitWindow = quitWindow.GetComponent<Canvas> ();
		nextButton = nextButton.GetComponent<Button> ();
		escapeButton = escapeButton.GetComponent<Button> ();

		quitWindow.enabled = false;
	}

	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
        }

        if (quitWindow.enabled)
        {
			//When the quit window appears, disable all buttons in the page
			nextButton.enabled = false;
			escapeButton.enabled = false;
        }
        else
        {
			//All buttons in the page are enabled
			nextButton.enabled = true;
			escapeButton.enabled = true;
        }
	}


	public void NextPressed ()
	{
		//Navigate to the Equivalent Resistance: Practice Exercise help scene
		SceneManager.LoadScene ("LayoutEquivResistance");
	}

	public void EscapePressed ()
	{
		//Navigate to the Main menu
		SceneManager.LoadScene ("StartMenu");
	}

	public void ExitPressed ()
	{
		//Disable all buttons when menu window appears
		quitWindow.enabled = true;
		nextButton.enabled = false;
		escapeButton.enabled = false;
	}

	public void ExitNoPressed ()
	{
		quitWindow.enabled = false;
		nextButton.enabled = true;
		escapeButton.enabled = true;
	}

	public void ExitYesPressed ()
	{
		//Quit the simulator
		Application.Quit ();
	}
		
}
