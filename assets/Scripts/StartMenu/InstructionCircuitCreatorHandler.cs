﻿/*
 * Author: Josiah Martinez
 * Project 59
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class InstructionCircuitCreatorHandler : MonoBehaviour
{
	//This script is used for linking buttons with different scenes in the Circuit Creator level

	public Canvas quitWindow;
	public Button nextButton;
	public Button lastButton;
	public Button escapeButton;
    private bool escapeToggle;

	// Use this for initialization
	void Start ()
	{
		quitWindow = quitWindow.GetComponent<Canvas> ();
		nextButton = nextButton.GetComponent<Button> ();
		lastButton = lastButton.GetComponent<Button> ();
		escapeButton = escapeButton.GetComponent<Button> ();

		quitWindow.enabled = false;
	}

	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
        }

        if (quitWindow.enabled)
        {
			//When the quit window appears, disable all buttons in the page
			nextButton.enabled = false;
			lastButton.enabled = false;
			escapeButton.enabled = false;
        }
        else
        {
			//All buttons in the page are enabled
			nextButton.enabled = true;
			lastButton.enabled = true;
			escapeButton.enabled = true;
        }
	}


	public void NextPressed ()
	{
		//Navigate to the Kirchhoff help scene
		SceneManager.LoadScene ("LayoutKirchhoff");
	}

	public void LastPressed ()
	{
		//Navigate to the Equivalent Resistance: Practice Exercise help scene
		SceneManager.LoadScene ("LayoutEquivResistance");
	}

	public void EscapePressed ()
	{
		//Navigate to the Main menu
		SceneManager.LoadScene ("StartMenu");
	}

	public void ExitPressed ()
	{
		//Disable all buttons when menu window appears
		quitWindow.enabled = true;
		nextButton.enabled = false;
		lastButton.enabled = false;
		escapeButton.enabled = false;
	}

	public void ExitNoPressed ()
	{
		quitWindow.enabled = false;
		nextButton.enabled = true;
		lastButton.enabled = true;
		escapeButton.enabled = true;
	}

	public void ExitYesPressed ()
	{
		//Quits the simulator
		Application.Quit ();
	}
		
}
