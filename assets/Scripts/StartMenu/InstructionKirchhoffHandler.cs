﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class InstructionKirchhoffHandler : MonoBehaviour
{
	//This script is used for linking buttons with different scenes in the Kirchhoff help level

	public Canvas quitWindow;
	public Button lastButton;
	public Button escapeButton;
    private bool escapeToggle;

	// Use this for initialization
	void Start ()
	{
		quitWindow = quitWindow.GetComponent<Canvas> ();
		lastButton = lastButton.GetComponent<Button> ();
		escapeButton = escapeButton.GetComponent<Button> ();

		quitWindow.enabled = false;
	}

	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
        }

        if (quitWindow.enabled)
        {
			//When the quit window appears, disable all buttons in the page
			lastButton.enabled = false;
			escapeButton.enabled = false;
        }
        else
        {
			//All buttons in the page are enabled
			lastButton.enabled = true;
			escapeButton.enabled = true;
        }
	}

	public void LastPressed ()
	{
		//Navigate to the Circuit Creator help scene
		SceneManager.LoadScene ("LayoutCircuitCreator");
	}

	public void EscapePressed ()
	{
		//Navigate to the Main menu
		SceneManager.LoadScene ("StartMenu");
	}

	public void ExitPressed ()
	{
		//Disable all buttons when menu window appears
		quitWindow.enabled = true;
		lastButton.enabled = false;
		escapeButton.enabled = false;
	}

	public void ExitNoPressed ()
	{
		quitWindow.enabled = false;
		lastButton.enabled = true;
		escapeButton.enabled = true;
	}

	public void ExitYesPressed ()
	{
		//Quit the simulator
		Application.Quit ();
	}
		
}
