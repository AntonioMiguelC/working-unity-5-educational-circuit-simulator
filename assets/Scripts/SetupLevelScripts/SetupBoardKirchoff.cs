﻿/*
 * Author: Josiah Martinez
 * Project 59
 */
using UnityEngine;
using System.Collections;

public class SetupBoardKirchoff : MonoBehaviour {

	public Transform resistor;
	public BoxCollider2D boxCol_1, boxCol_2;
	public Transform endNodeA, endNodeB;
	public Transform battery;
	public GameObject wireObject;
                                                                                                      
	//Set the coordinates and rotation of the resistors, end nodes and battery
	private Vector3[] resistorCoordinates = {new Vector3(-4.97f, 1.17f, 0f), new Vector3(0.29f, 1.17f, 0f), new Vector3(-2.3f, -0.97f, 0f), new Vector3(2.24f, -0.97f, 0f), new Vector3(-2.41f, 2.79f, 0f)};
	private Vector3[] inputTerminalCoordinates = { new Vector3 (-7f, -0.5f, 0f), new Vector3 (-7f, -2.54f, 0f) };
	private Vector3 batteryCoordinates = new Vector3(-7f, -1.52f, 0f);
	private bool[] resistorOrientation = {false, false, true, true, false};
	private Quaternion q = new Quaternion(0,0,0,90f);
	private GameObject[] resistorArray, wireArray, nodeArray, resTerminalArray;
	private int wireCount = 0;
	private GameObject wirething;

	// Use this for initialization
	void Start () 
	{		
		string name;
		//Instantiate all the resistors using the coordinates specified
		for (int i = 0; i < resistorCoordinates.Length; i++)
		{
			if(resistorOrientation[i] == true)
			{
				q.z = -90;
			}
			else
			{
				q.z = 0; 
			}
			Instantiate (resistor, resistorCoordinates [i], q);
		}
		//Dsiable draggable boolean in end nodes
		endNodeA.GetComponent<Draggable>().enabled = false;
		endNodeB.GetComponent<Draggable>().enabled = false;
		//Rename all resistors
		resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
		for(int i = 0; i < resistorArray.Length; i++)
		{
			resistorArray [i].transform.SetParent(this.transform);
			name = "R" + i.ToString();
			resistorArray [i].name = name;

			resistorArray[i].GetComponent<Draggable>().enabled = false;
		}
		//Disable all terminal selects on the resistors
		resTerminalArray = GameObject.FindGameObjectsWithTag("Terminal");
		foreach (GameObject terminal in resTerminalArray)
		{
			terminal.GetComponent<SelectHandler>().enabled = false;
		}

		//Call function to connect wires between resistor terminals
		ConnectWires (resistorArray[2].transform.GetChild(0).position, resistorArray[0].transform.GetChild(1).position);
		ConnectWires (resistorArray[3].transform.GetChild(0).position, resistorArray[1].transform.GetChild(1).position); 
		ConnectWires (resistorArray[3].transform.GetChild(0).position, resistorArray[4].transform.GetChild(1).position);
		ConnectWires (resistorArray[1].transform.GetChild(0).position, resistorArray[0].transform.GetChild(1).position);
		//Instantiate the end nodes and rename them
		Instantiate (endNodeA, inputTerminalCoordinates [0], q);
		Instantiate (endNodeB, inputTerminalCoordinates [1], q);
		nodeArray = GameObject.FindGameObjectsWithTag ("EndNode");
		nodeArray [0].transform.SetParent(this.transform);
		name = "NodeA";
		nodeArray [0].name = name;
		nodeArray [1].transform.SetParent(this.transform);
		name = "NodeB";
		nodeArray [1].name = name;

		//Do final connections between resistors and the end nodes
		ConnectWires (inputTerminalCoordinates[0], resistorArray[0].transform.GetChild(0).position);
		ConnectWires (inputTerminalCoordinates[0], resistorArray[4].transform.GetChild(0).position);
		ConnectWires (resistorArray[2].transform.GetChild(1).position, inputTerminalCoordinates[1]);
		ConnectWires (resistorArray[3].transform.GetChild(1).position, inputTerminalCoordinates[1]);

		//instantiate the battery in its position and rename
		Instantiate (battery, batteryCoordinates, q);
		GameObject batteryObject = GameObject.FindGameObjectWithTag ("Battery");
		name = "Battery";
		batteryObject.name = name;
		ConnectWires (inputTerminalCoordinates[0], inputTerminalCoordinates[1]);


		GameObject[] wires = GameObject.FindGameObjectsWithTag("Wire");

		foreach (GameObject wire in wires)
		{
		}

	}

	//This function connects the wires between two terminal positions, spawning a wire mid-way between the two points, scaling the wire to meet both points, and if needed
	//checking space for corners that may intersect to ensure very little overlapping occurs
	public void ConnectWires(Vector3 term1, Vector3 term2)
	{
		Vector3 term1Pos = term1;
		Vector3 term2Pos = term2;
		Vector3 midpoint = Vector3.Lerp (term1Pos, term2Pos, 0.5f);
		float distance = Vector3.Distance (term1Pos, term2Pos);

		if (distance < 0.1) //no need to spawn a wire if the distance is close enough that the colliders intersect.
		{
			return;
		}

		distance = distance / 0.05f;



		if ((term1Pos.x != term2Pos.x) && (term1Pos.y == term2Pos.y))
		{
			//Extend the wire so that it meets the terminal
			wirething = (GameObject)Instantiate (wireObject, midpoint, q);


			wirething.transform.localScale = new Vector3 (distance, 1, 1);
			BoxCollider2D[] wireColliders = wirething.GetComponentsInChildren<BoxCollider2D> ();

			float scale = 1 / distance;
			wireColliders [0].transform.position = term1Pos;
			wireColliders [1].transform.position = term2Pos;
			wireColliders [0].transform.localScale = new Vector3 (scale, 1f, 1f);
			wireColliders [1].transform.localScale = new Vector3 (scale, 1f, 1f);

			wireColliders [0].size = new Vector2 (0.17f, 0.17f);
			wireColliders [1].size = new Vector2 (0.17f, 0.17f);
			wireCount++;
		} 
		else if ( (term1Pos.y != term2Pos.y) && (term1Pos.x == term2Pos.x) )
		{
			//Extend the wire so that it meets the terminal
			wirething = (GameObject)Instantiate (wireObject, midpoint, q);

			wirething.transform.localScale = new Vector3 (1, distance, 1);
			BoxCollider2D[] wireColliders = wirething.GetComponentsInChildren<BoxCollider2D> ();

			float scale = 1 / distance;
			wireColliders [0].transform.position = term1Pos;
			wireColliders [1].transform.position = term2Pos;
			wireColliders [0].transform.localScale = new Vector3 (1f, scale, 1f);
			wireColliders [1].transform.localScale = new Vector3 (1f, scale, 1f);

			wireColliders [0].size = new Vector2 (0.17f, 0.17f);
			wireColliders [1].size = new Vector2 (0.17f, 0.17f);

			wireCount++;
		}
		else
		{
			Vector3 corner = new Vector3 (term1Pos.x, term2Pos.y, term1Pos.z);    

			var collisionOccurs = Physics2D.OverlapCircleAll(corner, 0.03f);
			if (collisionOccurs.Length > 0)
			{
				Vector3 cornerAlt = new Vector3 (term2Pos.x, term1Pos.y, term1Pos.z);
				var secondCollisionOccurs = Physics2D.OverlapCircleAll(cornerAlt, 0.03f);
				if (collisionOccurs.Length > secondCollisionOccurs.Length)
				{
					ConnectWires(term1, cornerAlt);
					ConnectWires(term2, cornerAlt); 
				}
				else
				{
					ConnectWires(term1, corner);
					ConnectWires(term2, corner);
				}
			} 
			else
			{
				//At this point, the terminals dont share an x or y value
				ConnectWires(corner, term1);
				ConnectWires(corner, term2);
				//do not include wireCount++;
			}

		}

		//Name the wire
		wirething.transform.SetParent(this.transform);
		wirething.transform.name = "Wire" + (wireCount - 1).ToString (); 
	}
}
