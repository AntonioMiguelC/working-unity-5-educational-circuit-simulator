﻿/*
 * Author: Josiah Martinez
 */using UnityEngine;
using System.Collections;

public class SetupBoardLvl2 : MonoBehaviour {

	//This is a setup script for the second Eqiuvalent Resistance: practice exercise, its contents are very similar to the SetupBoard with changes to the number of resistors,
	//positions of the components etc. 
	//Please Refer to SetupBoard.cs

	public Transform resistor;
	public BoxCollider2D boxCol_1, boxCol_2;
	public Transform endNodeA, endNodeB;

	public GameObject wireObject;
	private Vector3[] resistorCoordinates = {new Vector3(-4.13f, 1.54f, 0f), new Vector3(-2.73f, 1.54f, 0f), new Vector3(0.6f, 1.54f, 0f), new Vector3(0.62f, 0.41f, 0f), new Vector3(1.13f, 0.95f, 0f), new Vector3(1.75f, 1.53f, 0f), new Vector3(1.76f, 0.41f, 0f) };
	private Vector3[] inputTerminalCoordinates = { new Vector3 (-6f, 1.54f, 0f), new Vector3 (-6f, -2.54f, 0f) };
		
	private bool[] resistorOrientation = {false, false, false, false, true, false, false};
	private Quaternion q = new Quaternion(0,0,0,90f);
	private GameObject[] resistorArray, wireArray, nodeArray, resTerminalArray;
	private int wireCount = 0;
	private GameObject wirething;

	// Use this for initialization
	void Start () 
	{		
		string name;

		for (int i = 0; i < resistorCoordinates.Length; i++)
		{
			if(resistorOrientation[i] == true)
			{
				q.z = -90;
			}
			else
			{
				q.z = 0; 
			}
			Instantiate (resistor, resistorCoordinates [i], q);
		}

        endNodeA.GetComponent<Draggable>().enabled = false;
        endNodeB.GetComponent<Draggable>().enabled = false;


		resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
		for(int i = 0; i < resistorArray.Length; i++)
		{
			resistorArray [i].transform.SetParent(this.transform);
			name = "R" + i.ToString();
			resistorArray [i].name = name;

            resistorArray[i].GetComponent<Draggable>().enabled = false;
		}

        resTerminalArray = GameObject.FindGameObjectsWithTag("Terminal");
        foreach (GameObject terminal in resTerminalArray)
        {
            terminal.GetComponent<SelectHandler>().enabled = false;
        }

		ConnectWires (resistorArray[0].transform.GetChild(1).position, resistorArray[1].transform.GetChild(0).position);
		ConnectWires (resistorArray [1].transform.GetChild (1).position, resistorArray[2].transform.GetChild(0).position);
		ConnectWires (resistorArray[2].transform.GetChild(0).position, resistorArray[3].transform.GetChild(0).position); 
		ConnectWires (resistorArray[5].transform.GetChild(1).position, resistorArray[6].transform.GetChild(1).position);

		Instantiate (endNodeA, inputTerminalCoordinates [0], q);
		Instantiate (endNodeB, inputTerminalCoordinates [1], q);
		nodeArray = GameObject.FindGameObjectsWithTag ("EndNode");
		nodeArray [0].transform.SetParent(this.transform);
		name = "NodeA";
		nodeArray [0].name = name;
		nodeArray [1].transform.SetParent(this.transform);
		name = "NodeB";
		nodeArray [1].name = name;

		ConnectWires (resistorArray[0].transform.GetChild(0).position, inputTerminalCoordinates[0]);
		ConnectWires (resistorArray[6].transform.GetChild(1).position, inputTerminalCoordinates[1]);

        GameObject[] wires = GameObject.FindGameObjectsWithTag("Wire");

        foreach (GameObject wire in wires)
        {
            wire.transform.GetChild(2).GetComponent<Draggable>().enabled = false;
        }

	}

	public void ConnectWires(Vector3 term1, Vector3 term2)
	{
		Vector3 term1Pos = term1;
		Vector3 term2Pos = term2;
		Vector3 midpoint = Vector3.Lerp (term1Pos, term2Pos, 0.5f);
		float distance = Vector3.Distance (term1Pos, term2Pos);

		if (distance < 0.1) //no need to spawn a wire if the distance is close enough that the colliders intersect.
		{
			return;
		}

		distance = distance / 0.05f;



		if ((term1Pos.x != term2Pos.x) && (term1Pos.y == term2Pos.y))
		{
			//Extend the wire so that it meets the terminal
			wirething = (GameObject)Instantiate (wireObject, midpoint, q);


			wirething.transform.localScale = new Vector3 (distance, 1, 1);
			BoxCollider2D[] wireColliders = wirething.GetComponentsInChildren<BoxCollider2D> ();

			float scale = 1 / distance;
			wireColliders [0].transform.position = term1Pos;
			wireColliders [1].transform.position = term2Pos;
			wireColliders [0].transform.localScale = new Vector3 (scale, 1f, 1f);
			wireColliders [1].transform.localScale = new Vector3 (scale, 1f, 1f);

			wireColliders [0].size = new Vector2 (0.17f, 0.17f);
			wireColliders [1].size = new Vector2 (0.17f, 0.17f);
			wireCount++;
		} 
		else if ( (term1Pos.y != term2Pos.y) && (term1Pos.x == term2Pos.x) )
		{
			//Extend the wire so that it meets the terminal
			wirething = (GameObject)Instantiate (wireObject, midpoint, q);

			wirething.transform.localScale = new Vector3 (1, distance, 1);
			BoxCollider2D[] wireColliders = wirething.GetComponentsInChildren<BoxCollider2D> ();

			float scale = 1 / distance;
			wireColliders [0].transform.position = term1Pos;
			wireColliders [1].transform.position = term2Pos;
			wireColliders [0].transform.localScale = new Vector3 (1f, scale, 1f);
			wireColliders [1].transform.localScale = new Vector3 (1f, scale, 1f);

			wireColliders [0].size = new Vector2 (0.17f, 0.17f);
			wireColliders [1].size = new Vector2 (0.17f, 0.17f);

			wireCount++;
		}
		else
        {
            Vector3 corner = new Vector3 (term1Pos.x, term2Pos.y, term1Pos.z);    

            var collisionOccurs = Physics2D.OverlapCircleAll(corner, 0.03f);
            if (collisionOccurs.Length > 0)
            {
                Vector3 cornerAlt = new Vector3 (term2Pos.x, term1Pos.y, term1Pos.z);
                var secondCollisionOccurs = Physics2D.OverlapCircleAll(cornerAlt, 0.03f);
                if (collisionOccurs.Length > secondCollisionOccurs.Length)
                {
                    ConnectWires(term1, cornerAlt);
                    ConnectWires(term2, cornerAlt); 
                }
                else
                {
                    ConnectWires(term1, corner);
                    ConnectWires(term2, corner);
                }
            } 
            else
            {
                //At this point, the terminals dont share an x or y value
                ConnectWires(corner, term1);
                ConnectWires(corner, term2);
                //do not include wireCount++;
            }

		}

		//Name the wire
		wirething.transform.SetParent(this.transform);
		wirething.transform.name = "Wire" + (wireCount - 1).ToString (); //we want to start naming at Wire0
	}
}
