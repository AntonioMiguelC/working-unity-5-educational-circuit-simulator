﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Security.Cryptography.X509Certificates; // WHY?

/*This class file describes the funtionality of the Equivalent Resistance Levels' Controller. It handles all the Resisors and Wires in game.
  */

public class GameMasterControl : MonoBehaviour
{
	
	public Canvas pauseMenu, exitWindow;
	public Button seriesTransformButton, paralellTransformButton, delta2YTransformButton, y2DeltaTransformButton;
	public BoxCollider2D topWall, bottomWall, leftWall, rightWall;
	public Camera mainCam;
    public GameObject[] resistorArray, wireArray;

    private bool firstClick = false;
    private bool enableWireCommand = true;
    private bool connectCheck = true;
    private GameObject[] endNodeArray;
    private Scene thisScene;
	private Vector3 BoxCol1_pos;
	private Vector3 BoxCol2_pos;



	// Use this for initialization
	private void Start ()
	{
		/*initialise the boundaries*/
		setWallPosition ();
		setWallDimensions ();
		/**************************/

		pauseMenu.enabled = false;
		exitWindow.enabled = false;
        thisScene = SceneManager.GetActiveScene();
        endNodeArray = GameObject.FindGameObjectsWithTag ("EndNode");

		//Debug.Log ("Initialisation complete!" );
	}
	
	private void setWallPosition ()
	{
		topWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		bottomWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		leftWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2f, 0f)).y);
		rightWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2f, 0f)).y);
	}

	private void setWallDimensions ()
	{
		topWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y + 0.5f);
		bottomWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).y - 0.5f);
		leftWall.offset = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x - 0.5f, 0f);
		rightWall.offset = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x + 0.5f, 0f);
	}

	public void ResumePressed()
	{
		pauseMenu.enabled = false;

		seriesTransformButton.enabled = true;
		paralellTransformButton.enabled = true;
		delta2YTransformButton.enabled = true;
		y2DeltaTransformButton.enabled = true;
	}

    public void RestartScene()
    {
        SceneManager.LoadScene(thisScene.name);
    }

	public void ExitToMainMenuPressed()
	{
		SceneManager.LoadScene ("StartMenu");
	}

	public void ExitToDesktopPressed()
	{
		exitWindow.enabled = true;
	}

	public void ExitWindowNoPressed()
	{
		exitWindow.enabled = false;
	}

	public void ExitWindowYesPressed()
	{
		Application.Quit ();
	}

	private void Update ()
	{

        GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
		GameObject[] wireArray = GameObject.FindGameObjectsWithTag ("Wire");

		//Once the user has found the equivalent resistance, the controller deletes all wires and connects the last resistor to the terminals.
        if (resistorArray.Length == 1 && connectCheck)
        {
            Debug.Log("Only one resistor left");
            foreach (GameObject wire in wireArray)
            {
                Destroy(wire);
            }
            this.GetComponent<ConnectWireHandler>().ConnectWires(resistorArray[0].transform.GetChild(0).position, endNodeArray[0].transform.position);
            this.GetComponent<ConnectWireHandler>().ConnectWires(resistorArray[0].transform.GetChild(1).position, endNodeArray[1].transform.position);
            connectCheck = false;
        }

        foreach (GameObject element in resistorArray)
        {
            element.GetComponent<ResistorUtility> ().publicStart ();
        }

        foreach (GameObject element in wireArray)
        {
            element.GetComponent<WireUtility>().PublicStart();
            foreach (Draggable i in element.GetComponentsInChildren<Draggable>())
            {
                i.enabled = false;
            }
        }

        if (Input.GetMouseButtonDown(0)) 
        {
            firstClick = true;
        }

        if (firstClick)
        {
            /*After connecting wires, we need to make sure that each wire contains the name of the resistors it is connected to as well as the terminal*/
            if (enableWireCommand)
            {
                wireArray = GameObject.FindGameObjectsWithTag("Wire");

                foreach (GameObject i in wireArray)
                {
                    i.GetComponent<WireUtility>().wireToWireConnection();
                }
                enableWireCommand = false;
            }

            foreach (GameObject wire in wireArray)
            {
                if (wire.transform.GetComponent<WireUtility>().resistorsAt1.Count == 0 || wire.transform.GetComponent<WireUtility>().resistorsAt2.Count == 0)
                {
                    if (!wire.transform.GetComponent<WireUtility>().connectedToEndNode && !wire.transform.GetComponent<WireUtility>().newlySpawned)
                    {
						BoxCol1_pos = wire.transform.GetChild (0).position;
						BoxCol2_pos = wire.transform.GetChild (1).position;
                        Destroy(wire);
                    }
                }

                if (wire.transform.GetComponent<WireUtility>().resistorsAt1.Count == 0 && wire.transform.GetComponent<WireUtility>().resistorsAt2.Count == 0)
                {
                    Destroy(wire);
                }
            }
        }

		//This part checks to see if a transform caused the resulting configuration to have a resistor disconnected from the circuit
		//If a resistor is disconnected, it is reconnected to the correct part of the circuit
		foreach (GameObject resistor in resistorArray)
		{
			if(resistor.transform.GetComponent<ResistorUtility>().componentsAtleft.Count == 0)
			{
				if(Vector3.Distance(BoxCol1_pos, resistor.transform.GetChild(0).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (0).position, BoxCol2_pos);
				}
				else if(Vector3.Distance(BoxCol2_pos, resistor.transform.GetChild(0).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (0).position, BoxCol1_pos);
				}
			}
			else if(resistor.transform.GetComponent<ResistorUtility>().componentsAtright.Count == 0)
			{
				if(Vector3.Distance(BoxCol1_pos, resistor.transform.GetChild(1).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (1).position, BoxCol2_pos);
				}
				else if(Vector3.Distance(BoxCol2_pos, resistor.transform.GetChild(1).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (1).position, BoxCol1_pos);
				}
			}
		}


		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			pauseMenu.enabled = !pauseMenu.enabled; //toggle pause menu

			//toggle enability of the buttons
			seriesTransformButton.enabled = !seriesTransformButton.enabled;
			paralellTransformButton.enabled = !paralellTransformButton.enabled;
			delta2YTransformButton.enabled = !delta2YTransformButton.enabled;
			y2DeltaTransformButton.enabled = !y2DeltaTransformButton.enabled;

            if (exitWindow.enabled)
            {
                exitWindow.enabled = false;
            }

		}

		if (pauseMenu.enabled) 
		{ 
			//during the pause state, do not allow users to select resistors
			resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");

			for (int i = 0; i < resistorArray.Length; i++)
			{
				// hold the select state of the resistor
				resistorArray [i].GetComponent<SelectHandler> ().isSelectable = false;
			}
		}
		else 
		{
			for (int i = 0; i < resistorArray.Length; i++)
			{
				// hold the select state of the resistor
				resistorArray [i].GetComponent<SelectHandler> ().isSelectable = true;
			}
		}
	}
}