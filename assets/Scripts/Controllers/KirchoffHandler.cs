﻿/*
 * Author: Josiah Martinez	
 * Project 59
 */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class KirchoffHandler : MonoBehaviour {

	//This script is used for a level to just demonstrate on a single premade circuit which resistors are part of a valid KVL

	public string loopString = "";
	public Text loopText;

	private List<Transform> selectedResistor;
	private GameObject[] resistorArray;
	private GameObject batteryObject;


	void Start()
	{
		//Obtains all the gameobjects with the tags resistor and battery
		resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
		batteryObject = GameObject.FindGameObjectWithTag ("Battery");
	}

	public void Loop1 ()
	{
		//based on user selection, an equation will display for that loop
		if (batteryObject.GetComponent<SelectHandler> ().isSelected && resistorArray [0].GetComponent<SelectHandler> ().isSelected && !resistorArray [1].GetComponent<SelectHandler> ().isSelected && resistorArray [2].GetComponent<SelectHandler> ().isSelected && !resistorArray [3].GetComponent<SelectHandler> ().isSelected && !resistorArray [4].GetComponent<SelectHandler> ().isSelected)
		{
			loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
			loopString += "\n";
			loopString += "\nIn this loop:\n -V(battery) + (i1-i2)*R0 + (i1-i3)*R2 = 0";
		}
		else if (!batteryObject.GetComponent<SelectHandler> ().isSelected && resistorArray [0].GetComponent<SelectHandler> ().isSelected && resistorArray [1].GetComponent<SelectHandler> ().isSelected && !resistorArray [2].GetComponent<SelectHandler> ().isSelected && !resistorArray [3].GetComponent<SelectHandler> ().isSelected && resistorArray [4].GetComponent<SelectHandler> ().isSelected)
		{
			loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
			loopString += "\n";
			loopString += "\nIn this loop:\n (i2-i1)*R0 + i2*R4 + (i2-i3)*R1 = 0";
		} 
		else if (!batteryObject.GetComponent<SelectHandler> ().isSelected && !resistorArray [0].GetComponent<SelectHandler> ().isSelected && resistorArray [1].GetComponent<SelectHandler> ().isSelected && resistorArray [2].GetComponent<SelectHandler> ().isSelected && resistorArray [3].GetComponent<SelectHandler> ().isSelected && !resistorArray [4].GetComponent<SelectHandler> ().isSelected)
		{
			loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
			loopString += "\n";
			loopString += "\nIn this loop:\n (i3-i1)*R2 + (i3-i2)*R1 + i3*R3 = 0";
		}
        else if (batteryObject.GetComponent<SelectHandler> ().isSelected && !resistorArray [0].GetComponent<SelectHandler> ().isSelected && !resistorArray [1].GetComponent<SelectHandler> ().isSelected && !resistorArray [2].GetComponent<SelectHandler> ().isSelected&& resistorArray [3].GetComponent<SelectHandler> ().isSelected && resistorArray [4].GetComponent<SelectHandler> ().isSelected)
		{
			loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
			loopString += "\n";
			loopString += "\nIn this loop:\n -V(battery) + i2*R4 + i3*R3 = 0";
		}
        else if (batteryObject.GetComponent<SelectHandler> ().isSelected && !resistorArray [0].GetComponent<SelectHandler> ().isSelected && resistorArray [1].GetComponent<SelectHandler> ().isSelected && resistorArray [2].GetComponent<SelectHandler> ().isSelected && !resistorArray [3].GetComponent<SelectHandler> ().isSelected && resistorArray [4].GetComponent<SelectHandler> ().isSelected)
        {
            loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
            loopString += "\n";
            loopString += "\nIn this loop:\n  -V(battery) + i2*R4 + (i2-i3)*R1 + (i1-i3)*R2 = 0";
        }
        else if (batteryObject.GetComponent<SelectHandler> ().isSelected && resistorArray [0].GetComponent<SelectHandler> ().isSelected && resistorArray [1].GetComponent<SelectHandler> ().isSelected && !resistorArray [2].GetComponent<SelectHandler> ().isSelected && resistorArray [3].GetComponent<SelectHandler> ().isSelected && !resistorArray [4].GetComponent<SelectHandler> ().isSelected)
        {
            loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
            loopString += "\n";
            loopString += "\nIn this loop:\n  -V(battery) + (i1-i2)*R0 + (i3-i2)*R1 + i3*R3 = 0";
        }
        else if (!batteryObject.GetComponent<SelectHandler> ().isSelected && resistorArray [0].GetComponent<SelectHandler> ().isSelected && !resistorArray [1].GetComponent<SelectHandler> ().isSelected && resistorArray [2].GetComponent<SelectHandler> ().isSelected && resistorArray [3].GetComponent<SelectHandler> ().isSelected && resistorArray [4].GetComponent<SelectHandler> ().isSelected)
        {
            loopString = "Kirchoffs Voltage law states the sum of all voltages around a loop equals 0.";
            loopString += "\n";
            loopString += "\nIn this loop:\n  (i3-i1)*R2 + (i2-i1)*R0 + i2*R4 + i3*R3 = 0";
        }
		else
		{
			loopString = "The components you have selected are not valid for any loop in this circuit, please try again.";
		}
	}

	// Update is called once per frame
	void Update () {
		loopText.text = loopString;
	}
}
