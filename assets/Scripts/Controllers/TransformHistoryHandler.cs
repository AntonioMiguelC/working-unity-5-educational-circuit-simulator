﻿/*
 * Author: Josiah Martinez
 * Project 59
 */ 
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class TransformHistoryHandler : MonoBehaviour {

	//This script handles all the GUI elements for the Equivalent Resistance: practice exercise levels, updating new transforms inside the scroller panel as well as
	//incrementing the counter value at the top of the screen

	public List<string> allTransforms;
	public GameObject TransformMaster;
	public RectTransform myPanel;
	public GameObject myTextPrefab;
	public Text scrollRectText;
	public bool newValue = false;

	private string newText;
	private Text readText;
	private float nextTransform;
	private int countTransform;
	private string countString = "0";
	private int prevCountVal = 0;
	private bool display = false;
	private int offset = 0;
	private bool offsetCalc = false;
	private GUIStyle guiStyle = new GUIStyle();

	void Start ()	
	{
		//Reset all values each time a new level is opened
		newValue = false;
		allTransforms = new List<string> ();
		offsetCalc = false;
		newText = null;
		//Set the styling of the text in the history panel
		offset = 0;
		guiStyle.fontSize = 40;
		guiStyle.fontStyle = FontStyle.Bold;
	}

	//This function is meant to increase the value of counter once after each successful transform
	public void Increment()
	{
		if(SceneManager.GetActiveScene().name.Equals("CircuitCreator"))
		{
			return;
		}
		GameObject.Find ("ScoreManager").GetComponent<ScoreManager>().score++;
		if (!offsetCalc)
		{
			Debug.Log ("enters increment");
			offset = countTransform - 1;

			Debug.Log ("setting offsetCalc to true");  
			offsetCalc = true;
		}
		int temp = countTransform - offset;
		countString = temp.ToString();
	}

	public void StartRefresh ()
	{
		if(SceneManager.GetActiveScene().name.Equals("CircuitCreator"))
		{
			return;
		}
		//Refresh the counter value
		if (!newValue)
		{
			countTransform = 0;
			newValue = true;
		}
	}

	public void RetrieveNewTransform (string dialog) {
		//Ignore if level is not circuit creator
		if(SceneManager.GetActiveScene().name.Equals("CircuitCreator"))
		{
			return;
		}
		//Find the gameobject that holds the text for history panel, and append a string to the end of that text value, retrieved by a call in the transform scripts
		newText = GameObject.Find ("ScoreManager").GetComponent<ScoreManager>().history + "\n";
		newText += "Transform " + GameObject.Find ("ScoreManager").GetComponent<ScoreManager>().score.ToString() + "\n";
		newText += dialog;
		newText += "\n";
		GameObject.Find ("ScoreManager").GetComponent<ScoreManager>().history = newText;
	}

}

