using UnityEngine;
using System.Collections;

public class TableTopControl : MonoBehaviour {

	public GameObject[] otherTerminals;
	bool canSnapFlag = false;

	// Use this for initialization
	void Start () 
	{
		otherTerminals = GameObject.FindGameObjectsWithTag("Terminal");

		Debug.Log (otherTerminals.Length);
		for(int i = 0; i < otherTerminals.Length; i++)
		{
			Debug.Log (otherTerminals [i]);
		}
	}

	//void Snap(Transform resistor, int terminalEnd)
	void Snap(int terminalEnd)
	{
		int current = TerminalEndSnap.currentRes;
		int nearest = TerminalEndSnap.nearestRes;
		Vector2 offset = new Vector2 (otherTerminals [nearest].transform.parent.position.x, otherTerminals [nearest].transform.parent.position.y);

		//this is pretty messy, maybe clean it up through case statements or something?   0.564f
		if (otherTerminals [current].name == "LeftCollider" && otherTerminals [nearest].name == "RightCollider") {
			if (!TerminalEndSnap.currentIs90 && !TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x + 1.128f, offset.y);
				otherTerminals [current].transform.parent.position = temp;
			} else if (TerminalEndSnap.currentIs90 && !TerminalEndSnap.nearestIs90) {
//				Vector2 temp = new Vector2 (offset.x + 0.559f, offset.y - 0.564f);
				Vector2 temp = new Vector2 (offset.x + 0.561f, offset.y - 0.564f); 
				otherTerminals [current].transform.parent.position = temp;
//				otherTerminals [nearest].transform.parent.position = offset;
			} else if (!TerminalEndSnap.currentIs90 && TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x + 0.564f, offset.y - 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			} else if (TerminalEndSnap.currentIs90 && TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x, offset.y - 1.128f);
				otherTerminals [current].transform.parent.position = temp;
			}
		} else if (otherTerminals [current].name == "RightCollider" && otherTerminals [nearest].name == "LeftCollider") {
			if (!TerminalEndSnap.currentIs90 && !TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x - 1.128f, offset.y);
				otherTerminals [current].transform.parent.position = temp;
			} else if (TerminalEndSnap.currentIs90 && !TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x - 0.564f, offset.y + 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			} else if (!TerminalEndSnap.currentIs90 && TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x - 0.564f, offset.y + 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			} else if (TerminalEndSnap.currentIs90 && TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x, offset.y + 1.128f);
				otherTerminals [current].transform.parent.position = temp;
			}
		} else if (otherTerminals [current].name == "LeftCollider" && otherTerminals [nearest].name == "LeftCollider") {
			if (TerminalEndSnap.currentIs90 && !TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x - 0.564f, offset.y - 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			} else if (!TerminalEndSnap.currentIs90 && TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x + 0.564f, offset.y + 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			}
		} else if (otherTerminals [current].name == "RightCollider" && otherTerminals [nearest].name == "RightCollider") {
			if (TerminalEndSnap.currentIs90 && !TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x + 0.564f, offset.y + 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			} else if (!TerminalEndSnap.currentIs90 && TerminalEndSnap.nearestIs90) {
				Vector2 temp = new Vector2 (offset.x - 0.564f, offset.y - 0.564f);
				otherTerminals [current].transform.parent.position = temp;
			}
		}
		current = 0;
		nearest = 0;
	}
}

