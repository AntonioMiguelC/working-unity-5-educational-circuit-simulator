﻿/*
 * Author: Antonio Castro and Josiah Martinez
 * Project 59
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;


public class LevelHandler : MonoBehaviour {

	//This script is used for navigating to the appropriate levels in the level selection menu

	public Button level1;
	public Button level2;
	public Button level3;
	public Button level4;
	public Button levelKirchoff;
	public Button circuitCreator;
	public Button mainMenu;


	// Use this for initialization
	void Start ()
	{
		level1 = level1.GetComponent<Button> ();
		level2 = level2.GetComponent<Button> ();
		level3 = level3.GetComponent<Button> ();
		level4 = level4.GetComponent<Button> ();
		levelKirchoff = levelKirchoff.GetComponent<Button> ();
		circuitCreator = circuitCreator.GetComponent<Button> ();
		mainMenu = mainMenu.GetComponent<Button> ();
	}

	void Update ()
	{

	}

	//Based on the button selected by the user, load the corresponding scene level
	public void L1Pressed ()
	{
		SceneManager.LoadScene ("Level1");

	}

	public void L2Pressed ()
	{
		SceneManager.LoadScene ("Level2");

	}

	public void L3Pressed ()
	{
		SceneManager.LoadScene ("Level3");

	}

	public void L4Pressed ()
	{
		SceneManager.LoadScene ("Level4");

	}

	public void LKPressed ()
	{
		SceneManager.LoadScene ("LevelKirchoff");

	}

	public void CircuitCreatorPressed ()
	{
		SceneManager.LoadScene ("CircuitCreator");

	}

	public void MenuPressed ()
	{
		SceneManager.LoadScene ("StartMenu");

	}
}
