﻿using UnityEngine;
using System.Collections;

/*This is used for providing the general visual framework for the trash bin in Circuit Creator*/
public class TrashHandler : MonoBehaviour {

    private Vector3 local_scale;

    void Start()
    {
        local_scale = this.transform.localScale;
    }

    void OnMouseEnter()
    {
        Debug.Log("OnMouseEnter");
        this.transform.localScale += new Vector3(0.03f, 0.03f, 0f);

    }

    void OnMouseExit()
    {
        Debug.Log("OnMouseExit");
        this.transform.localScale = local_scale;
    }

}
