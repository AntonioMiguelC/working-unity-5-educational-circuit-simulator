﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	//Handles the Counter in the practice exercise levels, which counts after each successful transform

	public int score = 0;
	public string history = "";
	public Text scoreText;
	public Text historyText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame, updating the text in the Text object
	void Update () {
		scoreText.text = score.ToString();
		historyText.text = history.ToString();
	}
}
