﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Security.Cryptography.X509Certificates;

public class GameMasterControlKirchoff : MonoBehaviour
{
	
	public Canvas pauseMenu, exitWindow;
	public BoxCollider2D topWall, bottomWall, leftWall, rightWall;
	public Camera mainCam;
    public GameObject[] resistorArray, wireArray;

    private bool firstClick = false;
    private bool enableWireCommand = true;
    private bool connectCheck = true;
    private GameObject[] endNodeArray;

	private Vector3 BoxCol1_pos;
	private Vector3 BoxCol2_pos;



	// Use this for initialization
	private void Start ()
	{
		//initialise the boundaries
		setWallPosition ();
		setWallDimensions ();
		pauseMenu.enabled = false;
		exitWindow.enabled = false;

        endNodeArray = GameObject.FindGameObjectsWithTag ("EndNode");
	}

	//Set the position of the boundaries of the simulator as seen by the user, boxing it and limiting it to stay within the screen
	private void setWallPosition ()
	{
		topWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		bottomWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		leftWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2f, 0f)).y);
		rightWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height * 2f, 0f)).y);
	}

	//Set the dimensions of the boundaries of the simulator as seen by the user, boxing it and limiting it to stay within the screen
	private void setWallDimensions ()
	{
		topWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y + 0.5f);
		bottomWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).y - 0.5f);
		leftWall.offset = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x - 0.5f, 0f);
		rightWall.offset = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x + 0.5f, 0f);
	}

	//When in the pause menu, and the resume is pressed, disable pause menu
	public void ResumePressed()
	{
		pauseMenu.enabled = false;
	}
	//Load the main menu of the simulator when exit to menu is selected
	public void ExitToMainMenuPressed()
	{
		SceneManager.LoadScene ("StartMenu");
	}
	//Opens the exit window when selected by the user
	public void ExitToDesktopPressed()
	{
		exitWindow.enabled = true;
	}
	//Exits the exit menu and resumes the simulator
	public void ExitWindowNoPressed()
	{
		exitWindow.enabled = false;
	}
	//Quits to desktop
	public void ExitWindowYesPressed()
	{
		Application.Quit ();
	}

	private void Update ()
	{

        GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
        GameObject[] wireArray = GameObject.FindGameObjectsWithTag("Wire");
        //This section checks if a single resistor remains, and reconfigures the circuit 
        if (resistorArray.Length == 1 && connectCheck)
        {
            Debug.Log("Only one resistor left");
            foreach (GameObject wire in wireArray)
            {
                Destroy(wire);
            }
            this.GetComponent<ConnectWireHandler>().ConnectWires(resistorArray[0].transform.GetChild(0).position, endNodeArray[0].transform.position);
            this.GetComponent<ConnectWireHandler>().ConnectWires(resistorArray[0].transform.GetChild(1).position, endNodeArray[1].transform.position);
            connectCheck = false;
        }

        foreach (GameObject element in resistorArray)
        {
            element.GetComponent<ResistorKirchoffUtility> ().publicStart ();
        }

        foreach (GameObject element in wireArray)
        {
            element.GetComponent<WireKirchoffUtility>().PublicStart();
        }

        if (Input.GetMouseButtonDown(0)) 
        {
            firstClick = true;
        }

        if (firstClick)
        {
            //After connecting wires, we need to make sure that each wire contains the name of the resistors it is connected to as well as the terminal
            if (enableWireCommand)
            {
                wireArray = GameObject.FindGameObjectsWithTag("Wire");

                foreach (GameObject i in wireArray)
                {
                    i.GetComponent<WireKirchoffUtility>().wireToWireConnection();
                }
                enableWireCommand = false;
            }

			foreach (GameObject wire in wireArray)
            {
                if (wire.transform.GetComponent<WireKirchoffUtility>().resistorsAt1.Count == 0 || wire.transform.GetComponent<WireKirchoffUtility>().resistorsAt2.Count == 0)
                {
                    if (!wire.transform.GetComponent<WireKirchoffUtility>().connectedToEndNode && !wire.transform.GetComponent<WireKirchoffUtility>().newlySpawned)
                    {
						BoxCol1_pos = wire.transform.GetChild (0).position;
						BoxCol2_pos = wire.transform.GetChild (1).position;
                        Destroy(wire);
                    }
                }

                if (wire.transform.GetComponent<WireKirchoffUtility>().resistorsAt1.Count == 0 && wire.transform.GetComponent<WireKirchoffUtility>().resistorsAt2.Count == 0)
                {
                    Destroy(wire);
                }
            }
        }

		foreach (GameObject resistor in resistorArray)
		{
			if(resistor.transform.GetComponent<ResistorKirchoffUtility>().componentsAtleft.Count == 0)
			{
				if(Vector3.Distance(BoxCol1_pos, resistor.transform.GetChild(0).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (0).position, BoxCol2_pos);
				}
				else if(Vector3.Distance(BoxCol2_pos, resistor.transform.GetChild(0).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (0).position, BoxCol1_pos);
				}
			}
			else if(resistor.transform.GetComponent<ResistorKirchoffUtility>().componentsAtright.Count == 0)
			{
				if(Vector3.Distance(BoxCol1_pos, resistor.transform.GetChild(1).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (1).position, BoxCol2_pos);
				}
				else if(Vector3.Distance(BoxCol2_pos, resistor.transform.GetChild(1).position) < 0.2)
				{
					this.GetComponent<ConnectWireHandler> ().ConnectWires (resistor.transform.GetChild (1).position, BoxCol1_pos);
				}
			}
		}


		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			pauseMenu.enabled = !pauseMenu.enabled; //toggle pause menu
		}

		if (pauseMenu.enabled) { 
			//during the pause state, do not allow users to select resistors
			resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");

			for (int i = 0; i < resistorArray.Length; i++)
			{
				// hold the select state of the resistor
				resistorArray [i].GetComponent<SelectHandler> ().isSelectable = false;
			}
		}
		else 
		{
			for (int i = 0; i < resistorArray.Length; i++)
			{
				// hold the select state of the resistor
				resistorArray [i].GetComponent<SelectHandler> ().isSelectable = true;
			}
		}
	}
}