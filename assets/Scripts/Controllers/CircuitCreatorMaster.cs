﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Security.Cryptography.X509Certificates;

/*This class file describes the funtionality of the Circuit Creator's Controller. It handles all the Resisors and Wires in game and 
   provides circuit validation checking
  */

public class CircuitCreatorMaster : MonoBehaviour {

	public Transform endNodeSpawner, resistorSpawner;
	public Canvas pauseMenu, exitWindow, transformButtonCanvas, editorCanvas;
	public Button seriesTransformButton, paralellTransformButton, delta2YTransformButton, y2DeltaTransformButton;
	public Camera mainCam;
	public GameObject ConnectWire;
	public GameObject[] resistorArray, wireArray;
	public bool startEnabled = false;

	private bool justOnce = true; //Toggles just once
	private bool firstClick = false;
	private bool enableWireCommand = true;
	private bool connectCheck = true;
	private bool isValidCircuit = false;
	private float GUILabelCountdown;

	private bool dragging = false;
	Vector2 startPos, currentPos;
	private GameObject[] endNodeArray;

	// Use this for initialization
	void Start () 
	{
		transformButtonCanvas.enabled = false;
		editorCanvas.enabled = true;
		pauseMenu.enabled = false;
		exitWindow.enabled = false;

		endNodeArray = GameObject.FindGameObjectsWithTag ("EndNode");
	}

	public void ResumePressed()
	{
		//Paired up with the resume button in GUI level
		pauseMenu.enabled = false;

		seriesTransformButton.enabled = true;
		paralellTransformButton.enabled = true;
		delta2YTransformButton.enabled = true;
		y2DeltaTransformButton.enabled = true;
	}

	public void ResetPressed()
	{
		//clear everything and allow user to create the circuit from scratch

		GameObject[] resistors = GameObject.FindGameObjectsWithTag("Resistor");
		GameObject[] endNodesToConnect = GameObject.FindGameObjectsWithTag("EndNode");
		GameObject[] wires = GameObject.FindGameObjectsWithTag("Wire");

		justOnce = true;
		startEnabled = false;
		connectCheck = true;

		foreach (GameObject resistor in resistors)
		{
			Destroy(resistor);
		}

		foreach (GameObject endNode in endNodesToConnect)
		{
			Destroy(endNode);
		}

		foreach (GameObject wire in wires)
		{
			Destroy(wire);
		}

		transformButtonCanvas.enabled = false;
		editorCanvas.enabled = true;
		endNodeSpawner.gameObject.SetActive (true);
		resistorSpawner.gameObject.SetActive (true);
	}

	public void enableStart()
	{
		//This function is called when the "Start" Button is pressed in the Circuit Creator level

		//It does circuit design verification.

		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
		GameObject[] nodeArray = GameObject.FindGameObjectsWithTag("EndNode");
		GameObject[] wireArray = GameObject.FindGameObjectsWithTag("Wire");

		//There needs to be 2 terminal end nodes to make the circuit valid
		if(nodeArray.Length != 2)
		{
			Debug.Log("INVALID");
			startEnabled = false;
			GUILabelCountdown = 5f;
			return;
		}

		//All resistors and wires need to be connected to something Only end terminals may cause an open circuit
		foreach(GameObject resistor in resistorArray)
		{
			if(resistor.transform.GetComponent<ResistorUtility>().hasEmptyTerminal)
			{
				Debug.Log("INVALID");
				startEnabled = false;
				GUILabelCountdown = 5f;
				return;
			}
		}
		foreach (GameObject wire in wireArray)
		{
			if(wire.transform.GetComponent<WireUtility>().hasEmptyTerminal)
			{
				Debug.Log("INVALID");
				startEnabled = false;
				GUILabelCountdown = 5f;
				return;
			}
		}

		startEnabled = true;
	}

	public void ExitToMainMenuPressed()
	{
		SceneManager.LoadScene ("StartMenu");
	}

	public void ExitToDesktopPressed()
	{
		exitWindow.enabled = true;
	}

	public void ExitWindowNoPressed()
	{
		exitWindow.enabled = false;
	}

	public void ExitWindowYesPressed()
	{
		Application.Quit ();
	}

	// Update is called once per frame
	void Update () 
	{
		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
		GameObject[] nodeArray = GameObject.FindGameObjectsWithTag("EndNode");
		GameObject[] wireArray = GameObject.FindGameObjectsWithTag("Wire");


		foreach(GameObject resistor in resistorArray)
		{
			resistor.transform.GetComponent<ResistorUtility> ().publicStart ();
		}
		foreach(GameObject wire in wireArray)
		{
			wire.transform.GetComponent<WireUtility> ().PublicStart ();
		}

		//Excecutes only when User has made a valid circuit and clicked "Start" (Invoked enableStart())
		if (startEnabled)
		{
			if (justOnce) //Needs to be performed only once. Initialisation for resistor simplification
			{
				transformButtonCanvas.enabled = true;
				editorCanvas.enabled = false;
				endNodeSpawner.gameObject.SetActive (false);
				resistorSpawner.gameObject.SetActive (false);

				foreach(GameObject wire in wireArray)
				{
					wire.GetComponentInChildren<Draggable>().enabled = false;
				}

				foreach(GameObject resistor in resistorArray)
				{
					resistor.transform.GetComponent<Draggable>().enabled = false;
					resistor.transform.GetComponent<SelectHandler> ().enabled = true;
				}
				justOnce = false;
			}

			//This happens when only one resistor is left (The user has found the equivalent resistance)
			if (resistorArray.Length == 1 && connectCheck)
			{
				Debug.Log("Only one resistor left");
				//Makes sure that the resistor is connected to the end node.
				foreach (GameObject wire in wireArray)
				{
					Destroy(wire);
				}
				ConnectWire.GetComponent<ConnectWireHandler>().ConnectWires(resistorArray[0].transform.GetChild(0).position, nodeArray[0].transform.position);
				ConnectWire.GetComponent<ConnectWireHandler>().ConnectWires(resistorArray[0].transform.GetChild(1).position, nodeArray[1].transform.position);
				connectCheck = false;
			}

			//Updates the resistors in the game space. Keeps track of what components are connected to each resistor. See ResistorUtility.cs
			foreach (GameObject element in resistorArray)
			{
				element.GetComponent<ResistorUtility>().publicStart();
			}

			//If a wire is to become disconnected from the circuit. Delete it.
			foreach (GameObject element in wireArray)
			{
				/*TODO: Need to improve this functionality so that it verifies that the wire is still needed for the circuit*/
				Transform term1 = element.transform.GetChild (0);
				Transform term2 = element.transform.GetChild (1);
				
				element.GetComponent<WireUtility>().PublicStart();
				if(!element.transform.GetComponent<WireUtility>().connectedToEndNode)
				{
					if(element.transform.GetComponent<WireUtility>().hasEmptyTerminal)
					{
						Destroy (element);
					}
				}
			}

			//Added functionality to ensure all game components are active and component communication is accurate.
			if (Input.GetMouseButtonDown(0))
			{
				firstClick = true;
			}

			if (firstClick && enableWireCommand)
			{
				/*After connecting wires, we need to make sure that each wire contains the name of the resistors it is connected to as well as the terminal*/
				wireArray = GameObject.FindGameObjectsWithTag("Wire");

				foreach (GameObject i in wireArray)
				{
					i.GetComponent<WireUtility>().wireToWireConnection();
				}
				enableWireCommand = false;
			}
		}
		else
		{
			foreach (GameObject resistor in resistorArray)
			{
				resistor.transform.GetComponent<SelectHandler>().enabled = false;
			}
			foreach(GameObject wire in wireArray)
			{
				wire.GetComponentInChildren<Draggable>().enabled = true;
			}
		}


		if (Input.GetKeyDown(KeyCode.Escape))
		{
			pauseMenu.enabled = !pauseMenu.enabled; //toggle pause menu

			//toggle enability of the buttons
			seriesTransformButton.enabled = !seriesTransformButton.enabled;
			paralellTransformButton.enabled = !paralellTransformButton.enabled;
			delta2YTransformButton.enabled = !delta2YTransformButton.enabled;
			y2DeltaTransformButton.enabled = !y2DeltaTransformButton.enabled;

		}

		if (pauseMenu.enabled)
		{ //during the pause state, do not allow users to select resistors
			resistorArray = GameObject.FindGameObjectsWithTag("Resistor");

			for (int i = 0; i < resistorArray.Length; i++)
			{
				// hold the select state of the resistor
				resistorArray[i].GetComponent<SelectHandler>().isSelectable = false;
			}
		}
		else
		{
			for (int i = 0; i < resistorArray.Length; i++)
			{
				// hold the select state of the resistor
				resistorArray[i].GetComponent<SelectHandler>().isSelectable = true;
			}
		}


		if (nodeArray.Length >= 2)
		{
			endNodeSpawner.GetComponent<SpawnerHandler>().enabled = false;
		}
		else
		{
			endNodeSpawner.GetComponent<SpawnerHandler>().enabled = true;
		}
	}

	//ALL THE GUI DISPLAY PROMPTS ARE TO BE CODED HERE
	public void OnGUI()
	{
		if (!startEnabled && GUILabelCountdown > 0f)
		{
			GUI.color = Color.black;
			GUI.Label(new Rect(100, 100, 150, 150), "Invalid circuit. Please check the validity of your design.");
			GUILabelCountdown -= Time.deltaTime;
		}
	}

}