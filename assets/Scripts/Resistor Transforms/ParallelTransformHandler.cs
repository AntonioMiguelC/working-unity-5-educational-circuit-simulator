﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class ParallelTransformHandler : MonoBehaviour
{
	private bool noSimplificationNeededFlag = false;
	private bool invalidResistorsSelectedFlag = false;
	private bool validResistorSelectedFlag = false;
	private float GUILabelCountdown;

	public GameObject TransformMaster;
	public GameObject GameMaster;
	public int seriesResistorCount = 0;
	private Transform seriesResistor;
	public int parallelResistorCount = 0;
	public string newTransform;

	private List<Transform> selectedResistor;

	public void TransformExcecution()
	{
		List<Transform> resPair = new List<Transform>();
		noSimplificationNeededFlag = false;
		invalidResistorsSelectedFlag = false;
		validResistorSelectedFlag = false;
		selectedResistor = new List<Transform>();
		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
		newTransform = "Resistors ";

		//Add all resistors that have been selected by the user
		//to the selectedResistor List
		for (int i = 0; i < resistorArray.Length; i++) 
		{
			if (resistorArray [i].GetComponent<SelectHandler> ().isSelected) 
			{
				selectedResistor.Add(resistorArray[i].transform);
				newTransform = newTransform + resistorArray[i].name + " ";
			}
		}
		newTransform += "have been parallel transformed into resistor ";

		if (selectedResistor.Count < 2)
		{ //0 or 1 resistors selected
			noSimplificationNeededFlag = true;
			invalidResistorsSelectedFlag = false;
			GUILabelCountdown = 2f;
		}
		else if (selectedResistor.Count == 2)
		{
			Debug.Log("Number of selected resistors is: " + selectedResistor.Count);
			performCheck(selectedResistor);
			if (validResistorSelectedFlag == false)
			{
				noSimplificationNeededFlag = false;
				invalidResistorsSelectedFlag = true;
				GUILabelCountdown = 2f;
				return;
			}
			else
			{
				selectedResistor [0].name = "Rparallel" + parallelResistorCount.ToString ();
				newTransform += selectedResistor [0].name;
				performTransformation(selectedResistor);
				parallelResistorCount++;
				TransformMaster.GetComponent<TransformHistoryHandler> ().Increment ();
				TransformMaster.GetComponent<TransformHistoryHandler> ().RetrieveNewTransform (newTransform);
			}

		}
		else
		{

			for (int i = 0; i < selectedResistor.Count - 1; i++)
			{
				resPair.Clear();
				resPair.Add(selectedResistor[0]);
				resPair.Add(selectedResistor[i + 1]);

				performCheck(resPair);

				if (validResistorSelectedFlag == false)
				{
					Debug.Log ("HIHI");
					return;
				}
			}
				
			for (int i = 0; i < selectedResistor.Count - 1; i++)
			{
				resPair.Clear();
				resPair.Add(selectedResistor[i]);
				resPair.Add(selectedResistor[i + 1]);
				selectedResistor [i].name = "Rparallel" + parallelResistorCount.ToString ();
				if (i == 0)
				{
					newTransform += selectedResistor [i].name;
				}
				performTransformation(resPair);
			}
			parallelResistorCount++;
			TransformMaster.GetComponent<TransformHistoryHandler> ().Increment ();
			TransformMaster.GetComponent<TransformHistoryHandler> ().RetrieveNewTransform (newTransform);
		}
	}

	private void performCheck(List<Transform> resPair)
	{
		if (resPair.Count != 2)
		{
			Debug.Log("return");
			return;
		}

		if (resPair[0].GetComponent<ResistorUtility>().componentsAtleft.Count == 1 && resPair[0].GetComponent<ResistorUtility>().componentsAtleft[0].parent.tag == "Wire")
		{

			if (resPair[0].GetComponent<ResistorUtility>().componentsAtleft[0].name == "BoxCol_1")
			{
				if (resPair[0].GetComponent<ResistorUtility>().componentsAtleft[0].parent.GetComponent<WireUtility>().resistorsAt2.Count == 0)
				{
					//invalid
					noSimplificationNeededFlag = false;
					validResistorSelectedFlag = false;
					invalidResistorsSelectedFlag = true;
					GUILabelCountdown = 2f;
					Debug.Log("return");
					return;
				}
			}
			else
			{
				if (resPair[0].GetComponent<ResistorUtility>().componentsAtleft[0].parent.GetComponent<WireUtility>().resistorsAt1.Count == 0)
				{
					//invalid
					noSimplificationNeededFlag = false;
					validResistorSelectedFlag = false;
					invalidResistorsSelectedFlag = true;
					GUILabelCountdown = 2f;
					Debug.Log("return");
					return;
				}
			}
		}

		//look at the 0th resistor
		foreach (Transform component in resPair[0].GetComponent<ResistorUtility>().componentsAtleft)
		{
			Debug.Log(component.parent.tag);
			if (component.parent.tag == "Resistor" && component.parent.GetComponent<SelectHandler>().isSelected)
			{
				Debug.Log("Selected resistor");
				Debug.Log(component.name);
				if (component.name == "LeftCollider")
				{
					foreach (Transform check in component.parent.GetComponent<ResistorUtility>().componentsAtright)
					{
						if (check.parent.tag == "Wire" && check.name == "BoxCol_1")
						{
							foreach (Transform resTerminalCheck in check.parent.GetComponent<WireUtility>().resistorsAt2)
							{
								if (resTerminalCheck.parent.Equals(resPair[0]))
								{
									//valid
									noSimplificationNeededFlag = false;
									validResistorSelectedFlag = true;
									invalidResistorsSelectedFlag = false;
									GUILabelCountdown = 2f;

									Debug.Log("return");
									return;
								}
							}
						}
						else if (check.parent.tag == "Wire" && check.name == "BoxCol_2")
						{
							Debug.Log("BoxCol_2");
							foreach (Transform resTerminalCheck in check.parent.GetComponent<WireUtility>().resistorsAt1)
							{
								if (resTerminalCheck.parent.Equals(resPair[0]))
								{
									//valid
									noSimplificationNeededFlag = false;
									validResistorSelectedFlag = true;
									invalidResistorsSelectedFlag = false;
									GUILabelCountdown = 2f;

									Debug.Log("return");
									return;
								}
							}
						}
					}
				}
				else if (component.name == "RightCollider")
				{
					foreach (Transform check in component.parent.GetComponent<ResistorUtility>().componentsAtleft)
					{
						if (check.parent.tag == "Wire" && check.name == "BoxCol_1")
						{
							foreach (Transform resTerminalCheck in check.parent.GetComponent<WireUtility>().resistorsAt2)
							{
								if (resTerminalCheck.parent.Equals(resPair[0]))
								{
									//valid
									noSimplificationNeededFlag = false;
									validResistorSelectedFlag = true;
									invalidResistorsSelectedFlag = false;
									GUILabelCountdown = 2f;

									Debug.Log("return");
									return;
								}
							}
						}
						else if (check.parent.tag == "Wire" && check.name == "BoxCol_2")
						{
							Debug.Log("BoxCol_2");
							foreach (Transform resTerminalCheck in check.parent.GetComponent<WireUtility>().resistorsAt1)
							{
								if (resTerminalCheck.parent.Equals(resPair[0]))
								{
									//valid
									noSimplificationNeededFlag = false;
									validResistorSelectedFlag = true;
									invalidResistorsSelectedFlag = false;
									GUILabelCountdown = 2f;

									Debug.Log("return");
									return;
								}
							}
						}
					}
				}
			}
			else if (component.parent.tag == "Resistor") //resistor but not selected
			{
				if (component.name == "LeftCollider")
				{
					foreach (Transform check in component.parent.GetComponent<ResistorUtility>().componentsAtright)
					{
						if (check.parent.tag == "Resistor" && (check.parent.GetComponent<SelectHandler>().isSelected && !check.parent.Equals(resPair[0])))
						{
							//invalid
							noSimplificationNeededFlag = false;
							validResistorSelectedFlag = false;
							invalidResistorsSelectedFlag = true;
							GUILabelCountdown = 2f;

							return;
						}

					}
				}
				else
				{
					foreach (Transform check in component.parent.GetComponent<ResistorUtility>().componentsAtleft)
					{
						if (check.parent.tag == "Resistor" && (check.parent.GetComponent<SelectHandler>().isSelected && !check.parent.Equals(resPair[0])))
						{
							//invalid
							noSimplificationNeededFlag = false;
							validResistorSelectedFlag = false;
							invalidResistorsSelectedFlag = true;
							GUILabelCountdown = 2f;

							return;
						}
					}
				}
			}
			else if (component.parent.tag == "Wire")
			{
				if (component.name == "BoxCol_1")
				{
					foreach (Transform check in component.parent.GetComponent<WireUtility>().resistorsAt2)
					{
						if (check.parent.GetComponent<SelectHandler>().isSelected && !check.parent.Equals(resPair[0]))
						{
                            if (check.name == "LeftCollider")
                            {
                                if (check.parent.GetComponent<ResistorUtility>().componentsAtright.Contains(resPair[0].GetChild(0)) || (check.parent.GetComponent<ResistorUtility>().componentsAtright.Contains(resPair[0].GetChild(1))))
                                {
                                    noSimplificationNeededFlag = false;
                                    validResistorSelectedFlag = true;
                                    invalidResistorsSelectedFlag = false;
                                    GUILabelCountdown = 2f;
                                    Debug.Log("return");
                                    return;
                                }
                                else
                                {
                                    foreach (Transform checkComponentOtherSide in check.parent.GetComponent<ResistorUtility>().componentsAtright)
                                    {
                                        if (checkComponentOtherSide.parent.tag == "Wire")
                                        {
                                            if (checkComponentOtherSide.name == "BoxCol_1")
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(1)))
                                                {
                                                    
                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(1)))
                                                {
                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else //rightcollider
                            {
                                if (check.parent.GetComponent<ResistorUtility>().componentsAtleft.Contains(resPair[0].GetChild(0)) || (check.parent.GetComponent<ResistorUtility>().componentsAtleft.Contains(resPair[0].GetChild(1))))
                                {
                                    noSimplificationNeededFlag = false;
                                    validResistorSelectedFlag = true;
                                    invalidResistorsSelectedFlag = false;
                                    GUILabelCountdown = 2f;
                                    Debug.Log("return");
                                    return;
                                }
                                else
                                {
                                    foreach (Transform checkComponentOtherSide in check.parent.GetComponent<ResistorUtility>().componentsAtleft)
                                    {
                                        if (checkComponentOtherSide.parent.tag == "Wire")
                                        {
                                            if (checkComponentOtherSide.name == "BoxCol_1")
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(1)))
                                                {

                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(1)))
                                                {
                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //invalid
                            noSimplificationNeededFlag = false;
                            validResistorSelectedFlag = false;
                            invalidResistorsSelectedFlag = true;
                            GUILabelCountdown = 2f;
                            Debug.Log("return");
                            return;
						}
					}
				}
				else //BoxCol_2
				{
                    foreach (Transform check in component.parent.GetComponent<WireUtility>().resistorsAt1)
                    {
                        if (check.parent.GetComponent<SelectHandler>().isSelected && !check.parent.Equals(resPair[0]))
                        {
                            if (check.name == "LeftCollider")
                            {
                                if (check.parent.GetComponent<ResistorUtility>().componentsAtright.Contains(resPair[0].GetChild(0)) || (check.parent.GetComponent<ResistorUtility>().componentsAtright.Contains(resPair[0].GetChild(1))))
                                {
                                    noSimplificationNeededFlag = false;
                                    validResistorSelectedFlag = true;
                                    invalidResistorsSelectedFlag = false;
                                    GUILabelCountdown = 2f;
                                    Debug.Log("return");
                                    return;
                                }
                                else
                                {
                                    foreach (Transform checkComponentOtherSide in check.parent.GetComponent<ResistorUtility>().componentsAtright)
                                    {
                                        if (checkComponentOtherSide.parent.tag == "Wire")
                                        {
                                            if (checkComponentOtherSide.name == "BoxCol_1")
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(1)))
                                                {

                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(1)))
                                                {
                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else //rightcollider
                            {
                                if (check.parent.GetComponent<ResistorUtility>().componentsAtleft.Contains(resPair[0].GetChild(0)) || (check.parent.GetComponent<ResistorUtility>().componentsAtleft.Contains(resPair[0].GetChild(1))))
                                {
                                    noSimplificationNeededFlag = false;
                                    validResistorSelectedFlag = true;
                                    invalidResistorsSelectedFlag = false;
                                    GUILabelCountdown = 2f;
                                    Debug.Log("return");
                                    return;
                                }
                                else
                                {
                                    foreach (Transform checkComponentOtherSide in check.parent.GetComponent<ResistorUtility>().componentsAtleft)
                                    {
                                        if (checkComponentOtherSide.parent.tag == "Wire")
                                        {
                                            if (checkComponentOtherSide.name == "BoxCol_1")
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt2.Contains(resPair[0].GetChild(1)))
                                                {

                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                if (checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(0)) || checkComponentOtherSide.parent.GetComponent<WireUtility>().resistorsAt1.Contains(resPair[0].GetChild(1)))
                                                {
                                                    noSimplificationNeededFlag = false;
                                                    validResistorSelectedFlag = true;
                                                    invalidResistorsSelectedFlag = false;
                                                    GUILabelCountdown = 2f;
                                                    Debug.Log("return");
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //invalid
                            noSimplificationNeededFlag = false;
                            validResistorSelectedFlag = false;
                            invalidResistorsSelectedFlag = true;
                            GUILabelCountdown = 2f;
                            Debug.Log("return");
                            return;
                        }
                    }
				}
			}
			else if (component.tag == "EndNode")
			{
				//do nothing
			}
			else
			{
				//invalid
				noSimplificationNeededFlag = false;
				validResistorSelectedFlag = false;
				invalidResistorsSelectedFlag = true;
				GUILabelCountdown = 2f;
				Debug.Log("return");
				return;
			}
		}
		noSimplificationNeededFlag = false;
		validResistorSelectedFlag = false;
		invalidResistorsSelectedFlag = true;
		GUILabelCountdown = 2f;
		Debug.Log("return");
	}

	private void performTransformation(List<Transform> resPair)
	{
		Debug.Log("Doing the simplification");

		float ohm = 0;
		foreach (Transform i in selectedResistor)
		{
			ohm += 1 / (i.GetComponent<ResistorUtility> ().ohm); 
		}
		ohm = 1 / ohm;
		resPair [0].GetComponent<ResistorUtility> ().ohm = ohm;
		Destroy(resPair[1].gameObject);
		resPair [0].GetComponent<SelectHandler> ().isSelected = false;
	}

	public void OnGUI()
	{
		if (noSimplificationNeededFlag && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "No simplification necessary with selected resistors.");

			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((invalidResistorsSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Invalid selection of resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((validResistorSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Valid selection, performing transform");
			GUILabelCountdown -= Time.deltaTime;
		}
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
}