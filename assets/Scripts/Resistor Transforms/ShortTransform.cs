﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class ShortTransform : MonoBehaviour {

	public GameObject GameMaster;
	public GameObject TransformMaster;
	public string newTransform;

	private bool noSimplificationNeededFlag = false;
	private bool invalidResistorsSelectedFlag = false;
	private bool validResistorSelectedFlag = false;
	private float GUILabelCountdown;
	private List<Transform> selectedResistor;

	// Use this for initialization
	public void TransformExecution() 
	{
		noSimplificationNeededFlag = false;
		invalidResistorsSelectedFlag = false;
		validResistorSelectedFlag = false;
		selectedResistor = new List<Transform>();
		//Obtain all resistors in the space and add to an array
		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");

		//add all selected resistors into a new selected resistor list
		for (int i = 0; i < resistorArray.Length; i++) 
		{
			if (resistorArray [i].GetComponent<SelectHandler> ().isSelected) 
			{
				selectedResistor.Add(resistorArray[i].transform);
			}
		}

		if (selectedResistor.Count != 1)
		{ 
			//0 or more than 1 resistors selected, send error message to display
			noSimplificationNeededFlag = true;
			invalidResistorsSelectedFlag = false;
			validResistorSelectedFlag = false;
			GUILabelCountdown = 2f;
		} 
		else //one resistor is selected
		{
			//Checks the left terminal of this selected resistor and inspects the objects connected to it
			//If the right terminal of the same resistor falls inside these lists, then the resistor is in fact shorted
			foreach (Transform i in selectedResistor[0].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Wire")
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == selectedResistor[0].name)
							{
								validResistorSelectedFlag = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == selectedResistor[0].name)
							{
								validResistorSelectedFlag = true;
							}
						}
					}					
				}				
			}
			//If the resistor is not shorted, send error message
			if (!validResistorSelectedFlag)
			{
				noSimplificationNeededFlag = false;
				invalidResistorsSelectedFlag = true;
				validResistorSelectedFlag = false;
				GUILabelCountdown = 2f;
			}
			//perform transform on the resistor, destroying it
			else if (validResistorSelectedFlag)
			{
				noSimplificationNeededFlag = false;
				invalidResistorsSelectedFlag = false;
				validResistorSelectedFlag = true;
				GUILabelCountdown = 2f;
				//Send string value that a resistor has been short circuit transformed to the Tranform history panel
				newTransform = "Resistor " + selectedResistor[0].name + " has been short-circuit transformed.";
				TransformMaster.GetComponent<TransformHistoryHandler> ().Increment ();
				TransformMaster.GetComponent<TransformHistoryHandler> ().RetrieveNewTransform (newTransform);

				Destroy (selectedResistor [0].gameObject);
			}
		}
	}

	public void OnGUI()
	{
		//based on flags raised during the checking, send a message to the user informing them of their selections
		if (noSimplificationNeededFlag && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Incorrect number of selected resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((invalidResistorsSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Invalid selection of resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((validResistorSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Valid selection, performing transform");
			GUILabelCountdown -= Time.deltaTime;
		}
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
}
