﻿/*
 * Author: Josiah Martinez
 * Project 59
 */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class YTransformHandler : MonoBehaviour {

	/* 
	 * This file handles everything related to the Y transform
	 * It includes validity checking on the users selections of resistors and transform, as well as the reconfiguration of the circuit.
	 * Reconfiguration also involves the renaming of resistors and updating of ohm values 
	 */

	private bool noSimplificationNeededFlag = false;
	private bool invalidResistorsSelectedFlag = false;
	private bool validResistorSelectedFlag = false;
	private float GUILabelCountdown;
	public GameObject GameMaster;
	public GameObject TransformMaster;
	private Transform seriesResistor;
    public int yToDeltaResistorCount = 0;
	public string newTransform;

	private List<Transform> selectedResistor;

	public void TransformExcecution()
	{
        noSimplificationNeededFlag = false;
		invalidResistorsSelectedFlag = false;
        validResistorSelectedFlag = false;
		selectedResistor = new List<Transform>();
		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
        bool checkDone = false;
		float a = 0f;
		float b = 0f;
		float c = 0f;

		//Checks all the resistors and adds selected resistors into an array
		for (int i = 0; i < resistorArray.Length; i++) 
		{
			if (resistorArray [i].GetComponent<SelectHandler> ().isSelected) 
			{
				selectedResistor.Add(resistorArray[i].transform);
			}
		}

		//Checks if only 3 resistors are selected as Y only works with 3 resistors
		if (selectedResistor.Count != 3)
        { 
            //0 or 1 resistors selected
            noSimplificationNeededFlag = true;
            invalidResistorsSelectedFlag = false;
            invalidResistorsSelectedFlag = false;
            GUILabelCountdown = 2f;
        }
		else //three resistors are selected
		{
            //check the left terminal of the first selected resistor first
			if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft.Count == 2)
			{
				//Checks that the two resistors inside the list (inside the list of objects connected to the left terminal) are also selected
				if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.tag == "Resistor")
				{
					//Checks that the parent is a resistor as opposed to a wire
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.tag == "Resistor")
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<SelectHandler> ().isSelected && 
							selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<SelectHandler> ().isSelected)
						{
							validResistorSelectedFlag = true;
							GUILabelCountdown = 2f;
							Debug.Log ("valid work");
							checkDone = true; 
							//Call function to reconfigure the circuit from Y to Delta
							PerformYTransform ();
							//Log a string to Transform History Panel that describes the Y transform
							newTransform = "Resistors ";
							foreach (Transform resistor in selectedResistor)
							{
								newTransform = newTransform + resistor.name + " ";
							}
							newTransform = newTransform + " has been Y to Delta transformed to ";
							//rename all the new resistors
							foreach (Transform resistor in selectedResistor)
							{
								resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
								newTransform = newTransform + resistor.name + " ";
								yToDeltaResistorCount++;
								resistor.GetComponent<SelectHandler> ().isSelected = false;
							}
						}
					} 
					//This section checks if there is one wire connection and if that connects to the two other selected resistors
					else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].name == "BoxCol_1")
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<WireUtility> ().resistorsAt2.Count == 1)
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								//Call function to reconfigure the circuit from y to delta
								PerformYTransform ();
								//Log a string to Transform History Panel that describes the Y transform
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";
								//rename the new resistors
								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}	
					} 
					//Checks the connections of the other end of the same wire
					else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].name == "BoxCol_2")
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<WireUtility> ().resistorsAt1.Count == 1)
						{
							//Checks that the other two resistors are in the list of gameobject connected to the other end of this resistor
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								//Call function to reconfigure the circuit
								PerformYTransform ();
								//Log a string value to Transform history panel about this successful transform
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";
								//rename the resistors
								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}	
					}
				}
				//Checks the first terminal end of this wire
				else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].name == "BoxCol_1")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2.Count == 1)
					{
						//Checks the first end of this wire to see if the other two selected resistors share a common node
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								//Call function to reconfigure the circuit
								PerformYTransform ();
								//Log string value of this transform to Transform history panel
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";
								//rename resistors
								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}
					}					
				} 
				//Checks the other end of the same wire
				else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].name == "BoxCol_2")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1.Count == 1)
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [1].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true;
								//Calls function to perform the reconfiguration of the circuit
								PerformYTransform ();
								//Send string value of this transform to history panel 
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";
								//rename the resistors in this transform
								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}
					}					
				}
			} 
			//Checks if there is only one connection to this left terminal end, and checks it is a wire connection
			//Ensures that there are only 2 other resistors connected to the other end of this wire and that both resistors are selected
			else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft.Count == 1)
			{
				if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].name == "BoxCol_1")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2.Count == 2)
					{						
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2 [1].parent.tag == "Resistor")
							{
								if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.GetComponent<SelectHandler> ().isSelected && 
									selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt2 [1].parent.GetComponent<SelectHandler> ().isSelected)
								{
									validResistorSelectedFlag = true;
									GUILabelCountdown = 2f;
									Debug.Log ("valid work");
									checkDone = true; 
									//Call transfrom fuction
									PerformYTransform ();
									//Send new string value of this transform the the transform history panel
									newTransform = "Resistors ";
									foreach (Transform resistor in selectedResistor)
									{
										newTransform = newTransform + resistor.name + " ";
									}
									newTransform = newTransform + " has been Y to Delta transformed to ";
									//rename resistors
									foreach (Transform resistor in selectedResistor)
									{
										resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
										newTransform = newTransform + resistor.name + " ";
										yToDeltaResistorCount++;
										resistor.GetComponent<SelectHandler> ().isSelected = false;
									}
								}
							} 
						}
					}
				}
				//Checking the other end of the wire connected to this resistor, same logic applies with resistor connections in this wire end
				else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].name == "BoxCol_2")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1.Count == 2)
					{						
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1 [1].parent.tag == "Resistor")
							{
								if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.GetComponent<SelectHandler> ().isSelected && 
									selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtleft [0].parent.GetComponent<WireUtility> ().resistorsAt1 [1].parent.GetComponent<SelectHandler> ().isSelected)
								{
									validResistorSelectedFlag = true;
									GUILabelCountdown = 2f;
									Debug.Log ("valid work");
									checkDone = true; 
									//Call function to perform transform
									PerformYTransform ();
									//Send a string value containing info about this transform to the transform history panel
									newTransform = "Resistors ";
									foreach (Transform resistor in selectedResistor)
									{
										newTransform = newTransform + resistor.name + " ";
									}
									newTransform = newTransform + " has been Y to Delta transformed to ";
									//rename resistors
									foreach (Transform resistor in selectedResistor)
									{
										resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
										newTransform = newTransform + resistor.name + " ";
										yToDeltaResistorCount++;
										resistor.GetComponent<SelectHandler> ().isSelected = false;
									}
								}
							} 
						}
					}
				}
			}

            //check the right terminal of the selected resistor, instead of the left. This entire section has similar feature to the last whole section of code
            if (selectedResistor[0].GetComponent<ResistorUtility>().componentsAtright.Count == 2 && !checkDone)
            {
				if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.tag == "Resistor")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.tag == "Resistor")
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<SelectHandler> ().isSelected && 
							selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<SelectHandler> ().isSelected)
						{
							validResistorSelectedFlag = true;
							GUILabelCountdown = 2f;
							Debug.Log ("valid work");
							checkDone = true; 
							PerformYTransform ();
							newTransform = "Resistors ";
							foreach (Transform resistor in selectedResistor)
							{
								newTransform = newTransform + resistor.name + " ";
							}
							newTransform = newTransform + " has been Y to Delta transformed to ";

							foreach (Transform resistor in selectedResistor)
							{
								resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
								newTransform = newTransform + resistor.name + " ";
								yToDeltaResistorCount++;
								resistor.GetComponent<SelectHandler> ().isSelected = false;
							}
						}
					} 
					else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].name == "BoxCol_1")
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<WireUtility> ().resistorsAt2.Count == 1)
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								PerformYTransform ();
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";

								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}	
					} 
					else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].name == "BoxCol_2")
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<WireUtility> ().resistorsAt1.Count == 1)
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								PerformYTransform ();
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";

								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}	
					}
				} 
				else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].name == "BoxCol_1")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2.Count == 1)
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								PerformYTransform ();
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";

								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}
					}					
				} 
				else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].name == "BoxCol_2")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1.Count == 1)
					{
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.GetComponent<SelectHandler> ().isSelected && 
								selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [1].parent.GetComponent<SelectHandler> ().isSelected)
							{
								validResistorSelectedFlag = true;
								GUILabelCountdown = 2f;
								Debug.Log ("valid work");
								checkDone = true; 
								PerformYTransform ();
								newTransform = "Resistors ";
								foreach (Transform resistor in selectedResistor)
								{
									newTransform = newTransform + resistor.name + " ";
								}
								newTransform = newTransform + " has been Y to Delta transformed to ";

								foreach (Transform resistor in selectedResistor)
								{
									resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
									newTransform = newTransform + resistor.name + " ";
									yToDeltaResistorCount++;
									resistor.GetComponent<SelectHandler> ().isSelected = false;
								}
							}
						}
					}					
				}
            }
			else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright.Count == 1 && !checkDone)
			{
				if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].name == "BoxCol_1")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2.Count == 2)
					{						
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2 [1].parent.tag == "Resistor")
							{
								if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2 [0].parent.GetComponent<SelectHandler> ().isSelected && 
									selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt2 [1].parent.GetComponent<SelectHandler> ().isSelected)
								{
									validResistorSelectedFlag = true;
									GUILabelCountdown = 2f;
									Debug.Log ("valid work");
									checkDone = true;
									PerformYTransform ();
									newTransform = "Resistors ";
									foreach (Transform resistor in selectedResistor)
									{
										newTransform = newTransform + resistor.name + " ";
									}
									newTransform = newTransform + " has been Y to Delta transformed to ";

									foreach (Transform resistor in selectedResistor)
									{
										resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
										newTransform = newTransform + resistor.name + " ";
										yToDeltaResistorCount++;
										resistor.GetComponent<SelectHandler> ().isSelected = false;
									}
								}
							}
						}
					}
				}
				else if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].name == "BoxCol_2")
				{
					if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1.Count == 2)
					{						
						if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.tag == "Resistor")
						{
							if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1 [1].parent.tag == "Resistor")
							{
								if (selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1 [0].parent.GetComponent<SelectHandler> ().isSelected && 
									selectedResistor [0].GetComponent<ResistorUtility> ().componentsAtright [0].parent.GetComponent<WireUtility> ().resistorsAt1 [1].parent.GetComponent<SelectHandler> ().isSelected)
								{
									validResistorSelectedFlag = true;
									GUILabelCountdown = 2f;
									Debug.Log ("valid work");
									checkDone = true; 
									PerformYTransform ();
									newTransform = "Resistors ";
									foreach (Transform resistor in selectedResistor)
									{
										newTransform = newTransform + resistor.name + " ";
									}
									newTransform = newTransform + " has been Y to Delta transformed to ";

									foreach (Transform resistor in selectedResistor)
									{
										resistor.name = "Ry-Δ" + yToDeltaResistorCount.ToString ();
										newTransform = newTransform + resistor.name + " ";
										yToDeltaResistorCount++;
										resistor.GetComponent<SelectHandler> ().isSelected = false;
									}
								}
							}
						}

					}
				}
			}
			//If none of the checks worked, then signal to user that the selection made is not actually a Y formation of resistors
			if (!checkDone)
			{
				invalidResistorsSelectedFlag = true;
				GUILabelCountdown = 2f;
				return;
			} 
			else
			{
				TransformMaster.GetComponent<TransformHistoryHandler> ().StartRefresh ();
				TransformMaster.GetComponent<TransformHistoryHandler> ().Increment ();
				TransformMaster.GetComponent<TransformHistoryHandler> ().RetrieveNewTransform (newTransform);
				//Recalculate the new values of these resistors
				a = selectedResistor [0].GetComponent<ResistorUtility> ().ohm * selectedResistor [1].GetComponent<ResistorUtility> ().ohm;
				b = selectedResistor [0].GetComponent<ResistorUtility> ().ohm * selectedResistor [2].GetComponent<ResistorUtility> ().ohm;
				c = selectedResistor [1].GetComponent<ResistorUtility> ().ohm * selectedResistor [2].GetComponent<ResistorUtility> ().ohm;
				selectedResistor [0].GetComponent<ResistorUtility> ().ohm = (a + b + c) / selectedResistor [0].GetComponent<ResistorUtility> ().ohm;
				selectedResistor [1].GetComponent<ResistorUtility> ().ohm = (a + b + c) / selectedResistor [1].GetComponent<ResistorUtility> ().ohm;
				selectedResistor [2].GetComponent<ResistorUtility> ().ohm = (a + b + c) / selectedResistor [2].GetComponent<ResistorUtility> ().ohm;
			}
		}
	}

    private void PerformYTransform()
    {
        int verticalResistorCount = 0;
        Transform isolatedResistor = null;
        List<Transform> nonIsolatedResistors = new List<Transform>();
        List<Vector3> shortWire = new List<Vector3>();
        List<Vector3> wireA = new List<Vector3>();
        List<Vector3> wireB = new List<Vector3>();
        bool leftFound = false;
        bool rightFound = false;
		bool found1 = false;
		bool found2 = false;
		Vector3 leftConnect = new Vector3 ();
		Vector3 rightConnect = new Vector3 ();
        nonIsolatedResistors.Clear();
        float distanceToMove = 0f;
        Vector3 commonNode;
		float tol = 0.1f;

		//Counts the number of vertical resistors which will isolate the different configuration cases which will have specific transform instructions
        foreach (Transform element in selectedResistor)
        {
            if (element.rotation.z != 0)
            {
                verticalResistorCount++;
            }
        }

		//For a combination with 2 vertical resistors
		if (verticalResistorCount == 2)
		{
			Debug.Log ("2 vertical resistors");

			//Assigning the isolated resistor based on its rotation
			foreach (Transform element in selectedResistor)
			{
				if (element.rotation.z == 0) 
				{
					isolatedResistor = element;

				} else
				{
					nonIsolatedResistors.Add (element);
				}
			}

			//Assigning the position of one of the terminal ends of the isolated resistor to a variable, which will be used later on
			foreach (Transform i in isolatedResistor.GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor" && !found1)
				{
					leftConnect = i.position;
					found1 = true;
				} else if (i.parent.tag == "Wire" && !found1)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
				}
			}
			//Assigning the other terminal end position of the isolated resistor to a variable, which will connect a wire with the other assigned position 
			foreach (Transform i in isolatedResistor.GetComponent<ResistorUtility>().componentsAtright)
			{
				if (i.parent.tag == "Resistor" && !found2)
				{
					rightConnect = i.position;
					found2 = true;
				} else if (i.parent.tag == "Wire" && !found2)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
				}
			}
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (leftConnect, rightConnect);

			//Rotate the isolated resistor to match the other two resistor rotations
			isolatedResistor.Rotate (0f, 0f, 270f);
			//Obtain new position coordinates for the isolated resistor, based on the other two selected resistors
			if (isolatedResistor.position.x > nonIsolatedResistors [0].position.x)
			{
				//Case 1
				distanceToMove = -Vector3.Distance (isolatedResistor.position, nonIsolatedResistors [0].position) * 2f;
				commonNode = isolatedResistor.GetChild (0).position;
			} else if (isolatedResistor.position.x < nonIsolatedResistors [0].position.x)
			{
				//Case 2
				distanceToMove = Vector3.Distance (isolatedResistor.position, nonIsolatedResistors [0].position) * 2f;
				commonNode = isolatedResistor.GetChild (1).position;
			} else
			{
				Debug.Log ("Configuration error");
				return;
			}
			//Apply new position values to the isolated resistors
			isolatedResistor.Translate (0f, distanceToMove, 0f);
			wireA.Add (isolatedResistor.GetChild (0).position);
			wireB.Add (isolatedResistor.GetChild (1).position);

			//Connect the ends of the isolated resistors with the other two resistors and check wires don't overlap with anything (if possible)
			foreach (Transform element in nonIsolatedResistors)
			{
				if (!element.GetChild (0).GetComponent<BoxCollider2D> ().OverlapPoint (commonNode) && !leftFound)
				{
					wireA.Add (element.GetChild (0).position);
					leftFound = true;
				} else if (!element.GetChild (1).GetComponent<BoxCollider2D> ().OverlapPoint (commonNode) && !rightFound)
				{
					wireB.Add (element.GetChild (1).position);
					rightFound = true;
				}
			}
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (wireA [1], wireA [0]);
            GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (wireB [0], wireB [1]);
		} 
		//Second confguration case where there is only 1 vertical resistor, contains the same logic as the last section of code for 2 vertical resistors, with changes to
		//position and rotation
		else if (verticalResistorCount == 1)
		{
			Debug.Log ("2 Horizontal Resistors");
			foreach (Transform element in selectedResistor)
			{
				if (element.rotation.z != 0)
				{
					isolatedResistor = element;

				} else
				{
					nonIsolatedResistors.Add (element);
				}
			}

			foreach (Transform i in isolatedResistor.GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor" && !found1)
				{
					leftConnect = i.position;
					found1 = true;
				} else if (i.parent.tag == "Wire" && !found1)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
				}
			}
			foreach (Transform i in isolatedResistor.GetComponent<ResistorUtility>().componentsAtright)
			{
				if (i.parent.tag == "Resistor" && !found2)
				{
					rightConnect = i.position;
					found2 = true;
				} else if (i.parent.tag == "Wire" && !found2)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
				}
			}
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (leftConnect, rightConnect);

			isolatedResistor.Rotate (0f, 0f, -270f);

			if (isolatedResistor.position.y > nonIsolatedResistors [0].position.y)
			{
				//Case 1
				distanceToMove = -Vector3.Distance (isolatedResistor.position, nonIsolatedResistors [0].position) * 2f;
				commonNode = isolatedResistor.GetChild (1).position;
			} else if (isolatedResistor.position.y < nonIsolatedResistors [0].position.y)
			{
				//Case 2
				distanceToMove = Vector3.Distance (isolatedResistor.position, nonIsolatedResistors [0].position) * 2f;
				commonNode = isolatedResistor.GetChild (0).position;
			} else
			{
				Debug.Log ("Configuration error");
				return;
			}

			isolatedResistor.Translate (0f, distanceToMove, 0f);
			Vector3 checkSpace = isolatedResistor.position;
			var collisions = Physics2D.OverlapCircleAll(checkSpace, 0.03f);
			if (collisions.Length > 1)
			{
				isolatedResistor.Translate (0f, distanceToMove, 0f);	
			} 

			wireA.Add (isolatedResistor.GetChild (0).position);
			wireB.Add (isolatedResistor.GetChild (1).position);

			foreach (Transform element in nonIsolatedResistors)
			{
				if (!element.GetChild (0).GetComponent<BoxCollider2D> ().OverlapPoint (commonNode) && !leftFound)
				{
					wireA.Add (element.GetChild (0).position);
					leftFound = true;
				} else if (!element.GetChild (1).GetComponent<BoxCollider2D> ().OverlapPoint (commonNode) && !rightFound)
				{
					wireB.Add (element.GetChild (1).position);
					rightFound = true;
				}
			}
               
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (wireA [0], wireA [1]);
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (wireB [0], wireB [1]); 
		} 
		//Third confguration case where there are no vertical resistor, contains the same base logic as the last section of code for 2 vertical resistors, with changes to
		//position and rotation as well as handling the translation process
		else if (verticalResistorCount == 0)
		{
			int similarY = 0;
			float setX = 0;
			float setY = 0;
			float indexY = selectedResistor [0].position.y;
			var shiftOver = new GameObject ().transform;
			Vector3 leftTerminal = new Vector3 (0f, 0f, 0f); 
			Vector3 rightTerminal = new Vector3 (0f, 0f, 0f);
			List<Transform> otherResistors = new List<Transform> ();

			foreach (Transform resistor in selectedResistor)
			{
				if ((resistor.position.y < indexY + tol) && (resistor.position.y > indexY - tol))
				{
					similarY++;
				}
			}
			if (similarY == 1)
			{
				foreach (Transform resistor in selectedResistor)
				{
					if ((resistor.position.y < indexY + tol) && (resistor.position.y > indexY - tol))
					{
						shiftOver = resistor;
					}
					else
					{
						otherResistors.Add (resistor);
					}
				}
			}
			else if (similarY == 2)
			{
				foreach (Transform resistor in selectedResistor)
				{
					if ((resistor.position.y < indexY + tol) && (resistor.position.y > indexY - tol))
					{
						otherResistors.Add (resistor);
					}
					else
					{
						shiftOver = resistor;
					}
				}
			}
			foreach (Transform i in shiftOver.GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor" && !found1)
				{
					leftConnect = i.position;
					found1 = true;
				} else if (i.parent.tag == "Wire" && !found1)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
				}
			}
			foreach (Transform i in shiftOver.GetComponent<ResistorUtility>().componentsAtright)
			{
				if (i.parent.tag == "Resistor" && !found2)
				{
					rightConnect = i.position;
					found2 = true;
				} else if (i.parent.tag == "Wire" && !found2)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
				}
			}
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (leftConnect, rightConnect);

			if (shiftOver.position.y > otherResistors [0].position.y)
			{
				setY = otherResistors [0].position.y - 0.58f;
			}
			else if (shiftOver.position.y < otherResistors [0].position.y)
			{
				setY = otherResistors [0].position.y + 0.58f;
			}



			setX = (otherResistors [0].position.x + otherResistors [1].position.x)/2 ;
			shiftOver.position = new Vector3 (setX, 1.25f*setY, shiftOver.position.z);

			Vector3 checkSpace = shiftOver.position;
			var collisions = Physics2D.OverlapCircleAll(checkSpace, 0.03f);
			if (collisions.Length > 1)
			{
				shiftOver.Translate (0f, setY*0.3f, 0f);	
			} 

			if (otherResistors [0].position.x < otherResistors [1].position.x)
			{
				leftTerminal = otherResistors [0].GetChild (0).position;
				rightTerminal = otherResistors [1].GetChild (1).position;
			} 
			else
			{
				leftTerminal = otherResistors [1].GetChild (0).position;
				rightTerminal = otherResistors [0].GetChild (1).position;	
			}

			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (shiftOver.GetChild(0).position, leftTerminal);
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (shiftOver.GetChild(1).position, rightTerminal);
		}
		//Last confguration case where there are three vertical resistor, contains the same base logic as the last section of code for 2 vertical resistors, with changes to
		//position and rotation as well as handling the translation process
		else if (verticalResistorCount == 3)
		{
			int similarX = 0;
			float setX = 0;
			float setY = 0;
			float indexX = selectedResistor [0].position.x;
			var shiftOver = new GameObject ().transform;
			Vector3 leftTerminal = new Vector3 (0f, 0f, 0f); 
			Vector3 rightTerminal = new Vector3 (0f, 0f, 0f);
			List<Transform> otherResistors = new List<Transform> ();

			foreach (Transform resistor in selectedResistor)
			{
				if ((resistor.position.x < indexX + tol) && (resistor.position.x > indexX - tol))
				{
					similarX++;
				}
			}
			if (similarX == 1)
			{
				foreach (Transform resistor in selectedResistor)
				{
					if ((resistor.position.x < indexX + tol) && (resistor.position.x > indexX - tol))
					{
						shiftOver = resistor;
					}
					else
					{
						otherResistors.Add (resistor);
					}
				}
			}
			else if (similarX == 2)
			{
				foreach (Transform resistor in selectedResistor)
				{
					if ((resistor.position.x < indexX + tol) && (resistor.position.x > indexX - tol))
					{
						otherResistors.Add (resistor);
					}
					else
					{
						shiftOver = resistor;
					}
				}
			}
			foreach (Transform i in shiftOver.GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor" && !found1)
				{
					leftConnect = i.position;
					found1 = true;
				} else if (i.parent.tag == "Wire" && !found1)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								leftConnect = j.position;
								found1 = true;
							}
						}
					}
				}
			}
			foreach (Transform i in shiftOver.GetComponent<ResistorUtility>().componentsAtright)
			{
				if (i.parent.tag == "Resistor" && !found2)
				{
					rightConnect = i.position;
					found2 = true;
				} else if (i.parent.tag == "Wire" && !found2)
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
					else if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.tag == "Resistor")
							{
								rightConnect = j.position;
								found2 = true;
							}
						}
					}
				}
			}
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (leftConnect, rightConnect);

			if (shiftOver.position.x > otherResistors [0].position.x)
			{
				setX = otherResistors [0].position.x - 0.58f;
			}
			else if (shiftOver.position.x < otherResistors [0].position.x)
			{
				setX = otherResistors [0].position.x + 0.58f;
			}

			setY = (otherResistors [0].position.y + otherResistors [1].position.y)/2 ;
			shiftOver.position = new Vector3 (1.25f*setX, setY, shiftOver.position.z);

			Vector3 checkSpace = shiftOver.position;
			var collisions = Physics2D.OverlapCircleAll(checkSpace, 0.03f);
			if (collisions.Length > 1)
			{
				shiftOver.Translate (0f, setY*0.3f, 0f);	
			} 

			if (otherResistors [0].position.x < otherResistors [1].position.x)
			{
				leftTerminal = otherResistors [0].GetChild (0).position;
				rightTerminal = otherResistors [1].GetChild (1).position;
			} 
			else
			{
				leftTerminal = otherResistors [1].GetChild (0).position;
				rightTerminal = otherResistors [0].GetChild (1).position;	
			}

			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (shiftOver.GetChild(0).position, leftTerminal);
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (shiftOver.GetChild(1).position, rightTerminal);
		}

    }

	//Function to check if a coordinate position contains any box colliders within an area of radius 1
	void CheckSpace(Transform resistor, Vector3 position, float distance) 
	{
		var collisionOccurs = Physics2D.OverlapCircleAll(position, 1);
		if (collisionOccurs.Length > 1)
		{
			resistor.Translate (0f, distance, 0f);
			position = resistor.position;
			CheckSpace (resistor, position, distance);
		}
	}

	public void OnGUI()
	{
		//Based on flags raised during the checking of the code, display an string value, providing feedback to the user
		if (noSimplificationNeededFlag && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Incorrect number of selected resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((invalidResistorsSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Invalid selection of resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((validResistorSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Valid selection, performing transform");
			GUILabelCountdown -= Time.deltaTime;
		}
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
}
