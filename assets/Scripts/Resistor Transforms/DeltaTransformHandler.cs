﻿/*
 * Author: Josiah Martinez
 * Project 59
 */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class DeltaTransformHandler : MonoBehaviour {

	/* 
	 * This file handles everything related to the Delta transform
	 * It includes validity checking on the users selections of resistors and transform, as well as the reconfiguration of the circuit.
	 * Reconfiguration also involves the renaming of resistors and updating of ohm values 
	 */

	private bool noSimplificationNeededFlag = false;
	private bool invalidResistorsSelectedFlag = false;
	private bool validResistorSelectedFlag = false;
	private float GUILabelCountdown;
	public GameObject GameMaster;
	public GameObject TransformMaster;
	private Transform seriesResistor;
	public int DeltaToYResistorCount = 0;
	public string newTransform;

	private List<Transform> selectedResistor;

	public void TransformExcecution()
	{
		noSimplificationNeededFlag = false;
		invalidResistorsSelectedFlag = false;
		validResistorSelectedFlag = false;
		selectedResistor = new List<Transform>();

		//Obtains all gameobjects in the simulator tha have the tag resistor and store it into an array
		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");
		bool checkDone = false;
		int DeltaResistorCount;
		float ohm0 = 0f;
		float ohm1 = 0f;
		float ohm2 = 0f;

		//Checks all the resistors and adds selected resistors into an array
		for (int i = 0; i < resistorArray.Length; i++) 
		{
			if (resistorArray [i].GetComponent<SelectHandler> ().isSelected) 
			{
				selectedResistor.Add(resistorArray[i].transform);
			}
		}

		//Checks if only 3 resistors are selected as Delta only works with 3 resistors
		if (selectedResistor.Count != 3)
		{ 
			//0 or 1 resistors selected, raise an error flag
			noSimplificationNeededFlag = true;
			invalidResistorsSelectedFlag = false;
			validResistorSelectedFlag = false;
			GUILabelCountdown = 2f;
		} 
		else 
		{
			/*
			 * three resistors are selected, and connections are checked to see if resistor combination is a delta, by firstly checking there are more than one node
			 * otherwise the formation is interpreted as a series combination
			 */

			foreach (Transform resistor in selectedResistor)
			{
				if (resistor.GetComponent<ResistorUtility> ().componentsAtleft.Count == 1 && resistor.GetComponent<ResistorUtility> ().componentsAtleft [0].tag == "WireTerminal")
				{
					if (resistor.GetComponent<ResistorUtility> ().componentsAtleft [0].name == "BoxCol_1")
					{
						if (resistor.GetComponent<ResistorUtility> ().componentsAtleft [0].GetComponentInParent<WireUtility> ().resistorsAt2.Count < 2)
						{
							invalidResistorsSelectedFlag = true;
							GUILabelCountdown = 2f;
							return;
						}
					} else if (resistor.GetComponent<ResistorUtility> ().componentsAtleft [0].name == "BoxCol_2")
					{
						if (resistor.GetComponent<ResistorUtility> ().componentsAtleft [0].GetComponentInParent<WireUtility> ().resistorsAt1.Count < 2)
						{
							invalidResistorsSelectedFlag = true;
							GUILabelCountdown = 2f;
							return;
						}
					}
				} 
				else if (resistor.GetComponent<ResistorUtility> ().componentsAtleft.Count < 2)
				{
					invalidResistorsSelectedFlag = true;
					GUILabelCountdown = 2f;
					return;
				}

				if (resistor.GetComponent<ResistorUtility> ().componentsAtright.Count == 1 && resistor.GetComponent<ResistorUtility> ().componentsAtright[0].tag == "WireTerminal")
				{
					if (resistor.GetComponent<ResistorUtility> ().componentsAtright [0].name == "BoxCol_1")
					{
						if (resistor.GetComponent<ResistorUtility> ().componentsAtright [0].GetComponentInParent<WireUtility> ().resistorsAt2.Count < 2)
						{
							invalidResistorsSelectedFlag = true;
							GUILabelCountdown = 2f;
							return;
						}
					}
					else if (resistor.GetComponent<ResistorUtility> ().componentsAtright [0].name == "BoxCol_2")
					{
						if (resistor.GetComponent<ResistorUtility> ().componentsAtright [0].GetComponentInParent<WireUtility> ().resistorsAt1.Count < 2)
						{
							invalidResistorsSelectedFlag = true;
							GUILabelCountdown = 2f;
							return;
						}
					}
				}
				else if (resistor.GetComponent<ResistorUtility> ().componentsAtright.Count < 2)
				{
					invalidResistorsSelectedFlag = true;
					GUILabelCountdown = 2f;
					return;
				}
			}
			foreach (Transform resistor in selectedResistor)
			{
				//checking the components at the left terminal of this resistor and seeing if only 1 of the resistors it is connected to is selected
				DeltaResistorCount = 0;
				foreach (Transform component in resistor.GetComponent<ResistorUtility> ().componentsAtleft)
				{					
					if (component.tag == "WireTerminal")
					{
						if (component.name == "BoxCol_1")
						{
							foreach (Transform attach in component.GetComponentInParent<WireUtility>().resistorsAt2)
							{
								if (attach.parent.GetComponent<SelectHandler>().isSelected)
								{
									DeltaResistorCount++;
								}
							}
						} else if (component.name == "BoxCol_2")
						{
							foreach (Transform attach in component.GetComponentInParent<WireUtility>().resistorsAt1)
							{
								if (attach.parent.GetComponent<SelectHandler>().isSelected)									
								{
									DeltaResistorCount++;
								}
							}
						}
					} 
					else
					{
						if (component.parent.GetComponent<SelectHandler> ().isSelected)
						{
							DeltaResistorCount++;
						}	
					}
				}
				if (DeltaResistorCount != 1)
				{
					invalidResistorsSelectedFlag = true;
					GUILabelCountdown = 2f;
					return;
				}
				//checking the components at the right terminal of this resistor and seeing if only 1 of the resistors it is connected to is selected
				DeltaResistorCount = 0;
				foreach (Transform component in resistor.GetComponent<ResistorUtility> ().componentsAtright)
				{
					if (component.tag == "WireTerminal")
					{
						if (component.name == "BoxCol_1")
						{
							foreach (Transform attach in component.GetComponentInParent<WireUtility>().resistorsAt2)
							{
								if (attach.parent.GetComponent<SelectHandler>().isSelected)
								{
									DeltaResistorCount++;
								}
							}
						} 
						else if (component.name == "BoxCol_2")
						{
							foreach (Transform attach in component.GetComponentInParent<WireUtility>().resistorsAt1)
							{
								if (attach.parent.GetComponent<SelectHandler>().isSelected)
								{
									DeltaResistorCount++;
								}
							}
						}
					} 
					else
					{
						if (component.parent.GetComponent<SelectHandler> ().isSelected)
						{
							DeltaResistorCount++;
						}	
					}
				}
				if (DeltaResistorCount != 1)
				{
					invalidResistorsSelectedFlag = true;
					GUILabelCountdown = 2f;
					return;
				} 
			}

			/*
			 *Call function to perform reconfiguration of the circuit, changing the combination from a delta to a y
			 *Afterwards, recalculate the new values of the resistors according to the delta-y topology, and also send a string value
			 *that will be displayed in the Transform History Panel, whilst incrementing the transform counter
			 */
			PerformDeltaTransform (selectedResistor);
			float res1 = selectedResistor [0].GetComponent<ResistorUtility> ().ohm;
			float res2 = selectedResistor [1].GetComponent<ResistorUtility> ().ohm;
			float res3 = selectedResistor [2].GetComponent<ResistorUtility> ().ohm;
			selectedResistor [0].GetComponent<ResistorUtility> ().ohm = (res2 * res3)/(res1 + res2 + res3);
			selectedResistor [1].GetComponent<ResistorUtility> ().ohm = (res1 * res3)/(res1 + res2 + res3);
			selectedResistor [2].GetComponent<ResistorUtility> ().ohm = (res1 * res2)/(res1 + res2 + res3);
			TransformMaster.GetComponent<TransformHistoryHandler> ().StartRefresh ();
			TransformMaster.GetComponent<TransformHistoryHandler> ().Increment ();
			newTransform = "Resistors ";
			foreach (Transform resistor in selectedResistor)
			{
				newTransform = newTransform + resistor.name + " ";
			}
			newTransform = newTransform + " have been Delta to Y transformed to ";

			foreach (Transform i in selectedResistor)
			{
				i.name = "RΔ-y" + DeltaToYResistorCount.ToString();
				newTransform = newTransform + i.name + " ";
				DeltaToYResistorCount++;
				i.GetComponent<SelectHandler> ().isSelected = false;
			}
			TransformMaster.GetComponent<TransformHistoryHandler> ().RetrieveNewTransform (newTransform);

		}
	}

	//Function that performs the actual transformation
	private void PerformDeltaTransform(List<Transform> resistorList)
	{
		validResistorSelectedFlag = true;
		GUILabelCountdown = 2f;
		int resistorIs90 = 0;
		var shiftOver = new GameObject ().transform;
		List<Transform> selectedResistors = new List<Transform> ();
		float distance = 0;
		Vector3 open1 = new Vector3();
		Vector3 open2 = new Vector3();
		bool connect1 = false;
		bool connect2 = false;
		Vector3 terminal1 = new Vector3();
		Vector3 terminal2 = new Vector3();
		float tol = 0.1f;

		//Check to see what type of formation the resistors are in by checking how many of them are 90 degrees
		foreach (Transform resistor in resistorList)
		{
			if (resistor.rotation.z != 0)
			{
				resistorIs90++;
			}
		}

		//Below are each of the specific resistor reconfigurations for each specific case, which all share similar logic when transforming
		if (resistorIs90 == 1)
		{
			float setX = 0;
			float setY = 0;
			Vector3 clearArea = new Vector3 ();
			foreach (Transform resistor in resistorList)
			{
				//Assigning which resistor would be shifted over for new y configuration
				if (resistor.rotation.z != 0)
				{
					shiftOver = resistor;
				} else
				{
					selectedResistors.Add (resistor);
				}
			}

			//Assigning the position of one of the terminal ends of the shiftover resistor to a variable, which will be used later on
			foreach (Transform i in selectedResistors[0].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal1 = selectedResistors [0].GetChild (1).transform.position;
						connect1 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
				}
			}
			if (!connect1)
			{
				terminal1 = selectedResistors [0].GetChild (0).transform.position;
			}

			//Assigning the other terminal end position of the reference resistor to a variable, which will connect a wire with the other assigned position 
			foreach (Transform i in selectedResistors[1].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal2 = selectedResistors [1].GetChild (1).transform.position;
						connect2 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [0].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [0].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
				}
			}
			if (!connect2)
			{
				terminal2 = selectedResistors [1].GetChild (0).transform.position;
			}

			//Reposition the shiftover resistor according to the other selected resistors, and rotate it 
			shiftOver.Rotate (0f, 0f, -270f);
			if (shiftOver.position.x > selectedResistors [0].position.x)
			{
				setX = selectedResistors [0].position.x - 1.13f;
				if (selectedResistors [0].position.y < selectedResistors [1].position.y)
				{
					setY = selectedResistors [1].position.y;	
					clearArea = new Vector3 (setX, selectedResistors[0].position.y, 0f);
				} else if (selectedResistors [0].position.y > selectedResistors [1].position.y)
				{
					setY = selectedResistors [0].position.y;
					clearArea = new Vector3 (setX, selectedResistors[1].position.y, 0f);
				}
			}
			else if (shiftOver.position.x < selectedResistors [0].position.x)
			{
				setX = selectedResistors [0].position.x + 1.13f;
				if (selectedResistors [0].position.y < selectedResistors [1].position.y)
				{
					setY = selectedResistors [1].position.y;	
					clearArea = new Vector3 (setX, selectedResistors[0].position.y, 0f);
				} else if (selectedResistors [0].position.y > selectedResistors [1].position.y)
				{
					setY = selectedResistors [0].position.y;
					clearArea = new Vector3 (setX, selectedResistors[1].position.y, 0f);
				}
			}
			shiftOver.position = new Vector3 (setX, setY, shiftOver.position.z);

			//Connect the ends of the shiftover resistors with the other two selected resistors, whilst positioning the wires in such a way that no collision occurs
			Vector3 checkFree = (terminal1 + terminal2) / 2;
			var collisionOccurs = Physics2D.OverlapCircleAll(checkFree, 0.03f);
			if (collisionOccurs.Length == 0)
			{
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (terminal1, terminal2);
			}
			var collisionOccurs2 = Physics2D.OverlapCircleAll (clearArea, 0.05f);
			if (collisionOccurs2.Length != 0)
			{
				Debug.Log ("here");
				for (int i = 0; i < collisionOccurs2.Length; i++)
				{
					DestroyObject (collisionOccurs2[i].transform.parent.gameObject);
				}
			}
			//Obtain all wire objects in game space
			GameObject[] wireArray = GameObject.FindGameObjectsWithTag ("Wire");
			//check all wires and see if they collide with the shiftover resistor, and if they do, destroy that object
			foreach (GameObject wire in wireArray)
			{
				if (shiftOver.GetComponent<BoxCollider2D> ().bounds.Intersects (wire.transform.GetChild (2).GetComponent<BoxCollider2D> ().bounds))
				{
					open1 = wire.transform.GetChild (0).position;
					open2 = wire.transform.GetChild (1).position;
					DestroyObject (wire);
				}
			}
			//Connect a wire to form the Y formation across the two other selected resistors
			if (shiftOver.GetChild (0).GetComponent<BoxCollider2D> ().bounds.Contains (open1))
			{
				open1 = shiftOver.GetChild (1).position;
			} else if (shiftOver.GetChild (0).GetComponent<BoxCollider2D> ().bounds.Contains (open2))
			{
				open2 = shiftOver.GetChild (1).position;
			} else if (shiftOver.GetChild (1).GetComponent<BoxCollider2D> ().bounds.Contains (open1))
			{
				open1 = shiftOver.GetChild (0).position;
			} else if (shiftOver.GetChild (1).GetComponent<BoxCollider2D> ().bounds.Contains (open2))
			{
				open2 = shiftOver.GetChild (0).position;
			}
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (open1, open2);


		} else if (resistorIs90 == 2)
		{
			//this section of code is very similar to the last section, with changes in regards to axis and rotation
			float setX = 0;
			float setY = 0;
			Vector3 clearArea = new Vector3 ();
			foreach (Transform resistor in resistorList)
			{
				if (resistor.rotation.z == 0)
				{
					shiftOver = resistor;
				} else
				{
					selectedResistors.Add (resistor);
				}
			}

			foreach (Transform i in selectedResistors[0].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal1 = selectedResistors [0].GetChild (1).transform.position;
						connect1 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
				}
			}
			if (!connect1)
			{
				terminal1 = selectedResistors [0].GetChild (0).transform.position;
			}

			foreach (Transform i in selectedResistors[1].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal2 = selectedResistors [1].GetChild (1).transform.position;
						connect2 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [1].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [1].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
				}
			}
			if (!connect2)
			{
				terminal2 = selectedResistors [1].GetChild (0).transform.position;
			}
			shiftOver.Rotate (0f, 0f, 270f);
			if (shiftOver.position.y > selectedResistors [0].position.y)
			{
				setY = selectedResistors [0].position.y - 1.13f;
				if (selectedResistors [0].position.x < selectedResistors [1].position.x)
				{
					setX = selectedResistors [0].position.x;
					clearArea = new Vector3 (selectedResistors [1].position.x, setY, 0f);
				} else if (selectedResistors [0].position.x > selectedResistors [1].position.x)
				{
					setX = selectedResistors [1].position.x;
					clearArea = new Vector3 (selectedResistors [0].position.x, setY, 0f);
				}
			}
			else if (shiftOver.position.y < selectedResistors [0].position.y)
			{
				setY = selectedResistors [0].position.y + 1.13f;
				if (selectedResistors [0].position.x < selectedResistors [1].position.x)
				{
					setX = selectedResistors [0].position.x;	
					clearArea = new Vector3 (selectedResistors [1].position.x, setY, 0f);
				} else if (selectedResistors [0].position.x > selectedResistors [1].position.x)
				{
					setX = selectedResistors [1].position.x;
					clearArea = new Vector3 (selectedResistors [0].position.x, setY, 0f);
				}
			}
			shiftOver.position = new Vector3 (setX, setY, shiftOver.position.z);
			Vector3 checkFree = (terminal1 + terminal2) / 2;
			var collisionOccurs = Physics2D.OverlapCircleAll(checkFree, 0.03f);
			if (collisionOccurs.Length == 0)
			{
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (terminal1, terminal2);
			}
			var collisionOccurs2 = Physics2D.OverlapCircleAll (clearArea, 0.05f);
			if (collisionOccurs2.Length != 0)
			{
				for (int i = 0; i < collisionOccurs2.Length; i++)
				{
					DestroyObject (collisionOccurs2[i].transform.parent.gameObject);
				}
			}
			GameObject[] wireArray = GameObject.FindGameObjectsWithTag ("Wire");
			foreach (GameObject wire in wireArray)
			{
				if (shiftOver.GetComponent<BoxCollider2D> ().bounds.Intersects (wire.transform.GetChild (2).GetComponent<BoxCollider2D> ().bounds))
				{
					open1 = wire.transform.GetChild (0).position;
					open2 = wire.transform.GetChild (1).position;
					DestroyObject (wire);
				}
			}

			if (shiftOver.GetChild (0).GetComponent<BoxCollider2D> ().bounds.Contains (open1))
			{
				open1 = shiftOver.GetChild (1).position;
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (open1, open2);
			} else if (shiftOver.GetChild (0).GetComponent<BoxCollider2D> ().bounds.Contains (open2))
			{
				open2 = shiftOver.GetChild (1).position;
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (open1, open2);
			} else if (shiftOver.GetChild (1).GetComponent<BoxCollider2D> ().bounds.Contains (open1))
			{
				open1 = shiftOver.GetChild (0).position;
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (open1, open2);
			} else if (shiftOver.GetChild (1).GetComponent<BoxCollider2D> ().bounds.Contains (open2))
			{
				open2 = shiftOver.GetChild (0).position;
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (open1, open2);
			} 

		} else if (resistorIs90 == 0)
		{
			int similarY = 0;
			float setX = 0;
			float setY = 0;
			float indexY = resistorList [0].position.y;
			Vector3 clearArea = new Vector3 ();
			//Determining the shiftover resistor, by checking which of the three resistors isn't inline with the other two
			foreach (Transform resistor in resistorList)
			{
				if ((resistor.position.y < indexY + tol) && (resistor.position.y > indexY - tol))
				{
					similarY++;
				}
			}

			if (similarY == 1)
			{
				foreach (Transform resistor in resistorList)
				{
					if ((resistor.position.y < indexY + tol) && (resistor.position.y > indexY - tol))
					{
						shiftOver = resistor;
					}
					else
					{
						selectedResistors.Add (resistor);
					}
				}
			}
			else if (similarY == 2)
			{
				foreach (Transform resistor in resistorList)
				{
					if ((resistor.position.y < indexY + tol) && (resistor.position.y > indexY - tol))
					{
						selectedResistors.Add (resistor);
					}
					else
					{
						shiftOver = resistor;
					}
				}
			}
			//Assigning the position of one of the terminal ends of the shiftover resistor, which will be used later on
			foreach (Transform i in selectedResistors[0].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal1 = selectedResistors [0].GetChild (1).transform.position;
						connect1 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
				}
			}
			if (!connect1)
			{
				terminal1 = selectedResistors [0].GetChild (0).transform.position;
			}
			//Assigning the other terminal end position of the reference resistor, which will connect a wire with the other assigned position 
			foreach (Transform i in selectedResistors[1].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal2 = selectedResistors [1].GetChild (1).transform.position;
						connect2 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [0].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [0].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
				}
			}
			if (!connect2)
			{
				terminal2 = selectedResistors [1].GetChild (0).transform.position;
			}
			//set the new position for the shiftover resistors based on the other selected resistors
			if (shiftOver.position.y > selectedResistors [0].position.y)
			{
				setY = selectedResistors [0].position.y - 0.58f;
				if (selectedResistors [0].position.x < selectedResistors [1].position.x)
				{
					setX = selectedResistors [0].position.x + 0.52f;	
					clearArea = new Vector3 (selectedResistors [1].position.x, setY, 0f);
				} else if (selectedResistors [0].position.x > selectedResistors [1].position.x)
				{
					setX = selectedResistors [1].position.x + 0.52f;
					clearArea = new Vector3 (selectedResistors [0].position.x, setY, 0f);
				}
			}
			else if (shiftOver.position.y < selectedResistors [0].position.y)
			{
				setY = selectedResistors [0].position.y + 0.58f;
				if (selectedResistors [0].position.x < selectedResistors [1].position.x)
				{
					setX = selectedResistors [0].position.x + 0.52f;	
					clearArea = new Vector3 (selectedResistors [1].position.x, setY, 0f);
				} else if (selectedResistors [0].position.x > selectedResistors [1].position.x)
				{
					setX = selectedResistors [1].position.x + 0.52f;
					clearArea = new Vector3 (selectedResistors [0].position.x, setY, 0f);
				}
			}
			//Apply position change and rotate the shiftover resistor and connect the ends of the other selected resistor with the shiftover resistor
			shiftOver.position = new Vector3 (setX, setY, shiftOver.position.z);
			shiftOver.Rotate (0f, 0f, 270f);
			Vector3 checkFree = (terminal1 + terminal2) / 2;
			var collisionOccurs = Physics2D.OverlapCircleAll(checkFree, 0.03f);
			if (collisionOccurs.Length == 0)
			{
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (terminal1, terminal2);
			}
			var collisionOccurs2 = Physics2D.OverlapCircleAll (clearArea, 0.05f);
			if (collisionOccurs2.Length != 0)
			{
				Debug.Log (collisionOccurs2.Length);
				for (int i = 0; i < collisionOccurs2.Length; i++)
				{
					DestroyObject (collisionOccurs2[i].transform.parent.gameObject);
				}
			}
			//Also check that the space the new shiftover resistor is in to see if any wires occupy this area, otherwise delete that wire
			GameObject[] wireArray = GameObject.FindGameObjectsWithTag ("Wire");
			foreach (GameObject wire in wireArray)
			{
				if (shiftOver.GetComponent<BoxCollider2D> ().bounds.Intersects (wire.transform.GetChild (2).GetComponent<BoxCollider2D> ().bounds))
				{
					DestroyObject (wire);
				}

				if (((wire.GetComponentInParent<WireUtility> ().resistorsAt1.Contains(shiftOver.GetChild(0))) || (wire.GetComponentInParent<WireUtility> ().resistorsAt1.Contains(shiftOver.GetChild(1)))) && (wire.GetComponentInParent<WireUtility> ().resistorsAt1.Count == 1))
				{
					DestroyObject (wire);
				}
				if (((wire.GetComponentInParent<WireUtility> ().resistorsAt2.Contains(shiftOver.GetChild(0))) || (wire.GetComponentInParent<WireUtility> ().resistorsAt2.Contains(shiftOver.GetChild(1)))) && (wire.GetComponentInParent<WireUtility> ().resistorsAt2.Count == 1))
				{
					DestroyObject (wire);
				}
			}
		}else if (resistorIs90 == 3)
		{
			//This entire section of code is similar to the last section, with changes to axis and rotation
			int similarX = 0;
			float setX = 0;
			float setY = 0;
			float indexX = resistorList [0].position.x;
			Vector3 clearArea = new Vector3 ();
			foreach (Transform resistor in resistorList)
			{
				if ((resistor.position.x < indexX + tol) && (resistor.position.x > indexX - tol))
				{
					similarX++;
				}
			}

			if (similarX == 1)
			{
				foreach (Transform resistor in resistorList)
				{
					if ((resistor.position.x < indexX + tol) && (resistor.position.x > indexX - tol))
					{
						shiftOver = resistor;
					}
					else
					{
						selectedResistors.Add (resistor);
					}
				}
			}
			else if (similarX == 2)
			{
				foreach (Transform resistor in resistorList)
				{
					if ((resistor.position.x < indexX + tol) && (resistor.position.x > indexX - tol))
					{
						selectedResistors.Add (resistor);
					}
					else
					{
						shiftOver = resistor;
					}
				}
			}

			foreach (Transform i in selectedResistors[0].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal1 = selectedResistors [0].GetChild (1).transform.position;
						connect1 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal1 = selectedResistors [0].GetChild (1).transform.position;
								connect1 = true;
							}
						}	
					}
				}
			}
			if (!connect1)
			{
				terminal1 = selectedResistors [0].GetChild (0).transform.position;
			}

			foreach (Transform i in selectedResistors[1].GetComponent<ResistorUtility>().componentsAtleft)
			{
				if (i.parent.tag == "Resistor")
				{
					if (i.parent.name == shiftOver.name)
					{
						terminal2 = selectedResistors [1].GetChild (1).transform.position;
						connect2 = true;
					}
				} 
				else
				{
					if (i.name == "BoxCol_1")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt2)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [0].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
					if (i.name == "BoxCol_2")
					{
						foreach (Transform j in i.parent.GetComponent<WireUtility>().resistorsAt1)
						{
							if (j.parent.name == shiftOver.name)
							{
								terminal2 = selectedResistors [0].GetChild (1).transform.position;
								connect2 = true;
							}
						}	
					}
				}
			}
			if (!connect2)
			{
				terminal2 = selectedResistors [1].GetChild (0).transform.position;
			}

			if (shiftOver.position.x > selectedResistors [0].position.x)
			{
				setX = selectedResistors [0].position.x - 0.55f;
				setY = (selectedResistors [0].position.y + selectedResistors [1].position.y)/2;
			}
			else if (shiftOver.position.x < selectedResistors [0].position.x)
			{
				setX = selectedResistors [0].position.x + 0.6f;
				setY = (selectedResistors [0].position.y + selectedResistors [1].position.y)/2;
			}
			shiftOver.position = new Vector3 (setX, setY, shiftOver.position.z);
			shiftOver.Rotate (0f, 0f, -270f);
			Vector3 checkFree = (terminal1 + terminal2) / 2;
			var collisionOccurs = Physics2D.OverlapCircleAll(checkFree, 0.03f);
			if (collisionOccurs.Length == 0)
			{
				GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (terminal1, terminal2);
			}			
			GameObject[] wireArray = GameObject.FindGameObjectsWithTag ("Wire");
			foreach (GameObject wire in wireArray)
			{
				if (shiftOver.GetComponent<BoxCollider2D> ().bounds.Intersects (wire.transform.GetChild (2).GetComponent<BoxCollider2D> ().bounds))
				{
					DestroyObject (wire);
				}

				if (((wire.GetComponentInParent<WireUtility> ().resistorsAt1.Contains(shiftOver.GetChild(0))) || (wire.GetComponentInParent<WireUtility> ().resistorsAt1.Contains(shiftOver.GetChild(1)))) && (wire.GetComponentInParent<WireUtility> ().resistorsAt1.Count == 1))
				{
					DestroyObject (wire);
				}
				if (((wire.GetComponentInParent<WireUtility> ().resistorsAt2.Contains(shiftOver.GetChild(0))) || (wire.GetComponentInParent<WireUtility> ().resistorsAt2.Contains(shiftOver.GetChild(1)))) && (wire.GetComponentInParent<WireUtility> ().resistorsAt2.Count == 1))
				{
					DestroyObject (wire);
				}
			}
		}
	}

	public void OnGUI()
	{
		//Based on the flags that are set suring the running of this script, display an text on the GUI of the simulator to provide feedback
		if (noSimplificationNeededFlag && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Incorrect number of selected resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((invalidResistorsSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Invalid selection of resistors");
			GUILabelCountdown -= Time.deltaTime;
		}
		else if ((validResistorSelectedFlag) && GUILabelCountdown > 0f)
		{
			GUI.Label(new Rect(10, 10, 150, 150), "Valid selection, performing transform");
			GUILabelCountdown -= Time.deltaTime;
		}
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
}
