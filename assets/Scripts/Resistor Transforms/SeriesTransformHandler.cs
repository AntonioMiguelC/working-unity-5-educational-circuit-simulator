﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections;
using System.Collections.Generic;

public class SeriesTransformHandler : MonoBehaviour
{
	private bool noSimplificationNeededFlag = false;
    private bool invalidResistorsSelectedFlag = false;
	private bool validResistorSelectedFlag = false;
	private float GUILabelCountdown;
    public GameObject GameMaster;
	public GameObject TransformMaster;
    public int seriesResistorCount = 0;
	public string newTransform;

    private Transform seriesResistor;

    private List<Transform> selectedResistor;

	public void TransformExcecution()
	{
        noSimplificationNeededFlag = false;
        invalidResistorsSelectedFlag = false;
        validResistorSelectedFlag = false;
        selectedResistor = new List<Transform>();
		GameObject[] resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");

		for (int i = 0; i < resistorArray.Length; i++) 
		{
			if (resistorArray [i].GetComponent<SelectHandler> ().isSelected) 
			{
                selectedResistor.Add(resistorArray[i].transform);
			}
		}

        if (selectedResistor.Count < 2)
        { //0 or 1 resistors selected
            noSimplificationNeededFlag = true;
            invalidResistorsSelectedFlag = false;
            GUILabelCountdown = 2f;
        }
        else //if (selectedResistor.Count == 2)
		{
			noSimplificationNeededFlag = false;
            invalidResistorsSelectedFlag = false;
			GUILabelCountdown = 2f;

		   /*   Transformation algorithm goes here
            *
            *   Go through each selected resistor
            *   check if the terminal of the selected resistor is connected to exactly one thing (else break)
            *   if its a wire, make sure each terminal only has one element stored
            *   if its a resistor then its ok  
            */
            for(int k = 0; k < selectedResistor.Count - 1; k++)
            {
                //split them up into pairs
                List<Transform> temp = new List<Transform>();
                temp.Clear();
                temp.Add(selectedResistor[k]);
                temp.Add(selectedResistor[k + 1]);

                foreach(Transform i in temp) //looking at all selected resistors
                {
                    Debug.Log(i);
                    foreach (Transform leftElement in i.GetComponent<ResistorUtility>().componentsAtleft) //look at every component at the left terminal
                    {
                        if (leftElement.parent.tag == "Resistor") //if the component is a resistor
                        {
                            if ((leftElement.parent.GetComponent<SelectHandler>().isSelected) && (i.GetComponent<ResistorUtility>().componentsAtleft.Count > 1))
                            {
                                //More than one resistor is present at the terminal, therefore a series simplification cannot be done
                                invalidResistorsSelectedFlag = true;
                                validResistorSelectedFlag = false;
                                return;
                            }
                            else if ((leftElement.parent.GetComponent<SelectHandler> ().isSelected) && (i.GetComponent<ResistorUtility> ().componentsAtleft.Count == 1))
                            {
                                validResistorSelectedFlag = true;
                            }

                        }
                        else if (leftElement.tag == "WireTerminal") //if the component is a terminal
                        {
                            if (leftElement.name == "BoxCol_1")
                            {
                                if (leftElement.parent.GetComponent<WireUtility>().resistorsAt2.Count > 1) //if there are more than one component in this wire's terminal
                                {
                                    foreach (Transform wireElement in leftElement.parent.GetComponent<WireUtility>().resistorsAt2)
                                    {
                                        if (wireElement.parent.tag == "Resistor") //check if it is a selected resistor
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected)
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                invalidResistorsSelectedFlag = true;
                                                validResistorSelectedFlag = false;
                                                return;
                                            }
                                        }
                                    }
                                }
                                else if (leftElement.parent.GetComponent<WireUtility>().resistorsAt2.Count == 1)
                                {
                                    foreach (Transform wireElement in leftElement.parent.GetComponent<WireUtility>().resistorsAt2)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected)
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                validResistorSelectedFlag = true;
                                            }
                                        }
                                    }
                                }
                            }
                            else if (leftElement.name == "BoxCol_2")
                            {
                                if (leftElement.parent.GetComponent<WireUtility>().resistorsAt1.Count > 1)
                                {
                                    foreach (Transform wireElement in leftElement.parent.GetComponent<WireUtility>().resistorsAt1)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected)
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                invalidResistorsSelectedFlag = true;
                                                validResistorSelectedFlag = false;
                                                return;
                                            }
                                        }
                                    }
                                }
                                else if (leftElement.parent.GetComponent<WireUtility>().resistorsAt1.Count == 1)
                                {
                                    foreach (Transform wireElement in leftElement.parent.GetComponent<WireUtility>().resistorsAt1)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected)
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                validResistorSelectedFlag = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (Transform rightElement in i.GetComponent<ResistorUtility>().componentsAtright)
                    {
                        Debug.Log("Checking right terminal " + rightElement.parent.tag);
                        if (rightElement.parent.tag == "Resistor")
                        {                     
                            if ((rightElement.parent.GetComponent<SelectHandler>().isSelected) && (i.GetComponent<ResistorUtility>().componentsAtright.Count > 1)) //changed this to get parent component
                            {
                                Debug.Log("Invalid resistors selected flag set true");
                                invalidResistorsSelectedFlag = true;
                                validResistorSelectedFlag = false;
                                return;
                            }
                            else if ((rightElement.parent.GetComponent<SelectHandler>().isSelected) && (i.GetComponent<ResistorUtility>().componentsAtright.Count == 1))
                            {
                                validResistorSelectedFlag = true;
                            }
                        }
                        else if (rightElement.tag == "WireTerminal")
                        {
                            Debug.Log("Checks the wire terminals for right");
                            if (rightElement.name == "BoxCol_1")
                            {
                                if (rightElement.parent.GetComponent<WireUtility>().resistorsAt2.Count > 1)
                                {
                                    foreach (Transform wireElement in rightElement.parent.GetComponent<WireUtility>().resistorsAt2)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected && !wireElement.parent.Equals(i))
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                invalidResistorsSelectedFlag = true;
                                                validResistorSelectedFlag = false;
                                                return;
                                            }
                                        }
                                    }
                                }
                                else if (rightElement.parent.GetComponent<WireUtility>().resistorsAt2.Count == 1)
                                {
                                    foreach (Transform wireElement in rightElement.parent.GetComponent<WireUtility>().resistorsAt2)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected)
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                validResistorSelectedFlag = true;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (rightElement.parent.GetComponent<WireUtility>().resistorsAt1.Count > 1)
                                {
                                    foreach (Transform wireElement in rightElement.parent.GetComponent<WireUtility>().resistorsAt1)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected && !wireElement.parent.Equals(i))
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                Debug.Log ("wire that caused return: " + rightElement.parent.name);
                                                Debug.Log("Going through Resistor " + i.name);
                                                invalidResistorsSelectedFlag = true;
                                                validResistorSelectedFlag = false;
                                                return;
                                            }
                                        }
                                    }
                                }
                                else if (rightElement.parent.GetComponent<WireUtility>().resistorsAt1.Count == 1)
                                {
                                    foreach (Transform wireElement in rightElement.parent.GetComponent<WireUtility>().resistorsAt1)
                                    {
                                        if (wireElement.parent.tag == "Resistor")
                                        {
                                            if (wireElement.parent.GetComponent<SelectHandler>().isSelected)
                                            {
                                                Debug.Log("Invalid resistors selected flag set true");
                                                validResistorSelectedFlag = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                changeBoardState(temp);
				TransformMaster.GetComponent<TransformHistoryHandler> ().StartRefresh ();
				if (k == 0)
				{
					TransformMaster.GetComponent<TransformHistoryHandler> ().Increment ();
					TransformMaster.GetComponent<TransformHistoryHandler> ().RetrieveNewTransform (newTransform);
				}
            }
		}
	}

    private void changeBoardState(List<Transform> resPair)
    {
        Vector3 position1 = new Vector3();
        Vector3 position2 = new Vector3();

		newTransform = "Resistors ";
		foreach (Transform resistor in selectedResistor)
		{
			newTransform = newTransform + resistor.name + " ";
		}
		newTransform = newTransform + "have been series transformed into resistor ";

        //Transform happens here
        if (validResistorSelectedFlag)
        {
            //rename the first selected resistor. This will become the new resistor equivalent
            resPair [0].name = "Rseries" + seriesResistorCount.ToString ();
			newTransform = newTransform + resPair [0].name;
            seriesResistorCount++;
            seriesResistor = resPair [0];

			//resPair [0].GetComponent<ResistorUtility> ().ohm = resPair [0].GetComponent<ResistorUtility> ().ohm + resPair [1].GetComponent<ResistorUtility> ().ohm;
			float ohm = 0;
			foreach (Transform i in selectedResistor)
			{
				ohm += i.GetComponent<ResistorUtility> ().ohm;
			}
			resPair [0].GetComponent<ResistorUtility> ().ohm = ohm;

            position1 = resPair [resPair.Count - 1].GetChild (0).position;
            position2 = resPair [resPair.Count - 1].GetChild (1).position;
            for (int i = resPair.Count - 1; i > 0; i--)
            {
                //position1 = selectedResistor[i].GetChild(0).position;
                //position2 = selectedResistor[i].GetChild(1).position;
                foreach (Transform element in resPair[i].GetComponent<ResistorUtility>().componentsAtleft)
                {
                    Debug.Log (element.name);
                    if (element.tag == "WireTerminal")
                    {
                        Debug.Log ("Deleting Wire");
                        if (element.name == "BoxCol_1")
                        {
                            position1 = element.parent.GetChild (1).position;
                        } else if (element.name == "BoxCol_2")
                        {
                            position1 = element.parent.GetChild (0).position;   
                        }                        
                        //Destroy (element.parent.gameObject);
                    }

                }
                foreach (Transform element in resPair[i].GetComponent<ResistorUtility>().componentsAtright)
                {
                    Debug.Log (element.name);
                    if (element.tag == "WireTerminal")
                    {
                        Debug.Log ("Deleting Wire");
                        if (element.name == "BoxCol_1")
                        {
                            position2 = element.parent.GetChild (1).position;
                        } else if (element.name == "BoxCol_2")
                        {
                            position2 = element.parent.GetChild (0).position;   
                        }
                        //Destroy (element.parent.gameObject);
                    }
                }

                //Destroy(selectedResistor[i].GetComponent<ResistorUtility>().componentsAtleft.
                Destroy (resPair [i].gameObject);
            }
			GameMaster.GetComponent<ConnectWireHandler> ().ConnectWires (position1, position2);
            seriesResistor.GetComponent<SelectHandler> ().isSelected = false;
        }
    }

	public void OnGUI()
	{
        if (noSimplificationNeededFlag && GUILabelCountdown > 0f)
        {
            GUI.Label(new Rect(10, 10, 150, 150), "No simplification necessary with selected resistors.");
            GUILabelCountdown -= Time.deltaTime;
        }
		else if ((invalidResistorsSelectedFlag) && GUILabelCountdown > 0f)
        {
            GUI.Label(new Rect(10, 10, 150, 150), "Invalid selection of resistors");
            GUILabelCountdown -= Time.deltaTime;
        }
		else
		{
			GUI.Label (new Rect (10, 10, 150, 150), "");
		}
	}
        
}
