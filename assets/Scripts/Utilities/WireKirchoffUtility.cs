﻿/*
 * Author: Josiah Martinez
 * Project 59
 */ 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WireKirchoffUtility : MonoBehaviour 
{
	//This script is a copy of the WireUtility script but specific to the Kirchhoff level to prevent any sort of clashing and errors in the unity editor
	//Please refer to WireUtility.cs

    public List<Transform> resistorsAt1, resistorsAt2;
    public GameObject[] resistorArray;
    public GameObject[] terminalArray;
    public GameObject[] wireTerminalArray;
	public float potentialDifference;
	public float current;
	public int pdDirection;

    public bool connectedToEndNode = false;
    public bool newlySpawned = true;

    int BOXCOL1 = 0;
    int BOXCOL2 = 1;
    int WIRECOL = 2;
    int updateCount = 0;

    Transform wireTerminal_1;
    Transform wireTerminal_2;
    Transform wireCollider;

    Bounds wireTerminalBound_1;
    Bounds wireTerminalBound_2;

	// Use this for initialization
	void Start () 
    {
        PublicStart();
	}

    private bool isConnetedToEndNode()
    {
        //Wire terminals
        Transform wireTerm_0 = this.transform.GetChild(0);
        Transform wireTerm_1 = this.transform.GetChild(1);

        GameObject[] endNodeArray = GameObject.FindGameObjectsWithTag("EndNode");

        for (int i = 0; i < endNodeArray.Length; i++)
        {
            if (endNodeArray[i].transform.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerm_0.GetComponent<BoxCollider2D>().bounds) 
                || endNodeArray[i].transform.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerm_1.GetComponent<BoxCollider2D>().bounds))
            {
                return true;
            }
        }

        GameObject[] wireTerminalArray = GameObject.FindGameObjectsWithTag("WireTerminal");

        foreach (GameObject wireTerminal in wireTerminalArray)
        {
            if (wireTerminal.transform.parent.GetComponent<WireKirchoffUtility>().connectedToEndNode && ((wireTerm_0.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerminal.transform.GetComponent<BoxCollider2D>().bounds)) || 
                (wireTerm_1.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerminal.transform.GetComponent<BoxCollider2D>().bounds))))
                return true;
        }

        return false;
    }

	//Obtains the wire connections for each specific wire in the screen and adds it to one of two lists, depending on which end the object is connected to
    public void wireToWireConnection()
    {
        wireTerminalArray = GameObject.FindGameObjectsWithTag("WireTerminal");
        foreach (GameObject i in wireTerminalArray) //check each wire terminal
        {
            if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerminalBound_1) && (wireTerminal_1.parent.name != i.transform.parent.name))
            {
				if (i.transform.name == "BoxCol_1")
				{
					resistorsAt1.AddRange (i.transform.parent.GetComponent<WireKirchoffUtility> ().resistorsAt2);
				} 
				else if (i.transform.name == "BoxCol_2")
				{
					resistorsAt1.AddRange (i.transform.parent.GetComponent<WireKirchoffUtility> ().resistorsAt1);
				}

				//remove duplicates
				resistorsAt1 = resistorsAt1.Distinct().ToList();

            }
            else if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerminalBound_2) && (wireTerminal_2.parent.name != i.transform.parent.name))
            {
				if (i.transform.name == "BoxCol_1")
				{
					resistorsAt2.AddRange (i.transform.parent.GetComponent<WireKirchoffUtility> ().resistorsAt2);
				} 
				else if (i.transform.name == "BoxCol_2")
				{
					resistorsAt2.AddRange (i.transform.parent.GetComponent<WireKirchoffUtility> ().resistorsAt1);
				}

				//remove duplicates
				resistorsAt2 = resistorsAt2.Distinct().ToList();
            }
        }
    }

    public void PublicStart()
    {
        resistorsAt1.Clear();
        resistorsAt2.Clear();
        wireTerminal_1 = this.transform.GetChild(BOXCOL1);
        wireTerminal_2 = this.transform.GetChild(BOXCOL2);
        wireCollider = this.transform.GetChild(WIRECOL);

        wireTerminalBound_1 = wireTerminal_1.transform.GetComponent<BoxCollider2D>().bounds;
        wireTerminalBound_2 = wireTerminal_2.transform.GetComponent<BoxCollider2D>().bounds;

        resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
        terminalArray = GameObject.FindGameObjectsWithTag("Terminal");

        connectedToEndNode = isConnetedToEndNode();


        foreach (GameObject i in terminalArray) //check each non-wire component terminal
        {
            if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerminalBound_1))
            {
                resistorsAt1.Add(i.transform);
            }
            else if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(wireTerminalBound_2))
            {
                resistorsAt2.Add(i.transform);
            }
        }
        wireToWireConnection();

        if (updateCount == 10)
        {
            newlySpawned = false;
        }
        else
        {
            updateCount++;
        }

    }
	

}
