﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

/*This class handles the state machine to transition from the start menu to the levels and other menues. This also handles the pausing of the game to the termination of the software*/

public class StartMenuHandler : MonoBehaviour
{

	public Canvas quitWindow;
	public Button startButton;
	public Button helpButton;
	public Button exitButton;
    private bool escapeToggle;

	// Use this for initialization
	void Start ()
	{
		quitWindow = quitWindow.GetComponent<Canvas> ();
		startButton = startButton.GetComponent<Button> ();
		helpButton = helpButton.GetComponent<Button> ();
		exitButton = exitButton.GetComponent<Button> ();

		quitWindow.enabled = false;
	}

	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            quitWindow.enabled = !quitWindow.enabled;
        }

        if (quitWindow.enabled)
        {
            startButton.enabled = false;
            helpButton.enabled = false;
            exitButton.enabled = false;
        }
        else
        {
            startButton.enabled = true;
            helpButton.enabled = true;
            exitButton.enabled = true;
        }
	}

	public void StartPressed ()
	{
		SceneManager.LoadScene ("LevelSelect");

	}

	public void HelpPressed ()
	{
		//handle for help scene
		SceneManager.LoadScene ("InstructionIntro");
	}

	public void SettingsPressed ()
	{
		//handle for setting scene
	}

	public void ExitPressed ()
	{
		quitWindow.enabled = true;
		startButton.enabled = false;
		helpButton.enabled = false;
		exitButton.enabled = false;
	}

	public void ExitNoPressed ()
	{
		quitWindow.enabled = false;
		startButton.enabled = true;
		helpButton.enabled = true;
		exitButton.enabled = true;
	}

	public void ExitYesPressed ()
	{
		Application.Quit ();
	}
		
}
