﻿/*
 * Author: Antonio Castro and Josiah Martinez
 * Project 59
 */
using UnityEngine;
using System.Collections;

public class SelectHandler : MonoBehaviour
{
	//This file handles the selected behaviour of components, so when the user clicks on this object, it changes the appearance by displaying the second sprite attached to this
	//object

	public bool isSelected = false;
	public bool isSelectable = true; 
	public Sprite selectedSprite;
	public Sprite notSelectedSprite;

	void OnMouseDown()
	{
		//Change boolean value when object is clicked
		if (isSelectable && this.enabled) 
		{
			isSelected = !isSelected;
		}
	}

	void Update()
	{
		//Continuously updates the sprite of an object based on the selected boolean
		if(isSelected && this.tag != "Spawner") 
		{
			this.GetComponent<SpriteRenderer> ().sprite = selectedSprite;
		}
		else
		{
			this.GetComponent<SpriteRenderer> ().sprite = notSelectedSprite;
		}
	}

}

