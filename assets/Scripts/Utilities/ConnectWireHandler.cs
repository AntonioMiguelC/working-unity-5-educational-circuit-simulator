﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConnectWireHandler : MonoBehaviour {

	public GameObject wireObject;
	public Transform TableTop;
	private int wireCount;
	private Quaternion q;
	GameObject wirething;

	void Start()
	{
		wireCount = 0;
		q = new Quaternion(0,0,0,90f);
	}

	public void performWireConnect()
	{
		GameObject[] terminalsToConnect = GameObject.FindGameObjectsWithTag("Terminal");
		GameObject[] endNodesToConnect = GameObject.FindGameObjectsWithTag("EndNode");
		List<Transform> selectedTerminals = new List<Transform>();

		foreach (GameObject terminal in terminalsToConnect)
		{
			if (terminal.transform.GetComponent<SelectHandler>().isSelected)
			{
				selectedTerminals.Add(terminal.transform);
			}
		}

		foreach (GameObject endNode in endNodesToConnect)
		{
			if (endNode.transform.GetComponent<SelectHandler>().isSelected)
			{
				selectedTerminals.Add(endNode.transform);
			}
		}

		if (selectedTerminals.Count != 2)
		{
			Debug.Log("Not valid number of terminals selected");
			return;
		}
		else
		{
			this.ConnectWires(selectedTerminals[0].position, selectedTerminals[1].position);
		}

		foreach (Transform terminal in selectedTerminals)
		{
			terminal.transform.GetComponent<SelectHandler>().isSelected = false;
		}

	}

	public void ClearBoard()
	{
		GameObject[] resistors = GameObject.FindGameObjectsWithTag("Resistor");
		GameObject[] endNodesToConnect = GameObject.FindGameObjectsWithTag("EndNode");
		GameObject[] wires = GameObject.FindGameObjectsWithTag("Wire");

		foreach (GameObject resistor in resistors)
		{
			Destroy(resistor);
		}

		foreach (GameObject endNode in endNodesToConnect)
		{
			Destroy(endNode);
		}

		foreach (GameObject wire in wires)
		{
			Destroy(wire);
		}
	}

	public void ConnectWires(Vector3 term1, Vector3 term2)
	{

		Vector3 term1Pos = term1;
		Vector3 term2Pos = term2;
		//		wirething.transform.localScale = new Vector3 (distance, 1, 1);
		Vector3 midpoint = Vector3.Lerp (term1Pos, term2Pos, 0.5f);
		float distance = Vector3.Distance (term1Pos, term2Pos);

		if (distance < 0.1) //no need to spawn a wire if the distance is close enough that the colliders intersect.
		{
			return;
		}

		distance = distance / 0.05f;



		if ((term1Pos.x != term2Pos.x) && (term1Pos.y == term2Pos.y))
		{
			//Extend the wire so that it meets the terminal
			wirething = (GameObject)Instantiate (wireObject, midpoint, q);


			wirething.transform.localScale = new Vector3 (distance, 1, 1);
			BoxCollider2D[] wireColliders = wirething.GetComponentsInChildren<BoxCollider2D> ();

			float scale = 1 / distance;
			wireColliders [0].transform.position = term1Pos;
			wireColliders [1].transform.position = term2Pos;
			wireColliders [0].transform.localScale = new Vector3 (scale, 1f, 1f);
			wireColliders [1].transform.localScale = new Vector3 (scale, 1f, 1f);

			wireColliders [0].size = new Vector2 (0.17f, 0.17f);
			wireColliders [1].size = new Vector2 (0.17f, 0.17f);
			wireCount++;
		} 
		else if ( (term1Pos.y != term2Pos.y) && (term1Pos.x == term2Pos.x) )
		{
			//Extend the wire so that it meets the terminal
			wirething = (GameObject)Instantiate (wireObject, midpoint, q);

			wirething.transform.localScale = new Vector3 (1, distance, 1);
			BoxCollider2D[] wireColliders = wirething.GetComponentsInChildren<BoxCollider2D> ();

			float scale = 1 / distance;
			wireColliders [0].transform.position = term1Pos;
			wireColliders [1].transform.position = term2Pos;
			wireColliders [0].transform.localScale = new Vector3 (1f, scale, 1f);
			wireColliders [1].transform.localScale = new Vector3 (1f, scale, 1f);

			wireColliders [0].size = new Vector2 (0.17f, 0.17f);
			wireColliders [1].size = new Vector2 (0.17f, 0.17f);

			wireCount++;
		}
		else
		{
			Vector3 corner = new Vector3 (term1Pos.x, term2Pos.y, term1Pos.z);    

			var collisionOccurs = Physics2D.OverlapCircleAll(corner, 0.03f);
			//			Debug.Log ("Collisions in the area: " + collisionOccurs.Length);
			if (collisionOccurs.Length > 0)
			{
				//				Debug.Log ("Corner Changing");
				Vector3 cornerAlt = new Vector3 (term2Pos.x, term1Pos.y, term1Pos.z);
				var secondCollisionOccurs = Physics2D.OverlapCircleAll(cornerAlt, 0.03f);
				if (collisionOccurs.Length > secondCollisionOccurs.Length)
				{
					Debug.Log (collisionOccurs.Length +" "+ secondCollisionOccurs.Length);
					ConnectWires(term1, cornerAlt);
					ConnectWires(term2, cornerAlt); 
				}
				else
				{
					Debug.Log (collisionOccurs.Length +" "+ secondCollisionOccurs.Length);
					ConnectWires(term1, corner);
					ConnectWires(term2, corner);
				}
			} 
			else
			{				
				//At this point, the terminals dont share an x or y value
				ConnectWires(corner, term1);
				ConnectWires(corner, term2);
			}

		}

		//Name the wire
		wirething.transform.SetParent(this.transform);
		//		wirething.transform.Rotate (0f, 0f, 270f);
		wirething.transform.name = "Wire" + (wireCount - 1).ToString (); //we want to start naming at Wire0
		//Debug.Log ("connect wire complete");
	}
}