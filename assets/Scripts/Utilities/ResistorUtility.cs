﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ResistorUtility : MonoBehaviour {

    public List<Transform> componentsAtleft, componentsAtright;
    public GameObject[] resistorArray;
    public GameObject[] terminalArray;
    public GameObject[] wireTerminalArray;
    public Transform GameMaster;
	public bool hasEmptyTerminal;
	public float ohm;
	public float potentialDifference;
	public float leftPD;
	public float rightPD;
    public bool inCircuitCreatorEditMode;
    private bool changeResistorCredentialFlag = false;
    private string ohm_string;

	private List<float> eTwelve = new List<float>{10f, 12f, 15f, 18f, 22f, 27f, 33f, 39f, 47f, 56f, 68f, 82f};

    int LEFTCOL = 0;
    int RIGHTCOL = 1;
	bool escapePressed = false;
    Transform resTerminal_left;
    Transform resTerminal_right;

    Bounds resTerminalBound_left;
    Bounds resTerminalBound_right;

    // Use this for initialization
    void Start () 
    {
        GameMaster = this.transform.parent;
		hasEmptyTerminal = true;
		int randomNum = Random.Range (0, 12);
		ohm = eTwelve[randomNum];
        ohm_string = ohm.ToString();
		publicStart ();
    }

    public void wireToResistorConnection()
    {
        //Debug.Log("wireToResistorConnection");
        wireTerminalArray = GameObject.FindGameObjectsWithTag("WireTerminal");
        foreach (GameObject i in wireTerminalArray) //check each wire terminal
        {
            if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_left))
            {
                componentsAtleft.Add(i.transform);
            }
            else if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_right))
            {
                componentsAtright.Add(i.transform);
            }

        }
    }

	public void publicStart()
	{
		componentsAtleft.Clear ();
		componentsAtright.Clear ();
		resTerminal_left = transform.GetChild(LEFTCOL);
		resTerminal_right = transform.GetChild(RIGHTCOL);

		resTerminalBound_left = resTerminal_left.transform.GetComponent<BoxCollider2D>().bounds;
		resTerminalBound_right = resTerminal_right.transform.GetComponent<BoxCollider2D>().bounds;

		resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
		terminalArray = GameObject.FindGameObjectsWithTag("Terminal");

		foreach (GameObject i in terminalArray) //check each non-wire component terminal
		{
			if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_left) && (resTerminal_left.parent.name != i.transform.parent.name))
			{
				componentsAtleft.Add(i.transform);
				//Debug.Log(componentsAtleft.Count);
			}
			else if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_right) && (resTerminal_right.parent.name != i.transform.parent.name))
			{
				componentsAtright.Add(i.transform);
				//Debug.Log(componentsAtright.Count);
			}
		}

		wireToResistorConnection();
	}

    public void setName()
    {
        
    }

    public void setResistance(float resistanceValue)
    {
        this.ohm = resistanceValue;
    }

    void OnMouseOver()
    {
        if (inCircuitCreatorEditMode && !this.GetComponent<Draggable>().isBeingDragged)
        {
            if (Input.GetMouseButtonDown(1))
            {
                if (changeResistorCredentialFlag)
                {
                    changeResistorCredentialFlag = false;
                }
                else
                {
                    changeResistorCredentialFlag = true;
                }
            }
        }
    }

	void Update()
	{
		if(componentsAtleft.Count > 0 && componentsAtright.Count > 0)
		{
			hasEmptyTerminal = false;
		}
		else
		{
			hasEmptyTerminal = true;
		}

        if (GameMaster != null)
        {
            if (inCircuitCreatorEditMode)
            {
                
            }
            else
            {
                if (GameMaster.GetComponent<GameMasterControl>().pauseMenu.enabled)
                {
                    escapePressed = true;
                }
                else
                {
                    escapePressed = false;
                }
            }
        }
	}

	public void OnGUI()
	{	
		

		if (!escapePressed)
		{	
            GUI.color = Color.black;
			if (this.transform.rotation.z == 0)
			{
				float xPos = (this.GetComponent<Transform> ().position.x + 8.88f) * Screen.width / 17.76f;
				float yPos = (1f - ((this.GetComponent<Transform> ().position.y + 5.005f) / 10f)) * Screen.height;
				GUI.Label (new Rect (xPos - 20f, yPos - 28f, 200, 50), this.name);//GUI.Label (new Rect (xPos - 20f, yPos - 25f, 200, 50), this.name);
				GUI.Label (new Rect (xPos - 20f, yPos + 5f, 200, 50), this.ohm.ToString("n2") + "k");
			} else
			{
                float xPos = (this.GetComponent<Transform> ().position.x + 8.88f) * Screen.width / 17.76f;
                float yPos = (1f - ((this.GetComponent<Transform> ().position.y + 5.005f) / 10f)) * Screen.height;

				GUI.Label (new Rect (xPos + 15f, yPos - 17f, 200, 50), this.name);//GUI.Label (new Rect (xPos + 10f, yPos - 12f, 200, 50), this.name);
				GUI.Label (new Rect (xPos + 15f, yPos - 5f, 200, 50), this.ohm.ToString("n2") + "k");
			}

            if (changeResistorCredentialFlag)
            {
                float xPos = (this.GetComponent<Transform> ().position.x + 8.88f) * Screen.width / 17.76f;
                float yPos = (1f - ((this.GetComponent<Transform> ().position.y + 5.005f) / 10f)) * Screen.height;
                GUI.color = Color.white;
                this.name = GUI.TextField(new Rect(xPos + 10f, yPos - 17f, 100, 20), this.name);
                this.ohm_string = GUI.TextField(new Rect(xPos + 10f, yPos + 2f, 100, 20), this.ohm_string);
                this.ohm = float.Parse(this.ohm_string);
            }
		}
	}
}