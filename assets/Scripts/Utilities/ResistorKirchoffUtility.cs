﻿/*
 * Author: Josiah Martinez
 * Project 59
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResistorKirchoffUtility : MonoBehaviour {

    public List<Transform> componentsAtleft, componentsAtright;
    public GameObject[] resistorArray;
    public GameObject[] terminalArray;
    public GameObject[] wireTerminalArray;
	public float ohm;
	public float potentialDifference;
	public float current;
	public int pdDirection;
	public float leftPD;
	public float rightPD;

	private List<float> eTwelve = new List<float>{10f, 12f, 15f, 18f, 22f, 27f, 33f, 39f, 47f, 56f, 68f, 82f};

    int LEFTCOL = 0;
    int RIGHTCOL = 1;
	bool escapePressed = false;
    Transform resTerminal_left;
    Transform resTerminal_right;

    Bounds resTerminalBound_left;
    Bounds resTerminalBound_right;

    // Use this for initialization
    void Start () 
    {
		//Randomly choose a resistor value for each spawned resistor
		int randomNum = Random.Range (0, 12);
		ohm = eTwelve[randomNum];
		publicStart ();
    }

    public void wireToResistorConnection()
    {
        wireTerminalArray = GameObject.FindGameObjectsWithTag("WireTerminal");
        foreach (GameObject i in wireTerminalArray) //check each wire terminal
        {
            if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_left))
            {
                componentsAtleft.Add(i.transform);
            }
            else if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_right))
            {
                componentsAtright.Add(i.transform);
            }

        }
    }

	public void publicStart()
	{
		componentsAtleft.Clear ();
		componentsAtright.Clear ();
		resTerminal_left = transform.GetChild(LEFTCOL);
		resTerminal_right = transform.GetChild(RIGHTCOL);

		resTerminalBound_left = resTerminal_left.transform.GetComponent<BoxCollider2D>().bounds;
		resTerminalBound_right = resTerminal_right.transform.GetComponent<BoxCollider2D>().bounds;

		resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
		terminalArray = GameObject.FindGameObjectsWithTag("Terminal");

		foreach (GameObject i in terminalArray) //check each non-wire component terminal
		{
			if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_left) && (resTerminal_left.parent.name != i.transform.parent.name))
			{
				componentsAtleft.Add(i.transform);
			}
			else if (i.transform.GetComponent<BoxCollider2D>().bounds.Intersects(resTerminalBound_right) && (resTerminal_right.parent.name != i.transform.parent.name))
			{
				componentsAtright.Add(i.transform);
			}
		}

		wireToResistorConnection();
	}



	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			escapePressed = !escapePressed;
		}

	}

	//This provides much of the text for the components in the screen, displaying name of component (ohm for resistors, voltage for battery) and its value beside each component
	public void OnGUI()
	{	
		GUI.color = Color.black;

		if (!escapePressed)
		{	
			if (this.transform.rotation.z == 0)
			{
				float xPos = (this.GetComponent<Transform> ().position.x + 8.88f) * Screen.width / 17.76f;
				float yPos = (1f - ((this.GetComponent<Transform> ().position.y + 5.005f) / 10f)) * Screen.height;
				GUI.Label (new Rect (xPos - 20f, yPos - 28f, 200, 50), this.name);
				GUI.Label (new Rect (xPos - 20f, yPos + 5f, 200, 50), this.ohm.ToString("n2") + "k");
			} else
			{
				float xPos = (this.GetComponent<Transform> ().position.x + 8.88f) * Screen.width / 17.76f;
				float yPos = (1f - ((this.GetComponent<Transform> ().position.y + 5.005f) / 10f)) * Screen.height;
				GUI.Label (new Rect (xPos + 20f, yPos - 17f, 200, 50), this.name);
				GUI.Label (new Rect (xPos + 20f, yPos - 5f, 200, 50), this.ohm.ToString("n2") + "k");
			}	
		}
	}
}
