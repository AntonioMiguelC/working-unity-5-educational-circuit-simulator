﻿using UnityEngine;
using System.Collections;

public static class Utilities 
{
    public static GameObject[] resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
    public static GameObject[] wireArray = GameObject.FindGameObjectsWithTag("Wire");
}
