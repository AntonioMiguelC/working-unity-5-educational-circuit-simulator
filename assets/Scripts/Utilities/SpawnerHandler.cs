﻿using UnityEngine;
using System.Collections;

public class SpawnerHandler : MonoBehaviour {

	public GameObject spawnObject;
	private Vector2 currentScreenPoint;
	private Vector2 currentPosition;
	private Quaternion q = new Quaternion(0,0,0,90f);

	// Use this for initialization
	void Start () 
	{
		//nothing really needed here

	}

	void OnMouseDown()
	{
		if (enabled)
		{
			GameObject temp;
			GameObject[] resistorArray = GameObject.FindGameObjectsWithTag("Resistor");
			GameObject[] nodeArray = GameObject.FindGameObjectsWithTag("EndNode");
			Debug.Log("OnMouseDown");

			currentScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint);
			temp = (GameObject)Instantiate(spawnObject, new Vector3(currentPosition.x, currentPosition.y, 0f), q);
			temp.transform.GetComponent<Draggable>().enabled = true;
			temp.transform.SetParent(this.transform.parent);

			if (spawnObject.tag == "Resistor")
			{
                temp.transform.GetComponent<ResistorUtility>().inCircuitCreatorEditMode = true;
				temp.name = "R" + resistorArray.Length.ToString();
			}
			else if (spawnObject.tag == "EndNode")
			{
				temp.name = "Node " + nodeArray.Length.ToString();
			}
		}
	}
}