﻿using UnityEngine;
using System.Collections;
using System;

public class SpawnResistorHandler : MonoBehaviour 
{

	public Transform resistor;
	public Transform wire;
	public Transform wireGround;
	public Transform endNode;
	public GameObject[] otherTerminal;
	public GameObject[] terminalConx;
	public GameObject[] terminalEndPoints;

	public static bool currentIs90 = false;
	public static bool nearestIs90 = false;
	//                                                   -0.58f, 0.95f
	//private Vector3[] resistorCoordinates = {new Vector3(-1.13f, 1.54f, 0f), new Vector3(0.6f, 1.54f, 0f), new Vector3(0.62f, 0.41f, 0f), new Vector3(1.13f, 0.95f, 0f), new Vector3(1.75f, 1.53f, 0f), new Vector3(1.76f, 0.41f, 0f) };
	//private bool[] resistorOrientation = {false, false, false, true, false, false};
	//testing perpendicular config
	//private Vector3[] resistorCoordinates = {new Vector3(-0.26f, -0.69f, 0f), new Vector3(-0.25f, -1.83f, 0f), new Vector3(-1.38f, -1.83f, 0f), new Vector3(-1.38f, -0.71f, 0f), new Vector3(-0.78f, -1.26f, 0f) };
	//private bool[] resistorOrientation = {true, true, true, true, false};
	//normal testing
	private Vector3[] resistorCoordinates = {new Vector3(-4.13f, 1.54f, 0f), new Vector3(-1.13f, 1.54f, 0f), new Vector3(0.6f, 1.54f, 0f), new Vector3(0.62f, 0.41f, 0f), new Vector3(1.13f, 0.95f, 0f), new Vector3(1.75f, 1.53f, 0f), new Vector3(1.76f, 0.41f, 0f) };
	private bool[] resistorOrientation = {false, false, false, false, true, false, false};
	//these are just test values to see if each scenario for wire connection works
	//private Vector3[] resistorCoordinates = {new Vector3(-1.13f, 1.54f, 0f), new Vector3(-1.73f, -0.14f, 0f), new Vector3(0.6f, 1.54f, 0f), new Vector3(0.62f, 0.41f, 0f), new Vector3(1.13f, 0.95f, 0f), new Vector3(1.75f, 1.53f, 0f), new Vector3(1.76f, 0.41f, 0f) };
	//private bool[] resistorOrientation = {false, true, false, false, true, false, false};
	private GameObject[] resistorArray;
	private GameObject[] wireArray;
	private GameObject[] nodeArray;

	private GameObject[] selectedTerminals;

	private bool hasConnections = false;
	private int wireCount;
	private int nodeCount;

	private Quaternion q = new Quaternion(0,0,0,90f);

	void CheckOrientation(GameObject terminal1, GameObject terminal2)
	{
		if (terminal1.transform.parent.transform.rotation.z == 0) {
			currentIs90 = false;
//			Debug.Log ("Current Resistor is flat");
		} else {
			currentIs90 = true;
//			Debug.Log ("Current Resistor is 90 degrees");
		}

		if (terminal2.transform.parent.transform.rotation.z == 0) {
			nearestIs90 = false;
//			Debug.Log ("Other Resistor is flat");
		} else {
			nearestIs90 = true;
//			Debug.Log ("Other Resistor is 90 degrees");
		}
	}

	void InstantiateResistors(){
		resistorArray = GameObject.FindGameObjectsWithTag ("Resistor");

		for(int i = 0; i < resistorArray.Length; i++)
		{
			resistorArray [i].transform.SetParent(this.transform);
			name = "R" + i.ToString();
			resistorArray [i].name = name;
		}
	}

	void InstantiateGroundWire()
	{
		float stretch, xco, yco, wireScale;
		float minX = 0f;
		float minXY = 0f;
		float maxX = 0f;
		float maxXY = 0f;
		float minY = 6f;
		float tempY, tempX;

		for (int a = 0; a < otherTerminal.Length; a++)
		{
			if (otherTerminal [a].transform.position.x < minX)
			{
				minX = otherTerminal [a].transform.position.x; 
				minXY = otherTerminal [a].transform.position.y;
			}
			if (otherTerminal [a].transform.position.x > maxX)
			{
				maxX = otherTerminal [a].transform.position.x; 
				maxXY = otherTerminal [a].transform.position.y;
			}
			if (otherTerminal [a].transform.position.y < minY)
			{
				minY = otherTerminal [a].transform.position.y; 
			}
		}

		stretch = maxX - minX + 2f;
		xco = (minX + maxX)/2;
		yco = minY - 2f;
		wireScale = stretch / 0.05f;

		var instantiatedPrefabNode = Instantiate (endNode, new Vector3 (minX - 1f, yco, 0f), new Quaternion(0,0,0,90f)) as GameObject;
		nodeArray = GameObject.FindGameObjectsWithTag ("EndNode");
		for (int x = 0; x < nodeArray.Length; x++)
		{
			nodeArray [x].transform.SetParent (this.transform);
			if (x == nodeCount)
			{
				name = "Node" + x.ToString();
				nodeArray [x].name = name;
				nodeCount++;
			}
		}

		var instantiatedPrefabNode1 = Instantiate (endNode, new Vector3 (minX - 1f, minXY, 0f), new Quaternion(0,0,0,90f)) as GameObject;
		nodeArray = GameObject.FindGameObjectsWithTag ("EndNode");
		for (int x = 0; x < nodeArray.Length; x++)
		{
			nodeArray [x].transform.SetParent (this.transform);
			if (x == nodeCount)
			{
				name = "Node" + x.ToString();
				nodeArray [x].name = name;
				nodeCount++;
			}
		}

		var instantiatedPrefab = Instantiate (wireGround, new Vector3 (xco, yco, 0f), new Quaternion(0,0,0,90f)) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "WireGround";
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (wireScale, 1, 1); 
				wireCount++;
			}
		}

		stretch = 1f;
		tempX = minX - 0.5f;
		tempY = minXY;
		wireScale = stretch / 0.05f;
		var instantiatedPrefabWire1 = Instantiate (wireGround, new Vector3 (tempX, tempY, 0f), new Quaternion(0,0,0,90f)) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (wireScale, 1, 1); 
				wireCount++;
			}
		}

		stretch = maxXY - yco;
		tempY = (yco + maxXY)/2;
		tempX = maxX + 1f;
		wireScale = stretch / 0.05f;
		var instantiatedPrefab2 = Instantiate (wireGround, new Vector3 (tempX, tempY, 0f), new Quaternion(0,0,0,90f)) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "WireGroundR";
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (1, wireScale, 1); 
				wireCount++;
			}
		}

		stretch = 1f;
		tempX = maxX + 0.5f;
		tempY = maxXY;
		wireScale = stretch / 0.05f;
		var instantiatedPrefabWire2 = Instantiate (wireGround, new Vector3 (tempX, tempY, 0f), new Quaternion(0,0,0,90f)) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (wireScale, 1, 1); 
				wireCount++;
			}
		}
	}

	public void ConnectWires()
	{
		
		wireCount = 0;
		nodeCount = 0;
		Debug.Log ("Wire count is " + wireCount);
		terminalConx = new GameObject[25];
		otherTerminal = new GameObject[50];
		terminalEndPoints = new GameObject[10];
		resistorArray = new GameObject[30];
//		Array.Clear (otherTerminal,0,otherTerminal.Length);
//		Array.Clear (terminalConx,0,terminalConx.Length);
//		Array.Clear (terminalEndPoints,0,terminalEndPoints.Length);
		Debug.Log ("otherTerminal: " + otherTerminal.Length + ", terminalConx: " + terminalConx.Length + ", terminalEndPoints: " + terminalEndPoints.Length);
// need to figure out how to properly clear all the arrays properly

		InstantiateResistors ();

		//Array.Clear (wireArray,0,wireArray.Length);	
		//Array.Clear (nodeArray,0,nodeArray.Length);

		int counter = 0;
		otherTerminal = GameObject.FindGameObjectsWithTag ("Terminal");

		Debug.Log (otherTerminal.Length);
		for (int i = 0; i < otherTerminal.Length; i++) 
		{
			Debug.Log (otherTerminal [i].transform.parent.name + " " + otherTerminal[i]);
		}

		for (int i = 0; i < otherTerminal.Length; i++)
		{
			for (int j = 0; j < otherTerminal.Length; j++)
			{
				if (i == j)
				{
					continue;
				}
				else if (otherTerminal [i].GetComponent<BoxCollider2D> ().bounds.Intersects (otherTerminal [j].GetComponent<BoxCollider2D> ().bounds))
				{
					hasConnections = true;
					//counter++;
					Debug.Log(otherTerminal[i].transform.parent.name + " " + otherTerminal[i] + " connects to " + otherTerminal[j].transform.parent.name + " " + otherTerminal[j]);
				} 					
			}
			if (!hasConnections)
			{
				Debug.Log (otherTerminal [i].transform.parent.name + " " + otherTerminal [i] + " is not connected to anything");
				terminalConx [counter] = otherTerminal [i];
				counter++;
			}
			hasConnections = false;
		}

		for (int i = 0; i < terminalConx.GetLength(0); i++)
		{
			if (terminalConx [i] != null)
			{
				Debug.Log (terminalConx[i]);	
			}
		}

		float dist;
		float objScale;
		float xpos, ypos;
		Debug.Log (terminalConx.Length);
		for (int i = 0; i < terminalConx.Length - 1; i++)
		{
			for (int j = i + 1; j < terminalConx.Length; j++)
			{
				if (terminalConx[i]!=null&&terminalConx[j]!=null)
				{
					//				Debug.Log (terminalConx[i] + " " + terminalConx[i].transform.parent + " & " + terminalConx[j] + " " + terminalConx[j].transform.parent);
					CheckOrientation (terminalConx [i], terminalConx [j]);

					if (!currentIs90)
					{
						if (terminalConx [i].name == "LeftCollider")
						{
							if (terminalConx [j].transform.position.x < terminalConx [i].transform.position.x + 0.3f)
							{							
								if ((terminalConx [j].transform.position.x > terminalConx [i].transform.position.x - 0.3f) || ((terminalConx [j].transform.position.y < terminalConx [i].transform.position.y + 0.3f) && (terminalConx [j].transform.position.y > terminalConx [i].transform.position.y - 0.3f)))
								{
									if (!((!nearestIs90 && terminalConx [j].name == "LeftCollider" && ((terminalConx [j].transform.position.x > terminalConx [i].transform.position.x + 0.2f)||(terminalConx [j].transform.position.x < terminalConx [i].transform.position.x - 0.2f))) || (nearestIs90 && ((terminalConx [j].name == "LeftCollider" && terminalConx [j].transform.position.y > terminalConx [i].transform.position.y) || (terminalConx [j].name == "RightCollider" && terminalConx [j].transform.position.y < terminalConx [i].transform.position.y)))))
									{//issue here
										//Debug.Log (terminalConx[i].transform.parent + "enters here");
										if (terminalConx [j].transform.position.x < terminalConx [i].transform.position.x + 0.3f && terminalConx [j].transform.position.x > terminalConx [i].transform.position.x - 0.3f)
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											xpos = terminalConx [i].transform.position.x;
											ypos = (terminalConx [i].transform.position.y + terminalConx [j].transform.position.y) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);
												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (1, objScale, 1);
													wireCount++;
												}
											}
										} 
										else
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											ypos = terminalConx [i].transform.position.y;
											xpos = (terminalConx [i].transform.position.x + terminalConx [j].transform.position.x) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (objScale, 1, 1); 
													wireCount++;
												}
											}
										}
									}
								}
							}
						} else if (terminalConx [i].name == "RightCollider")
						{
							if (terminalConx [j].transform.position.x > terminalConx [i].transform.position.x - 0.3f)
							{							
								if ((terminalConx [j].transform.position.x < terminalConx [i].transform.position.x + 0.3f) || ((terminalConx [j].transform.position.y < terminalConx [i].transform.position.y + 0.3f) && (terminalConx [j].transform.position.y > terminalConx [i].transform.position.y - 0.3f)))
								{
									if (!((!nearestIs90 && terminalConx [j].name == "RightCollider" && ((terminalConx [j].transform.position.x > terminalConx [i].transform.position.x + 0.2f)||(terminalConx [j].transform.position.x < terminalConx [i].transform.position.x - 0.2f))) || (nearestIs90 && ((terminalConx [j].name == "LeftCollider" && terminalConx [j].transform.position.y > terminalConx [i].transform.position.y) || (terminalConx [j].name == "RightCollider" && terminalConx [j].transform.position.y < terminalConx [i].transform.position.y)))))
									{//issue here
										//Debug.Log (terminalConx[i].transform.parent + "enters here");
										if (terminalConx [j].transform.position.x < terminalConx [i].transform.position.x + 0.3f && terminalConx [j].transform.position.x > terminalConx [i].transform.position.x - 0.3f)
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											xpos = terminalConx [i].transform.position.x;
											ypos = (terminalConx [i].transform.position.y + terminalConx [j].transform.position.y) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (1, objScale, 1); 
													wireCount++;
												}
											}
										} 
										else
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											ypos = terminalConx [i].transform.position.y;
											xpos = (terminalConx [i].transform.position.x + terminalConx [j].transform.position.x) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (objScale, 1, 1); 
													wireCount++;
												}
											}
										}
									}
								}
							}
						}
					} else if (currentIs90)
					{
						if (terminalConx [i].name == "LeftCollider")
						{
							if (terminalConx [j].transform.position.y > terminalConx [i].transform.position.y - 0.3f)
							{							
								if ((terminalConx [j].transform.position.y < terminalConx [i].transform.position.y + 0.3f) || ((terminalConx [j].transform.position.x < terminalConx [i].transform.position.x + 0.3f) && (terminalConx [j].transform.position.x > terminalConx [i].transform.position.x + 0.3f)))
								{   //need to make an edit here to prevent overlap
									if (!((nearestIs90 && terminalConx [j].name == "LeftCollider" && ((terminalConx [j].transform.position.y > terminalConx [i].transform.position.y + 0.2f)||(terminalConx [j].transform.position.y < terminalConx [i].transform.position.y - 0.2f))) || (!nearestIs90 && ((terminalConx [j].name == "LeftCollider" && terminalConx [j].transform.position.x < terminalConx [i].transform.position.x) || (terminalConx [j].name == "RightCollider" && terminalConx [j].transform.position.x > terminalConx [i].transform.position.x)))))
									{//issue here
										//Debug.Log (terminalConx[i].transform.parent + "enters here");
										if (terminalConx [j].transform.position.y < terminalConx [i].transform.position.y + 0.3f && terminalConx [j].transform.position.y > terminalConx [i].transform.position.y - 0.3f)
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											ypos = terminalConx [i].transform.position.y;
											xpos = (terminalConx [i].transform.position.x + terminalConx [j].transform.position.x) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (objScale, 1, 1); 
													wireCount++;
												}
											}
										} 
										else
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											ypos = terminalConx [i].transform.position.y;
											xpos = (terminalConx [i].transform.position.x + terminalConx [j].transform.position.x) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (objScale, 1, 1); 
													wireCount++;
												}
											}
										}
									}
								}
							}
						} else if (terminalConx [i].name == "RightCollider")
						{
							if (terminalConx [j].transform.position.y < terminalConx [i].transform.position.y + 0.3f)
							{							
								if ((terminalConx [j].transform.position.y > terminalConx [i].transform.position.y - 0.3f) || ((terminalConx [j].transform.position.x < terminalConx [i].transform.position.x + 0.3f) && (terminalConx [j].transform.position.x > terminalConx [i].transform.position.x - 0.3f)))
								{
									if (!((nearestIs90 && terminalConx [j].name == "RightCollider" && terminalConx [j].transform.position.y < terminalConx [i].transform.position.y) || (!nearestIs90 && ((terminalConx [j].name == "LeftCollider" && terminalConx [j].transform.position.x < terminalConx [i].transform.position.x) || (terminalConx [j].name == "RightCollider" && terminalConx [j].transform.position.x > terminalConx [i].transform.position.x)))))
									{//issue here
										//Debug.Log (terminalConx[i].transform.parent + "enters here");
										if (terminalConx [j].transform.position.y < terminalConx [i].transform.position.y + 0.3f && terminalConx [j].transform.position.y > terminalConx [i].transform.position.y - 0.3f)
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											ypos = terminalConx [i].transform.position.y;
											xpos = (terminalConx [i].transform.position.x + terminalConx [j].transform.position.x) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (objScale, 1, 1); 
													wireCount++;
												}
											}
										} 
										else
										{
											dist = Vector2.Distance (terminalConx [i].transform.position, terminalConx [j].transform.position);
											ypos = terminalConx [i].transform.position.y;
											xpos = (terminalConx [i].transform.position.x + terminalConx [j].transform.position.x) / 2;
											objScale = dist / 0.05f;
											var instantiatedPrefab = Instantiate (wire, new Vector3 (xpos, ypos, 0f), q) as GameObject;
											wireArray = GameObject.FindGameObjectsWithTag ("Wire");
											for (int x = 0; x < wireArray.Length; x++)
											{
												wireArray [x].transform.SetParent (this.transform);

												if (x == wireCount)
												{
													name = "Wire" + x.ToString ();
													wireArray [x].name = name;
													wireArray [x].transform.localScale = new Vector3 (objScale, 1, 1); 
													wireCount++;
												}
											}
										}
									}
								}
							}
						}	
					}					
				}
			}
		}

		InstantiateGroundWire ();
	}

	//do the same for perpendicular resistors
	public void ConnectYParallelWires(){
		float lowerMinX, lowerMaxX, lowerY, upperMinX, upperMaxX, upperY;
		int countST = 0;
		float distWireY, xPosY, yPosY;
		float wideLeft, wideRight, high, low;

		selectedTerminals = new GameObject[6];
		otherTerminal = new GameObject[30];
		otherTerminal = GameObject.FindGameObjectsWithTag ("Terminal");

		for (int i = 0; i < otherTerminal.Length; i++)
		{
			if (GameObject.Find (otherTerminal[i].transform.parent.name).GetComponent<SelectHandler> ().isSelected)
			{
				selectedTerminals [countST] = otherTerminal [i];
				countST++;
			}
		}

		//something wrong here
		wideLeft = selectedTerminals [0].transform.position.x;
		wideRight = selectedTerminals [0].transform.position.x;
		high = selectedTerminals [0].transform.position.y;
		low = selectedTerminals [0].transform.position.y;

		for (int i = 1; i < selectedTerminals.Length; i++)
		{
			if (selectedTerminals [i].transform.position.x < wideLeft)
			{
				wideLeft = selectedTerminals [i].transform.position.x;
			}
			if (selectedTerminals [i].transform.position.x > wideRight)
			{
				wideRight = selectedTerminals [i].transform.position.x;
			}
			if (selectedTerminals [i].transform.position.y < low)
			{
				low = selectedTerminals [i].transform.position.y;
			}
			if (selectedTerminals [i].transform.position.y > high)
			{
				high = selectedTerminals [i].transform.position.y;
			}
		}

		Debug.Log (wideLeft + " " + wideRight + " " + high + " " + low);

		float centralX = (wideLeft + wideRight) / 2;
		float centralY = (high + low) / 2;
		lowerMinX = centralX;
		lowerMaxX = centralX;
		lowerY = centralY;
		upperMinX = centralX;
		upperMaxX = centralX;
		upperY = centralY;

		for (int i = 0; i < selectedTerminals.Length; i++)
		{
			if (selectedTerminals [i].transform.position.y < centralY)
			{
				lowerY = selectedTerminals [i].transform.position.y;
				if (selectedTerminals[i].transform.position.x < lowerMinX)
				{
					lowerMinX = selectedTerminals [i].transform.position.x;
				}
				if (selectedTerminals[i].transform.position.x > lowerMaxX)
				{
					lowerMaxX = selectedTerminals [i].transform.position.x;	
				}
			}
			if (selectedTerminals [i].transform.position.y > centralY)
			{
				upperY = selectedTerminals [i].transform.position.y;
				if (selectedTerminals[i].transform.position.x < upperMinX)
				{
					upperMinX = selectedTerminals [i].transform.position.x;
				}
				if (selectedTerminals[i].transform.position.x > upperMaxX)
				{
					upperMaxX = selectedTerminals [i].transform.position.x;	
				}
			}
		}

		distWireY = (upperY- lowerY)/0.05f;
		if (lowerMinX < upperMinX)
		{
			xPosY = lowerMinX;
		} 
		else
		{
			xPosY = upperMinX;	
		}
		yPosY = (lowerY + upperY)/2;
		var newWire1 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (1, distWireY, 1);
				wireCount++;
			}
		}

		if (lowerMaxX > upperMaxX)
		{
			xPosY = lowerMaxX;
		} 
		else
		{
			xPosY = upperMaxX;	
		}
		yPosY = (lowerY + upperY)/2;
		var newWire2 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (1, distWireY, 1);
				wireCount++;
			}
		}

		distWireY = (upperMaxX - lowerMaxX)/0.05f;
		if (lowerMaxX < upperMaxX)
		{
			yPosY = lowerY;
		} 
		else
		{
			yPosY = upperY;	
		}
		xPosY = (lowerMaxX + upperMaxX)/2;
		var newWire3 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (distWireY, 1, 1);
				wireCount++;
			}
		}

		distWireY = (upperMinX - lowerMinX)/0.05f;
		if (lowerMinX < upperMinX)
		{
			yPosY = upperY;
		} 
		else
		{
			yPosY = lowerY;	
		}
		xPosY = (lowerMinX + upperMinX)/2;
		var newWire4 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (distWireY, 1, 1);
				wireCount++;
			}
		}

		//replace original resistor with wire here

	}

	public void ConnectYPerpendicularWires(){
		float lowerMinY, lowerMaxY, lowerX, upperMinY, upperMaxY, upperX;
		int countST = 0;
		float distWireY, xPosY, yPosY;
		float wideLeft, wideRight, high, low;

		selectedTerminals = new GameObject[6];
		otherTerminal = new GameObject[30];
		otherTerminal = GameObject.FindGameObjectsWithTag ("Terminal");

		for (int i = 0; i < otherTerminal.Length; i++)
		{
			if (GameObject.Find (otherTerminal[i].transform.parent.name).GetComponent<SelectHandler> ().isSelected)
			{
				selectedTerminals [countST] = otherTerminal [i];
				countST++;
			}
		}

		wideLeft = selectedTerminals [0].transform.position.x;
		wideRight = selectedTerminals [0].transform.position.x;
		high = selectedTerminals [0].transform.position.y;
		low = selectedTerminals [0].transform.position.y;

		for (int i = 1; i < selectedTerminals.Length; i++)
		{
			if (selectedTerminals [i].transform.position.x < wideLeft)
			{
				wideLeft = selectedTerminals [i].transform.position.x;
			}
			if (selectedTerminals [i].transform.position.x > wideRight)
			{
				wideRight = selectedTerminals [i].transform.position.x;
			}
			if (selectedTerminals [i].transform.position.y < low)
			{
				low = selectedTerminals [i].transform.position.y;
			}
			if (selectedTerminals [i].transform.position.y > high)
			{
				high = selectedTerminals [i].transform.position.y;
			}
		}
		//Debug.Log (wideLeft + " " + wideRight + " " + high + " " + low);
		float centralX = (wideLeft + wideRight) / 2;
		float centralY = (high + low) / 2;
		lowerMinY = centralY;
		lowerMaxY = centralY;
		lowerX = centralX;
		upperMinY = centralY;
		upperMaxY = centralY;
		upperX = centralX;

		for (int i = 0; i < selectedTerminals.Length; i++)
		{
			if (selectedTerminals [i].transform.position.x < centralX)
			{
				lowerX = selectedTerminals [i].transform.position.x;
				if (selectedTerminals[i].transform.position.y < lowerMinY)
				{
					lowerMinY = selectedTerminals [i].transform.position.y;
				}
				if (selectedTerminals[i].transform.position.y > lowerMaxY)
				{
					lowerMaxY = selectedTerminals [i].transform.position.y;	
				}
			}
			if (selectedTerminals [i].transform.position.x > centralX)
			{
				upperX = selectedTerminals [i].transform.position.x;
				if (selectedTerminals[i].transform.position.y < upperMinY)
				{
					upperMinY = selectedTerminals [i].transform.position.y;
				}
				if (selectedTerminals[i].transform.position.y > upperMaxY)
				{
					upperMaxY = selectedTerminals [i].transform.position.y;	
				}
			}
		}

		distWireY = (upperX- lowerX)/0.05f;
		if (lowerMinY < upperMinY)
		{
			yPosY = lowerMinY;
		} 
		else
		{
			yPosY = upperMinY;	
		}
		xPosY = (lowerX + upperX)/2;
		var newWire1 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (distWireY, 1, 1);
				wireCount++;
			}
		}

		if (lowerMaxY > upperMaxY)
		{
			yPosY = lowerMaxY;
		} 
		else
		{
			yPosY = upperMaxY;	
		}
		xPosY = (lowerX + upperX)/2;
		var newWire2 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (distWireY, 1, 1);
				wireCount++;
			}
		}

		distWireY = (upperMaxY - lowerMaxY)/0.05f;
		if (lowerMaxY < upperMaxY)
		{
			xPosY = lowerX;
		} 
		else
		{
			xPosY = upperX;	
		}
		yPosY = (lowerMaxY + upperMaxY)/2;
		var newWire3 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (1, distWireY, 1);
				wireCount++;
			}
		}

		distWireY = (upperMinY - lowerMinY)/0.05f;
		if (lowerMinY < upperMinY)
		{
			xPosY = upperX;
		} 
		else
		{
			xPosY = lowerX;	
		}
		yPosY = (lowerMinY + upperMinY)/2;
		var newWire4 = Instantiate (wire, new Vector3 (xPosY, yPosY, 0f), q) as GameObject;
		wireArray = GameObject.FindGameObjectsWithTag ("Wire");
		for (int x = 0; x < wireArray.Length; x++)
		{
			wireArray [x].transform.SetParent (this.transform);
			if (x == wireCount)
			{
				name = "Wire" + x.ToString ();
				wireArray [x].name = name;
				wireArray [x].transform.localScale = new Vector3 (1, distWireY, 1);
				wireCount++;
			}
		}

		//replace original resistor with wire here

	}

	// Use this for initialization
	void Start () 
	{		
		string name;
		for (int i = 0; i < resistorCoordinates.Length; i++)
		{
			if(resistorOrientation[i] == true)
			{
				q.z = -90;
			}
			else
			{
				q.z = 0; 
			}
			Instantiate (resistor, resistorCoordinates [i], q);
		}

//***************************************************************************************** make into new function v
		ConnectWires();
	}	
}
