﻿/*
 * Author: Antonio Castro and Josiah Martinez
 * Project 59
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class Draggable : MonoBehaviour
{
	//This is mainly used in the circuit creator portion of the simulator when dragging objects such as the resistor
	private bool rightIsPressed = false;
    public bool isBeingDragged = false;

	//Function that when an object (resistor or end node) is left clicked, change the position of this object to match the position of the mouse cursor  
	void OnMouseDrag()
	{
        if (this.enabled)
        {
            isBeingDragged = true;
            if(this.tag == "Resistor" || this.tag == "EndNode")
            { 
                float currentZ = this.transform.position.z;
                float currentZRotation = this.transform.rotation.z;

                Vector2 currentScreenPoint = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
                Vector2 currentPosition = Camera.main.ScreenToWorldPoint (currentScreenPoint);
                this.transform.position = new Vector3(currentPosition.x, currentPosition.y, currentZ);

				//If right click is pressed, change rotation
                if(Input.GetMouseButtonDown(1))
                {
                    if(!this.rightIsPressed)
                    {
                        this.rightIsPressed = true;
                        if(currentZRotation == 0)
                        {
                            currentZRotation = -90;
                            this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, currentZRotation, 90f);
                        }
                        else
                        {
                            currentZRotation = 0;
                            this.transform.rotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, currentZRotation, 90f);
                        }
                    }
                }
                else if(Input.GetMouseButtonUp(1))
                {
                    this.rightIsPressed = false;
                }
            }
            else
            {
                float currentZ = this.transform.parent.position.z;
                float currentZRotation = this.transform.parent.rotation.z;

                Vector2 currentScreenPoint = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
                Vector2 currentPosition = Camera.main.ScreenToWorldPoint (currentScreenPoint);
                this.transform.parent.position = new Vector3(currentPosition.x, currentPosition.y, currentZ);
            }
        }
	}

	//When the left click is released, the object is no longer considered dragged by the mouse, thus do not change its position
	void OnMouseUp()
    {
        if (this.enabled)
        {
            isBeingDragged = false;
            GameObject trash = GameObject.FindGameObjectWithTag("Trash");
       
            if (trash != null)
            {
                if (this.GetComponent<BoxCollider2D>().bounds.Intersects(trash.GetComponent<CircleCollider2D>().bounds))
                {
                    if (this.tag == "Resistor" || this.tag == "EndNode")
                    {
                        Destroy(gameObject);
                    }
                    else
                    {
                        Destroy(transform.parent.gameObject);
                    }
                }

            }
        }
    }
		
		
}