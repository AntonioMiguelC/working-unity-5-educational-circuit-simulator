﻿/*
 *	Author: Josiah Martinez
 *	Project 59
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BatteryUtility : MonoBehaviour {

	public float voltage;

	bool escapePressed = false;

	// Initialisation chooses a random voltage value for the battery between 1 and 100 inclusive
	void Start () {
		int randomNum = Random.Range (1, 101);
		voltage = randomNum;
	}
	
	// Update is called once per frame, and checks if the ESC key on the keyboard is pressed, toggling the boolean value for escapePressed
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			escapePressed = !escapePressed;
		}	
	}

	//Provides the text for information relating to the battery, and displays it for the user to see, showing the name of the component and voltage value
	public void OnGUI()
	{	
		GUI.color = Color.black;
		if (!escapePressed)
		{	
			float xPos = (this.GetComponent<Transform> ().position.x + 8.88f) * Screen.width / 17.76f;
			float yPos = (1f - ((this.GetComponent<Transform> ().position.y + 5.005f) / 10f)) * Screen.height;
			GUI.Label (new Rect (xPos - 70f, yPos - 17f, 200, 50), this.name);
			if (this.voltage != 100)
			{
				GUI.Label (new Rect (xPos - 60f, yPos - 2f, 200, 50), this.voltage.ToString () + "V");
			} else
			{
				GUI.Label (new Rect (xPos - 75f, yPos - 2f, 200, 50), this.voltage.ToString () + "V");
			}
		}
	}
}
